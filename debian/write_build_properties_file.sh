#!/bin/sh

set -e

echo "#--Jalview Build Details--" > build/resources/resources_build/.build_properties
echo "#"$(date --utc --date="@$(dpkg-parsechangelog -STimestamp)" +"%Y-%m-%d %H:%M:%S") >> build/resources/resources_build/.build_properties
echo "BUILD_DATE="$(date --utc --date="@$(dpkg-parsechangelog -STimestamp)" +"%H\\:%M\\:%S %d %B %Y") >> build/resources/resources_build/.build_properties
echo "INSTALLATION=deb" >> build/resources/resources_build/.build_properties
echo "VERSION="$(dpkg-parsechangelog -SVersion) >> build/resources/resources_build/.build_properties
