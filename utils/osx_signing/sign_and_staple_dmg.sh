#!/usr/bin/env bash

# some defaults
ARG_D="Developer ID"
ARG_G="~/uod-development/jalview-builds/git/jalview"
# These are the defaults if neither env vars or command line option are set
[ -z "$GITDIR" ] && GITDIR="$ARG_G"
[ -z "$DEVELOPERID" ] && DEVELOPERID="$ARG_D"
[ -z "$JVER" ] && JVER="8"
TMP="/tmp"
TMPDMG="signingDMG"
TESTARCH="|x64|aarch64|"
YES=0
CLEANUP=0
GITENTITLEMENTSPATH="utils/osx_signing/entitlements.txt"
SIGN=0
DOSIGN=1
STAPLE=0
DOSTAPLE=1
VOLUMEICON=""
VOLUMEICONPATH="utils/channels/release/images/jalview-VolumeIcon.icns"
NOVOLUMEICON=0
DEFAULTVOLUMEICONFILE=".VolumeIcon.icns"
HDIUTILV="-quiet"
QUIET=0

usage() {
  echo "Usage: $( basename $0 ) [-h] [[-g gitdir] | [-e entfile]] [-d devid] [[-a appname] [-v appver ] [-j arch] [-w jver] | [-i dmgfile]] [-o outputdmg] [-O] [-t tmpdir] [-s] [-S] [-p] [-P] [-z icnsfile] [-Z] [-y] [-C] [-V] [-q]"
  echo "  "
  echo "  This script is used in the signing process of DMG disk image files for macOS."
  echo "  Either -g GITDIR or -e ENTFILE should be given."
  echo "  Either -i DMGFILE or all of -a APPNAME -v APPVER -j ARCH -w JVER should be given."
  echo "  Environment variables will be used if set and no options given."
  echo "  Precedence is in the order of command line option, environment variable (where indicated), default (where indicated)."
  echo "  "
  echo "  -h             Show help"
  echo "  -g gitdir      Use git directory gitdir (also uses GITDIR env variable, default '$ARG_G')"
  echo "  -e entfile     Use entitlements file entfile (defaults to 'GITDIR/${GITENTITLEMENTSPATH}')"
  echo "  -d devid       Use the Developer ID devid (also uses DEVELOPERID env variable, default '$ARG_D')."
  echo "  -a appname     Use the Application name appname (defaults to the first .app name found on DMG volume)."
  echo "  -v appver      Assume application version appver (also uses APPVER env variable. No default)."
  echo "  -j arch        Use the JVM architecture arch (also uses ARCH env variable. No default, should be one of${TESTARCH//|/ })."
  echo "  -w jver        Assume java version jver (also uses JVER env variable. Defaults to '1.8')."
  echo "  -i dmgfile     The existing DMG file to sign/staple (also uses DMGFILE env variable. Defaults to a combination of GITDIR, APPNAME, APPVER, ARCH and JVER)."
  echo "  -t tmpdir      Use temp directory tmpdir (default '/tmp')."
  echo "  -s             Run codesigning on the executables and created DMG file."
  echo "  -S             Don't actually perform any code signing (the command that would be run is output to stdout)."
  echo "  -p             Run stapling on the APP bundle."
  echo "  -P             Don't actually run stapling on the APP bundle (the command that would be run is output to stdout)."
  echo "  -z icnsfile    Use icnsfile as the volume icon file (defaults to using existing '$DEFAULTVOLUMEICONFILE' file or 'GITDIR/${VOLUMEICONPATH}'."
  echo "  -Z             Don't set the volume icon, even if it already exists in the existing DMG volume (default is to set volume icons if one is there/given)."
  echo "  -o outputdmg   Output DMG file (defaults to existing dmgfile in a 'signed' sub-directory)."
  echo "  -O             Overwrite the output DMG file if it already exists."
  echo "  -y             Assume 'yes' to all confirmation requests."
  echo "  -C             Cleanup temporary folders for the given DMG file or equivalent (Runs INSTEAD of all other activities. Cleanup can be narrowed down with either -i or some/all of -a -v -j -w)."
  echo "  -V             Allow tools such as hdiutil to be more verbose."
  echo "  -q             Run as quietly as we can.  Sets -y.  Use once (no output from other run commands) or twice (reduce output from this script)."
  echo "  "
  echo "  EXAMPLE:"
  echo "  $( basename $0 ) -g ~/work/git/jalview -i build/install4j/11/Jalview_Develop-2_11_4_0-d20240816-macos-aarch64-java_11.dmg -s -p -z utils/channels/develop/images/jalview_develop-VolumeIcon.icns"
  echo "  will use entitlements.txt from the gitdir (-g), and output a signed (-s) and stapled (-p) DMG file in build/install4j/11/stapled with a volume icon for Jalview Develop (-z)."
}

while getopts "hg:e:d:a:v:j:w:i:t:sSpPz:Zo:OyCVq" opt; do
  case ${opt} in
    h)
      usage
      exit
      ;;
    g)
      GITDIR="${OPTARG}"
      ;;
    e)
      ENTITLEMENTSFILE="${OPTARG}"
      ;;
    d)
      DEVELOPERID="${OPTARG}"
      ;;
    a)
      APPNAME="${OPTARG}"
      ;;
    v)
      APPVER="${OPTARG}"
      ;;
    j)
      ARCH="${OPTARG}"
      ;;
    w)
      JVER="${OPTARG}"
      WJVER="$JVER"
      ;;
    i)
      DMGFILE="${OPTARG}"
      ;;
    t)
      TMP="${OPTARG}"
      ;;
    s)
      SIGN=1
      ;;
    S)
      DOSIGN=0
      ;;
    p)
      STAPLE=1
      ;;
    P)
      DOSTAPLE=0
      ;;
    z)
      VOLUMEICON="${OPTARG}"
      ;;
    Z)
      NOVOLUMEICON=1
      ;;
    o)
      OUTPUTDMGFILE="${OPTARG}"
      ;;
    O)
      OVERWRITE=1
      ;;
    y)
      YES=1
      ;;
    C)
      CLEANUP=1
      ;;
    V)
      HDIUTILV=""
      ;;
    q)
      QUIET=$(( QUIET + 1 ))
      YES=1
      HDIUTILV="-quiet"
      ;;
    *)
      echo "Unrecognised option. Run with -h for help."
      exit 1
      ;;
  esac
done

if [ "$CLEANUP" != 1 ]; then
  # Now check GITDIR, ENTITLEMENTSFILE, DEVELOPERID, APPNAME, APPVER, ARCH, JVER, DMGFILE, TMP, TMPDMG

  # check entitlements setting and file exists
  if [ -z "$ENTITLEMENTSFILE" ]; then
    ENTITLEMENTSFILE="${GITDIR}/${GITENTITLEMENTSPATH}"
  fi
  if [ -z "$ENTITLEMENTSFILE" ]; then
    echo "Must set an entitlements file with -e entfile or -g GITDIR (or with GITDIR env variable)."
    echo ""
    usage
    exit 2
  fi
  if [ ! -e "$ENTITLEMENTSFILE" -a "$SIGN$DOSIGN" = 11 ]; then
    echo "Entitlements file '$ENTITLEMENTSFILE' doesn't exist"
    exit 3
  fi

  # check developer id
  if [ -z "$DEVELOPERID" -a "$SIGN$DOSIGN" = 11 ]; then
    echo "Must set a Developer ID with -d DEVELOPERID (or with env variable)."
    echo ""
    usage
    exit 4
  fi

  # check ARCH is set and valid
  if [ ! -z "$ARCH" -a "${TESTARCH/|$ARCH|/}" = "$TESTARCH" ]; then # not a valid arch
    echo "ARCH must be one of${TESTARCH//|/ }. Set with -a ARCH (or with env variable)."
    echo ""
    usage
    exit 5
  fi

  # check VOLUMEICON
  USEVOLUMEICON=0
  if [ ! -z "$VOLUMEICON" ]; then
    if [ ! -e "$VOLUMEICON" ]; then
      echo "Volume icon is set to '$VOLUMEICON' but it does not exist.  Use -Z to NOT set a volume icon."
      exit 6
    fi
    USEVOLUMEICON=1
  else
    VOLUMEICON="${GITDIR}/${VOLUMEICONPATH}"
    if [ ! -e "$VOLUMEICON" ]; then
      VOLUMEICON=""
    fi
    USEVOLUMEICON=1
  fi

  # check DMGFILE or alternative component args, set DMGFILE and check it exists
  if [ -z "$DMGFILE" -a \( -z "$APPNAME" -o -z "$APPVER" -o -z "$ARCH" -o -z "$JVER" \) ]; then
    echo "Must set either -i DMGFILE or all of -a APPNAME -v APPVER -j ARCH -w JVER (or with env variables)."
    echo ""
    usage
    exit 7
  fi
fi

if [ -z "$DMGFILE" ]; then
  if [ "$CLEANUP" = 1 ]; then
    # we can use wildcards for cleanup
    [ -z "$APPNAME" ] && WAPPNAME="jalview*" || WAPPNAME="$APPNAME"
    [ -z "$APPVER" ] && WAPPVER="*" || WAPPVER="$APPVER"
    [ -z "$ARCH" ] && WARCH="*" || WARCH="$ARCH"
    [ -z "$WJVER" ] && WJVER="*" # JVER is never ""
    DMGNAME="${WAPPNAME// /_}-${WAPPVER//\./_}-macos-${WARCH}-java_${WJVER}.dmg"
    DMGFILE="/tmp/fictitious.dmg"
  else
    DMGNAME="${APPNAME// /_}-${APPVER//\./_}-macos-${ARCH}-java_${JVER}.dmg"
    OLDJVER=$([ "$JVER" -lt 9 ] && echo "1.${JVER}" || echo "${JVER}" )
    DMGFILE="${GITDIR}/install4j/${OLDJVER}/${DMGNAME}"
  fi
else
  DMGNAME="$( basename "$DMGFILE" )"
fi

DMGNAMELC=$(echo "${DMGNAME//[ .]/_}" | tr '[:upper:]' '[:lower:]')XXX

if [ -z "$DMGFILE" ]; then
  echo "Must set a DMG disk image file with -i."
  echo ""
  usage
  exit 8
fi

if [ "$CLEANUP" = 1 ]; then
  TO_REMOVE=""
  TEMPBASE="${TMP}/${DMGNAMELC}"
else
  if [ ! -e "$DMGFILE" ]; then
    echo "DMG disk image file '$DMGFILE' doesn't exist"
    exit 9
  fi
  TEMPDIR="$(mktemp -d -p "${TMP}" "${DMGNAMELC}.XXXXXXXXXX")"
  if [ -z "$TEMPDIR" ]; then
    echo "Could not formulate a temp dir"
    exit 30
  fi
  DMGDIR=$(dirname "$DMGFILE")
  echo Working in temporary directory ''$TEMPDIR''
fi

myecho() {
  local MSG="$1"
  local LEVEL="$2"
  [ -z "$LEVEL" ] && LEVEL=0
  LEVEL=$((LEVEL + 1))
  if [ "$QUIET" -le "$LEVEL" ]; then
    echo "$MSG"
  fi
}

if [ "$CLEANUP" = 1 ]; then
  myecho "* -- Removing leftover temporary folders for DMG file '$DMGNAME' matching '${TEMPBASE}.*' ."
  TO_REMOVE=$( ls -1d $TEMPBASE.* 2> /dev/null )

  if [ -z "$TO_REMOVE" ]; then
    myecho "* Nothing to remove. Exiting."
    exit
  fi

  while IFS= read -r REMOVE; do
    myecho "*    + will remove '${REMOVE}'"
  done <<< "$TO_REMOVE"
else
  # Not cleaning up, tell user what we're about to do

  myecho "* -- Working with DMG file '$DMGFILE'"

  if [ "$SIGN" = 1 ]; then
    myecho "* -- Signing DMG file"
    myecho "* ---- Using entitlements file '$ENTITLEMENTSFILE'"
    myecho "* ---- Using key '$DEVELOPERID'"
    if [ "$DOSIGN" != 1 ]; then
      myecho "* ---- NOT actually code signing, but will still make new DMG file"
    fi
  fi
  myecho "* -- Working in temp dir '$TEMPDIR'"
fi

# Confirmation of what's about to happen
if [ "${YES}" != 1 ]; then
  read -r -p "* Continue? [y/N] " response
  case $(echo "${response}" | tr '[:upper:]' '[:lower:]') in
    yes|y)
      myecho "* Continuing."
      ;;
    *)
      myecho "Aborting due to negative confirmation."
      exit
      ;;
  esac
fi

# Keep a list of attached mount points to detach when exiting.
declare -a ATTACHED=()
myexit() {
  local MSG=$1
  local CODE=$2
  for VOL in "${ATTACHED[@]}"; do
    if [ ! -z "$VOL" -a -e "$VOL" ]; then
      myecho "* Detaching '${VOL}'"
      hdiutil detach $HDIUTILV "$VOL"
      myecho "* There may be temporary directories (${TEMPDIR}) left.  Clean up with -C."
    fi
  done
  myecho "# ${MSG}"
  exit $CODE
}

mydetached() {
  local D="$1"
  local NEWATTACHED=()
  for A in "${ATTACHED[@]}"; do
    [ "$A" != "$D" ] && NEWATTACHED+=("$D")
  done
  ATTACHED=("${NEWATTACHED[@]}")
}

myparanoidrm() {
  local REMOVE="${1%/}"
  local MSG="NOT REMOVING '${REMOVE}'"
  # BE PARANOID BEFORE rm -Rf
  local TEST="${REMOVE}"
  [ -z "$TEST" ] && myexit "${MSG}: Empty string." 20
  [ "$TEST" != "${TEST/../}" ] && myexit "${MSG}: Contains .. ." 21
  [ "$TEST" != "${TEST/\*/}" ] && myexit "${MSG}: Contains * ." 22
  [ "$TEST" = "${TEST#$TMP}" ] && myexit "${MSG}: Doesn't start with TMP='$TMP'." 23
  [ ${#TEST} -lt 10 ] && myexit "${MSG}: Too short." 24
  [ ! -e "$REMOVE" ] && myexit "${MSG}: Doesn't exist." 25

  # check for mounted folders
  local REALPATH="$( realpath "$REMOVE" )"
  if [ "$REALPATH" != "$REMOVE" ]; then
    myecho "* Real path for removal is '${REALPATH}'"
  fi
  local MOUNT=""
  local MOUNTS=$( hdiutil info | grep "Apple_HFS" | grep "$REALPATH" 2>/dev/null | sed -e 's/^[^[:space:]]*[[:space:]]*Apple_HFS[[:space:]]*//' )
  while IFS= read -r MOUNT; do
    if [ ! -z "$MOUNT" -a -d "$MOUNT" ]; then
      myecho "* First detaching '${MOUNT}'"
      hdiutil detach $HDIUTILV "$MOUNT"
    fi
  done <<< "$MOUNTS"

  rm -Rf "${REMOVE}"
}

mycommand() {
  local ARGS=("${@}")
  local RUN=${ARGS[0]}
  local DISPLAY=""
  i=1
  while [ "$i" -lt "${#ARGS[@]}" ]; do
    local A="${ARGS[i]}"
    [ "$A" != "${A/ /}" ] && A="\"${A}\""
    [ DISPLAY != "" ] && DISPLAY="${DISPLAY} "
    DISPLAY="${DISPLAY}${A}"
    i=$((i+1))
  done
  local COMMAND=("${ARGS[@]:1}")
  if [ "$RUN" = 1 ]; then
    myecho "+ Running: $DISPLAY"
    local RET=1
    if [ "$QUIET" -le 2 ]; then
      "${COMMAND[@]}"
      RET=$?
    else
      "${COMMAND[@]}" > /dev/null
      RET=$?
    fi
  else
    myecho "* NOT running: $DISPLAY"
    RET=0
  fi
  return $RET
}

if [ "$CLEANUP" = 1 ]; then
  # just cleaning up
  while IFS= read -r REMOVE; do
    myecho "* Removing '${REMOVE}'"
    myparanoidrm "${REMOVE}"
  done <<< "$TO_REMOVE"
  myecho "* Finished cleanup. Exiting."
  exit
fi

MOUNTROOT="${TEMPDIR}/Volume"
mkdir -p "$MOUNTROOT" || myexit "Could not create mount root dir '${MOUNTROOT}'" 31

myecho "* Mounting disk image '${DMGFILE}' in '${MOUNTROOT}'"
mycommand 1 hdiutil attach $HDIUTILV -mountroot "${MOUNTROOT}" "${DMGFILE}" || myexit "Could not mount '${DMGFILE}' in '${MOUNTROOT}'. Aborting." 10
VOLDIR=$(ls -1d "${MOUNTROOT%/}"/* | head -1)
VOLDIR="${VOLDIR%/}" # remove trailing slash
if [ -z "$VOLDIR" ]; then
  myexit "Failed to find mounted volume in '${MOUNTROOT}'" 11
fi
ATTACHED+=("$VOLDIR")
VOLNAME=$(basename "$VOLDIR")
FOUNDAPPNAME=$(ls -1d "${VOLDIR}/"*.app | head -1)
FOUNDAPPNAME="${FOUNDAPPNAME%/}" # remove trailing slash
FOUNDAPPNAME="${FOUNDAPPNAME%.app}" # without the ".app"
FOUNDAPPNAME="$(basename "$FOUNDAPPNAME")"

# More info for user before confirming continuation
myecho "* -- Found volume name '${VOLNAME}'"
myecho "* -- Found application name '${FOUNDAPPNAME}'"
TEMPDMGDIR="${TEMPDIR%/}/${TMPDMG}"
if [ -e "$TEMPDMGDIR" ]; then
  myexit "Folder '${TEMPDMGDIR}' already exists. Please remove it or use -s SIGNINGDMG to set a different dir name." 12
  exit 11
fi
if [ "$FOUNDAPPNAME" != "$APPNAME" ]; then
  if [ -z "$APPNAME" ]; then
    myecho "* ---- Going to use APPNAME '${FOUNDAPPNAME}'"
  else
    myecho "* ---- Going to now use APPNAME '${APPNAME}' (was set to '${FOUNDAPPNAME}')"
  fi
fi
myecho "* -- Going to copy volume contents to '${TEMPDMGDIR}'"
if [ "$VOLUMEICON" = 1 ]; then
  myecho "* -- Going to try and set a volume icon to '${VOLUMEICON}'"
fi
if [ "$STAPLE" = 1 ]; then
  myecho "* -- Stapling '${FOUNDAPPNAME}'.app"
  if [ "$DOSTAPLE" != 1 ]; then
    myecho "* ---- NOT actually stapling"
  fi
fi

# Confirmation of final steps
if [ "${YES}" != 1 ]; then
  read -r -p "* Continue? [y/N] " response
  case $(echo "${response}" | tr '[:upper:]' '[:lower:]') in
    yes|y)
      myecho "* Continuing."
      ;;
    *)
      myexit "Aborting due to negative confirmation." 0
      exit
      ;;
  esac
fi

APPNAME="$FOUNDAPPNAME"

# Copy volume contents
myecho "* Copying '${VOLDIR}' to '${TEMPDMGDIR}'"
mycommand 1 ditto "$VOLDIR" "$TEMPDMGDIR"

myecho "* Unmounting '${VOLDIR}' and removing '$MOUNTROOT"
mycommand 1 hdiutil detach $HDIUTILV "$VOLDIR"
mydetached "$VOLDIR"
rmdir "$MOUNTROOT" || myecho "Could not remove mount rood dir '$MOUNTROOT'. Continuing, but you might want to clean up with 'rmdir \"${MOUNTROOT}\"' or use the -C option."

APPPATH="${TEMPDMGDIR}/${APPNAME}.app"

if [ "$SIGN" = 1 ]; then
  myecho "* Code signing in '${TEMPDMGDIR}'"

  FILE="${APPPATH}/Contents/Resources/app/jre/Contents/MacOS/libjli.dylib"
  mycommand $DOSIGN codesign  --remove-signature --force --deep -vvvv -s "$DEVELOPERID" --options runtime --entitlements "$ENTITLEMENTSFILE" "$FILE"
  mycommand $DOSIGN codesign  --verify --deep -v "$FILE"


  # Need to unpack and sign everything in here too
  CWD=$(pwd)
  mycommand $DOSIGN mkdir ${TEMPDIR}/jarsign
  mycommand $DOSIGN cd ${TEMPDIR}/jarsign
  
  # for TEST-RELEASE or DEVELOP
  JARPATH=alt
  # if it is a release build then this happens 
  if [ -d "${APPPATH}/Contents/Resources/app/release" ]; then
  	JARPATH=release
  fi
  
  JFILE="${APPPATH}/Contents/Resources/app/${JARPATH}/libquaqua-8.0.jnilib.jar"
  mycommand $DOSIGN jar xf "$JFILE" 

  FILE="libquaqua.jnilib"
  mycommand $DOSIGN codesign  --remove-signature --force --deep -vvvv -s "$DEVELOPERID" --options runtime --entitlements "$ENTITLEMENTSFILE" "$FILE"
  mycommand $DOSIGN codesign  --verify --deep -v "$FILE"

  mycommand $DOSIGN mv "$JFILE" "${JFILE}.old"
  mycommand $DOSIGN jar cf "$JFILE" *
  mycommand $DOSIGN rm "${JFILE}.old"


  JFILE="${APPPATH}/Contents/Resources/app/${JARPATH}/libquaqua64-8.0.jnilib.jar"
  mycommand $DOSIGN jar xf "$JFILE" 

  FILE="libquaqua64.jnilib"
  mycommand $DOSIGN codesign  --remove-signature --force --deep -vvvv -s "$DEVELOPERID" --options runtime --entitlements "$ENTITLEMENTSFILE" "$FILE"
  mycommand $DOSIGN codesign  --verify --deep -v "$FILE"

  mycommand $DOSIGN mv "$JFILE" "${JFILE}.old"
  mycommand $DOSIGN jar cf "$JFILE" *
  mycommand $DOSIGN rm "${JFILE}.old"

  mycommand $DOSIGN codesign  --remove-signature --force --deep -vvvv -s "$DEVELOPERID" --options runtime --entitlements "$ENTITLEMENTSFILE" "$JFILE"
  mycommand $DOSIGN codesign  --verify --deep -v "$JFILE"

  mycommand $DOSIGN cd $CWD
  mycommand $DOSIGN rm -Rf ${TEMPDIR}/jarsign

  FILE="${APPPATH}/Contents/MacOS/JavaApplicationStub"
  mycommand $DOSIGN codesign  --remove-signature --force --deep -vvvv -s "$DEVELOPERID" --options runtime --entitlements "$ENTITLEMENTSFILE" "$FILE"
fi

OUTPUTDIRNAME="notsigned"
# stapling
if [ "$STAPLE" = 1 ]; then
  OUTPUTDIRNAME="stapled"

  myecho "* Stapling '${APPNAME}.app'"
  mycommand $DOSTAPLE xcrun stapler staple "${APPPATH}"
elif [ "$SIGN" = 1 ]; then
  OUTPUTDIRNAME="signed"
fi

if [ ! -z "$OUTPUTDMGFILE" ]; then
  NEWDMGFILE="$OUTPUTDMGFILE"
else
  SIGNEDDIR="${DMGDIR%/}/${OUTPUTDIRNAME}"
  NEWDMGFILE="${SIGNEDDIR}/${DMGNAME}"
  myecho "* Creating folder '${SIGNEDDIR}' for new DMG file '${DMGNAME}'"
  mkdir -p "$SIGNEDDIR"
fi

if [ -e "$NEWDMGFILE" ]; then
  if [ "$OVERWRITE" = 1 ]; then
    rm "$NEWDMGFILE"
  else
    myexit "* New DMG file already exists.  Use -O to overwrite.  Exiting." 13
  fi
fi


if [ "$NOVOLUMEICON" = 1 ]; then
  myecho "* NOT setting a volume icon"

  # without volume icon

  myecho "* Creating new DMG file '${NEWDMGFILE}' to sign"
  mycommand 1 hdiutil create $HDIUTILV -megabytes 260 -srcfolder "$TEMPDMGDIR" -volname "$VOLNAME" "$NEWDMGFILE" || myexit "Could not create new DMG file '${NEWDMGFILE}'" 15
else
  # with volume icon

  GOTVOLUMEICON=0
  # Copy a given VolumeIcon.icns (if given).  Do this before hdiutil create
  if [ ! -z "$VOLUMEICON" -a -e "$VOLUMEICON" -a "$USEVOLUMEICON" = 1 ]; then
    myecho "* Copying the volume icon '${VOLUMEICON}' to the temporary volume"
    cp -f "$VOLUMEICON" "${TEMPDMGDIR}/${DEFAULTVOLUMEICONFILE}"
  fi

  TEMP_RW_BASE=$(mktemp -d -p "${TEMPDIR}" -t "temp_rwXXX")
  TEMPDMGFILE="${TEMP_RW_BASE}.dmg"
  TEMPMOUNTDIR="${TEMP_RW_BASE}/Volume"

  myecho "* Creating temporary RW DMG file '${TEMPDMGFILE}' to sign"
  mycommand 1 hdiutil create $HDIUTILV -format UDRW -megabytes 260 -srcfolder "$TEMPDMGDIR" -volname "$VOLNAME" "$TEMPDMGFILE" || myexit "Could not create temporary DMG file '${TEMPDMGFILE}'" 15

  myecho "* Mounting temporary disk image '${TEMPDMGFILE}' on '${TEMPMOUNTDIR}'"
  mycommand 1 hdiutil attach $HDIUTILV -mountpoint "${TEMPMOUNTDIR}" "${TEMPDMGFILE}" || myexit "Could not mount '${TEMPDMGFILE}' on '${TEMPMOUNTDIR}'. Aborting." 16
  ATTACHED+=("$TEMPMOUNTDIR")

  if [ -e "${TEMPMOUNTDIR}/${DEFAULTVOLUMEICONFILE}" ]; then
    myecho "* Setting the volume icon '${DEFAULTVOLUMEICONFILE}'"
    mycommand 1 SetFile -c icnC "${TEMPMOUNTDIR}/${DEFAULTVOLUMEICONFILE}"
    mycommand 1 SetFile -a C "${TEMPMOUNTDIR}"
  else
    myecho "* Could not find a volume icon '${VOLUMEICON}' in '${TEMPMOUNTDIR}'.  Not setting a volume icon."
  fi

  myecho "* Unmounting '${TEMPMOUNTDIR}'"
  mycommand 1 hdiutil detach $HDIUTILV "$TEMPMOUNTDIR"
  mydetached "$TEMPMOUNTDIR"

  myecho "* Converting temporary DMG file to new DMG file '${NEWDMGFILE}' to sign"
  mycommand 1 hdiutil convert $HDIUTILV "$TEMPDMGFILE" -format UDZO -o "$NEWDMGFILE" || myexit "Could not convert to new DMG file '${NEWDMGFILE}'" 17

  myecho "* Removing temporary DMG file '${TEMPDMGFILE}'"
  rm "$TEMPDMGFILE"
fi

if [ "$SIGN" = 1 ]; then
  myecho "* Code signing '${NEWDMGFILE}'"
  mycommand $DOSIGN codesign --force --deep -vvvv -s "$DEVELOPERID" --options runtime --entitlements "$ENTITLEMENTSFILE" "$NEWDMGFILE"
  mycommand $DOSIGN codesign --deep -vvvv "$NEWDMGFILE"
fi

myecho "* Removing TEMPDIR '${TEMPDIR}'"
myparanoidrm "${TEMPDIR}"

SIGNED=" Unsigned"
if [ "$SIGN" = 1 ]; then
  SIGNED=" Signed"
  [ "$STAPLE" = 1 ] && AND=" and"
fi
[ "$STAPLED" = 1 ] && STAPLED=" stapled"
myecho "***${SIGNED}${AND}${STAPLED} DMG file at '${NEWDMGFILE}'"
