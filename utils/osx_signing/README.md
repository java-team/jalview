# Using sign_and_staple_dmg.sh

## Important flags

|||
|-|-|
|`-i dmgfile`    | Path to the DMG file to be signed/stapled |
|`-s`            | Sign the known binaries in the DMG file |
|`-p`            | Staple the .app in the DMG file |
|`-v iconfile`   | Set the volume icon to iconfile |
|`-o outputfile` | Output DMG will be outputfile |
|`-O`            | Overwrite output file if it already exists |
|`-y`            | Don't ask for confirmation |

### Expected usage

*e.g.*
`sign_and_staple_dmg.sh -i build/install4j/1.8/Jalview_Develop-2_11_4_0-d*-macos-aarch64-java_11.dmg -s -r -v utils/channel/release/images/jalview_develop-VolumeIcon.icns`

will work mount the DMG file `build/install4j/1.8/Jalview_Develop-2_11_4_0-d*-macos-aarch64-java_11.dmg` in temp folder,
make a copy of the contents,
sign the known binaries,
put the Volume Icon into the copy of the volume,
staple the .app bundle,
make a new RW DMG, mount that and set the Volume Icon for the volume,
convert that back to a RO DMG file.

The DMG file will be saved in a subfolder where the original file was found, called one of `notsigned`, `signed`, `stapled` and with the same filename as the original.
The output file can be changed with `-o outputfile` (which works from pwd, not necessarily the directory of the original dmgfile).

## Codesigned files

Presently only the files that were signed in sign_dmg.sh and stapled in staple_dmg.sh (see previous commits, e.g. be110b0de5) are being signed/stapled:

### Signed

(`${APPNAME}` is, e.g. "Jalview Develop.app")

- `${APPNAME}/Contents/Resources/app/jre/Contents/MacOS/libjli.dylib`
- `${APPNAME}/Contents/MacOS/JavaApplicationStub`

### Stapled

- `${APPNAME}`

### Final signing

- The resulting DMG file.


### Not signed, maybe should be?

The JAR and specific contents of the JAR mentioned in `README.old` are NOT signed in `sign_and_staple_dmg.sh` (yet).

### Future work

The list of files to be signed should be able to be passed in, maybe as comma or space-separated list option value, or list of remaining args, or as an input text file.

