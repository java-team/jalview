#!/usr/bin/env bash

# perform a dev build and install on local macOS machine
APP=""
INSTALLERVOL=""
APPLICATIONS=/Applications
CHANNEL="LOCAL"
DMG=build/install4j/11/Jalview_Local-TEST-macos-java_11.dmg
GRADLE=""
APPBASEOVERRIDEARG=""
APPDIR=""
BUILDID=MACOS-AARCH64-DMG
CLEAN="clean"
JAVA=11
x=$(grep jalview.version= RELEASE)
VERSION=${x#*=}
UOS=$(uname -s)
UARCH=$(uname -m)
JARCH=aarch64
case $UARCH in
  x86_64)
  JARCH=x64
  ;;
  arm64)
  JARCH=aarch64
  ;;
  *)
  echo "Unknown architecture '$UARCH'. Exiting."
  exit 1;
  ;;
esac
JARCHUC=$(echo $JARCH | tr '[:lower:]' '[:upper:]')
case $UOS in
  Darwin)
    BUILDID=MACOS-${JARCHUC}-DMG
    OS=macos
    ;;
  Linux)
    BUILDID=LINUX-${JARCHUC}-SH
    OS=linux
    ;;
#  Windows)
#    BUILDID=WINDOWS-{$JARCHUC}-EXE
#    OS=windows
#    echo "Not configured for OS '${OS}'. Exiting."
#    exit 2
#    ;;
  *)
    echo "Not configured for OS '${OS}'. Exiting."
    exit 2
    ;;
esac

echo "Set OS to '${OS}' and BUILDID to '${BUILDID}'"


while getopts ":i:n:a:c:d:go:b:j:p:lx" opt; do
  case ${opt} in
    i)
      INSTALLERVOL="${OPTARG}"
      ;;
    n)
      APP="${OPTARG}"
      ;;
    a)
      APPLICATIONS="${OPTARG}"
      ;;
    c)
      CHANNEL="${OPTARG}"
      ;;
    d)
      DMG="${OPTARG}"
      ;;
    g)
      GRADLE=1
      ;;
    o)
      APPBASEOVERRIDEARG="-Pgetdown_appbase_override=${OPTARG}"
      ;;
    b)
      BUILDID="${OPTARG}"
      ;;
    j)
      JAVA="${OPTARG}"
      ;;
    p)
      APPDIR="${OPTARG}"
      ;;
    x)
      APPBASEOVERRIDEARG="-Pgetdown_appbase_override=https://www.jalview.org/NOCHANNEL"
      ;;
    l)
      CLEAN=""
      ;;
    ?)
      echo "Invalid option -${OPTARG}"
      exit 3;
      ;;
  esac
done

if [ -z $APP ]; then
  V=""
  case ${CHANNEL} in
    DEVELOP)
      APP="Jalview Develop"
      V="${VERSION//\./_}-d$(date +%Y%m%d)"
      ;;
    TEST-RELEASE)
      APP="Jalview Test"
      V="${VERSION//\./_}-test"
      ;;
    RELEASE)
      APP="Jalview"
      V="${VERSION//\./_}"
      ;;
    *)
      APP="Jalview Local"
      V="TEST"
      ;;
  esac
fi
APPU="${APP// /_}"
APPLCU=$(echo "$APPU" | tr '[:upper:]' '[:lower:]')
DMG="build/install4j/11/${APPU}-${V}-${OS}-${JARCH}-java_${JAVA}.dmg"
SH="build/install4j/11/${APPLCU}-${V}-${OS}-${JARCH}-java_${JAVA}.sh"

if [ "${GRADLE}" = 1 ]; then
  echo "Running: gradle ${CLEAN} installers -PCHANNEL="${CHANNEL}" -Pinstall4j_build_ids="${BUILDID}" ${APPBASEOVERRIDEARG} -PJAVA_VERSION=${JAVA}"
  gradle ${CLEAN} installers -PCHANNEL="${CHANNEL}" -Pinstall4j_build_ids="${BUILDID}" ${APPBASEOVERRIDEARG} -PJAVA_VERSION=${JAVA}
  if [ $? != 0 ]; then
    echo "Problem with gradle build: exit code $?"
    exit 4
  fi
else
  echo "Not running gradle installers"
fi

case $OS in

  macos)

    WC="*"
    if [ -z $INSTALLERVOL ]; then
      INSTALLERVOL="Install ${APP}"
      WC="*"
    fi

    if [ -e "/Volumes/$INSTALLERVOL" ]; then
      hdiutil detach "/Volumes/$INSTALLERVOL"
    fi
    if [ -e "$DMG" ]; then
      open $DMG
    else
      echo "No DMG file '$DMG'" 1>&2
      exit 5
    fi
    echo "Mounting '$DMG' at /Volumes"
    N=0
    MOUNTEDAPP="/Volumes/$INSTALLERVOL$WC/$APP.app"
    echo "Waiting for '$MOUNTEDAPP' to appear"
    while [ "$(ls -1 "/Volumes/$INSTALLERVOL"$WC"/$APP.app" 2>/dev/null)" = "" ]; do
      echo -n "."
      N=$(( N+1 ))
      if [ $N = 40 ]; then
        echo ""
        echo "Looks like something wrong with the DMG '$DMG'"
        exit 6
      fi
      sleep 0.1
    done
    INSTALLERVOL="$(ls -1d "/Volumes/Install ${APP} "$WC 2>/dev/null | head -1)"
    INSTALLERVOL=${INSTALLERVOL%/}
    INSTALLERVOL=${INSTALLERVOL#/Volumes/}
    MOUNTEDAPP="/Volumes/$INSTALLERVOL/$APP.app"
    echo ""

    if [ -e "$MOUNTEDAPP" ]; then
      echo "Found '$MOUNTEDAPP'"
      echo "Removing '$APPLICATIONS/$APP.app'"
      /bin/rm -r "$APPLICATIONS/$APP.app"
      echo "Syncing '$MOUNTEDAPP' to '$APPLICATIONS/'"
      rsync --delete -avh "$MOUNTEDAPP" "$APPLICATIONS/"
      echo "Unmounting '/Volumes/$INSTALLERVOL'"
      hdiutil detach "/Volumes/$INSTALLERVOL"
    else
      echo "Could not find mounted app '$MOUNTEDAPP'"
      exit 7
    fi

    ;;
  
  
  linux)
  
    if [ -z $APPDIR ]; then
      APPDIR="${HOME}/opt/${APPLCU}"
    fi
    
    if [ -e "$APPDIR" ]; then
      echo "Uninstalling ${APP} at '${APPDIR}'"
      if [ -e "${APPDIR}/uninstall" ]; then
        ${APPDIR}/uninstall -q
      else
        echo "Could not find uninstall script at '${APPDIR}/uninstall'"
        exit 8
      fi
    fi
    
    echo "Installing '${SH}' into '${APPDIR}'"
    if [ -e "$SH" ]; then
      bash "$SH" -q -dir "${APPDIR}"
    else
      echo "Can't find install script '${SH}'"
      exit 9
    fi
    
    ;;
    
    
  *)
    echo "Cannot handle installation in OS '${OS}' yet."
    exit 10
    ;;
    
esac
