/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.datamodel;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ContactRangeTest
{

  @Test
  public void testContactRangeBean()
  {
    ContactRange cr = new ContactRange();
    cr.update(5, 15, 6, 0.2, 12, 1.5, 3.7);
    Assert.assertEquals(5, cr.getFrom_column());
    Assert.assertEquals(15, cr.getTo_column());
    Assert.assertEquals(6, cr.getMinPos());
    Assert.assertEquals(0.2, cr.getMin());
    Assert.assertEquals(12, cr.getMaxPos());
    Assert.assertEquals(1.5, cr.getMax());
    Assert.assertEquals(3.7, cr.getMean());
    cr.setFrom_column(6);
    Assert.assertEquals(6, cr.getFrom_column());
    cr.setTo_column(16);
    Assert.assertEquals(16, cr.getTo_column());
    cr.setMinPos(7);
    Assert.assertEquals(7, cr.getMinPos());
    cr.setMin(0.4);
    Assert.assertEquals(0.4, cr.getMin());
    cr.setMaxPos(13);
    Assert.assertEquals(13, cr.getMaxPos());
    cr.setMax(2.5);
    Assert.assertEquals(2.5, cr.getMax());
    cr.setMean(3.7);
    Assert.assertEquals(3.7, cr.getMean());
  }
}
