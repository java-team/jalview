/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.gui;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

import java.awt.Font;
import java.awt.FontMetrics;

import jalview.datamodel.Alignment;
import jalview.datamodel.AlignmentAnnotation;
import jalview.datamodel.Sequence;

import org.mockito.Mockito;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class AnnotationLabelsTest
{
  @Test(groups = "Functional")
  public void testGetTooltip()
  {
    assertNull(AnnotationLabels.getTooltip(null));

    /*
     * simple description only
     */
    AlignmentAnnotation ann = new AlignmentAnnotation("thelabel", "thedesc",
            null);
    String expected = "<html>thedesc</html>";
    assertEquals(AnnotationLabels.getTooltip(ann), expected);

    /*
     * description needing html encoding
     * (no idea why '<' is encoded but '>' is not)
     */
    ann.description = "TCoffee scores < 56 and > 28";
    expected = "<html>TCoffee scores &lt; 56 and > 28</html>";
    assertEquals(AnnotationLabels.getTooltip(ann), expected);

    /*
     * description already html formatted
     */
    ann.description = "<html>hello world</html>";
    assertEquals(AnnotationLabels.getTooltip(ann), ann.description);

    /*
     * simple description and score
     */
    ann.description = "hello world";
    ann.setScore(2.34d);
    expected = "<html>hello world<br/> Score: 2.34</html>";
    assertEquals(AnnotationLabels.getTooltip(ann), expected);

    /*
     * html description and score
     */
    ann.description = "<html>hello world</html>";
    ann.setScore(2.34d);
    expected = "<html>hello world<br/> Score: 2.34</html>";
    assertEquals(AnnotationLabels.getTooltip(ann), expected);

    /*
     * score, no description
     */
    ann.description = " ";
    assertEquals(AnnotationLabels.getTooltip(ann),
            "<html> Score: 2.34</html>");
    ann.description = null;
    assertEquals(AnnotationLabels.getTooltip(ann),
            "<html> Score: 2.34</html>");

    /*
     * sequenceref, simple description
     */
    ann.description = "Count < 12";
    ann.sequenceRef = new Sequence("Seq1", "MLRIQST");
    ann.hasScore = false;
    ann.score = Double.NaN;
    expected = "<html>Seq1 : Count &lt; 12</html>";
    assertEquals(AnnotationLabels.getTooltip(ann), expected);

    /*
     * sequenceref, html description, score
     */
    ann.description = "<html>Score < 4.8</html>";
    ann.sequenceRef = new Sequence("Seq1", "MLRIQST");
    ann.setScore(-2.1D);
    expected = "<html>Seq1 : Score < 4.8<br/> Score: -2.1</html>";
    assertEquals(AnnotationLabels.getTooltip(ann), expected);

    /*
     * no score, null description
     */
    ann.description = null;
    ann.hasScore = false;
    ann.score = Double.NaN;
    assertNull(AnnotationLabels.getTooltip(ann));

    /*
     * no score, empty description, sequenceRef
     */
    ann.description = "";
    assertEquals(AnnotationLabels.getTooltip(ann), "<html>Seq1 :</html>");

    /*
     * no score, empty description, no sequenceRef
     */
    ann.sequenceRef = null;
    assertNull(AnnotationLabels.getTooltip(ann));
  }

  @Test(groups = "Functional")
  public void testGetStatusMessage()
  {
    assertNull(AnnotationLabels.getStatusMessage(null, null));

    /*
     * simple label
     */
    AlignmentAnnotation aa = new AlignmentAnnotation("IUPredWS Short",
            "Protein disorder", null);
    assertEquals(AnnotationLabels.getStatusMessage(aa, null),
            "IUPredWS Short");

    /*
     * with sequence ref
     */
    aa.setSequenceRef(new Sequence("FER_CAPAA", "MIGRKQL"));
    assertEquals(AnnotationLabels.getStatusMessage(aa, null),
            "FER_CAPAA : IUPredWS Short");

    /*
     * with graph group (degenerate, one annotation only)
     */
    aa.graphGroup = 1;
    AlignmentAnnotation aa2 = new AlignmentAnnotation("IUPredWS Long",
            "Protein disorder", null);
    assertEquals(
            AnnotationLabels.getStatusMessage(aa, new AlignmentAnnotation[]
            { aa, aa2 }), "FER_CAPAA : IUPredWS Short");

    /*
     * graph group with two members; note labels are appended in
     * reverse order (matching rendering order on screen)
     */
    aa2.graphGroup = 1;
    assertEquals(
            AnnotationLabels.getStatusMessage(aa, new AlignmentAnnotation[]
            { aa, aa2 }), "FER_CAPAA : IUPredWS Long, IUPredWS Short");

    /*
     * graph group with no sequence ref
     */
    aa.sequenceRef = null;
    assertEquals(
            AnnotationLabels.getStatusMessage(aa, new AlignmentAnnotation[]
            { aa, aa2 }), "IUPredWS Long, IUPredWS Short");
  }
  
  
  @Test(groups = "Functional", dataProvider = "TestDrawLabelsData")
  public void testDrawLabels(AlignmentAnnotation[] annotations, int expectedWidth) {

      FontMetrics mockFontMetrics = Mockito.mock(FontMetrics.class);
      Mockito.when(mockFontMetrics.getHeight()).thenReturn(10);
      Mockito.when(mockFontMetrics.getDescent()).thenReturn(2);
      Mockito.when(mockFontMetrics.stringWidth(Mockito.anyString())).thenAnswer(invocation -> {
          String str = invocation.getArgument(0);
          return str.length() * 7;  
      });

      Font mockFont = new Font("Arial", Font.PLAIN, 12);  
      Mockito.when(mockFontMetrics.getFont()).thenReturn(mockFont);

      boolean actuallyDraw = false;
      boolean clip = false;
      int width = 200;
      boolean forGUI = true;
      boolean includeHidden = true;

      AlignViewport av = Mockito.mock(AlignViewport.class);
      Alignment alignment = Mockito.mock(Alignment.class);
      Mockito.when(alignment.getAlignmentAnnotation()).thenReturn(annotations);
      Mockito.when(av.getAlignment()).thenReturn(alignment);
      Mockito.when(av.getFont()).thenReturn(new Font("Arial", Font.PLAIN, 12));  // Mock font

      AnnotationLabels annotationLabels = new AnnotationLabels(av);

      int resultWidth = annotationLabels.drawLabels(null, clip, width, actuallyDraw, forGUI, mockFontMetrics, includeHidden);

      assertEquals(resultWidth, expectedWidth);
      Mockito.reset(mockFontMetrics, av, alignment);
  }
  
  @DataProvider(name = "TestDrawLabelsData")
  public static Object[][] provideSecondaryStructureAnnotation() {


    Sequence s1 = new Sequence("Seq1", "MLRIQST");
    Sequence s2 = new Sequence("Sequence2", "MLRIQST");
    
    String label1 = "Label1";
    String label2 = "Label_2";
    String label3 = "Label_three";
    String description1 = "Desc1";
    String description2 = "<html><h1>Desc_2</h1></html>";
    String description3 = "Description_three";
    
    
    AlignmentAnnotation a1 = new AlignmentAnnotation(label1, description1, null);
    AlignmentAnnotation a2 = new AlignmentAnnotation(label2, description2, null);
    AlignmentAnnotation a3 = new AlignmentAnnotation(label3, description3, null);
    AlignmentAnnotation a4 = new AlignmentAnnotation(label3, description3, null);
    AlignmentAnnotation a5 = new AlignmentAnnotation(label1, description2, null);
    AlignmentAnnotation a6 = new AlignmentAnnotation(label1, description2, null);
    
    a1.sequenceRef = s1;
    a2.sequenceRef = s1;
    a3.sequenceRef = s1;
    a4.sequenceRef = s2;
    a5.sequenceRef = s1;
    a6.sequenceRef = s2;
    
    a1.visible = true;
    a2.visible = true;
    a3.visible = true;
    a4.visible = true;
    a5.visible = true;
    a6.visible = true;
    
    return new Object[][]{

        {new AlignmentAnnotation[]{a2, a2, a1}, 7 * ((s1.getName()+" ").length() + label2.length()) + 3},
        {new AlignmentAnnotation[]{a1, a2, a3}, 7 * (label3.length()) + 3}, //Different labels and descriptions, same sequenceRef
        {new AlignmentAnnotation[]{a1, a2, a4}, 7 * ((s2.getName()+" ").length() + label3.length()) + 3}, //Different labels, descriptions, sequenceRef
        {new AlignmentAnnotation[]{a1, a1, a1}, 7 * ((s1.getName()+" ").length() + label1.length()) + 3}, //Same labels, descriptions, sequenceRef
        {new AlignmentAnnotation[]{a1, a1, a2}, 7 * ((s1.getName()+" ").length() + label1.length()) + 3}, 
        {new AlignmentAnnotation[]{a1, a5, a2}, 7 * ((s1.getName()+" ").length() + "Desc_2".length()) + 3}, //same labels and sequence, different description
        {new AlignmentAnnotation[]{a1, a6, a2}, 7 * ((s2.getName()+" ").length() + "Desc_2".length()) + 3}, //same labels and sequence, different description

    };
  }
}
