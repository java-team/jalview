/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.gui;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.io.File;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import jalview.bin.Cache;
import jalview.bin.Jalview;
import jalview.datamodel.AlignmentI;
import jalview.datamodel.SequenceI;
import jalview.gui.StructureViewer.ViewerType;
import jalview.io.DataSourceType;
import jalview.io.FileLoader;
import jalview.structure.StructureImportSettings.TFType;
import jalview.util.Platform;

public class AnnotationLabelsTest2
{
  private static double scaling;

  @BeforeClass(alwaysRun = true)
  public static void setUpBeforeClass() throws Exception
  {
    if (Desktop.instance != null)
      Desktop.instance.closeAll_actionPerformed(null);

    setUpJvOptionPane();
    /*
     * use read-only test properties file
     */
    Cache.loadProperties("test/jalview/io/testProps.jvprops");
    Jalview.main(new String[] { "--nonews", "--nosplash", });

    scaling = JvSwingUtilsTest.getScaling(Desktop.instance);
  }

  @AfterMethod(alwaysRun = true)
  public void tearDown()
  {
    if (Desktop.instance != null)
      Desktop.instance.closeAll_actionPerformed(null);
  }

  /**
   * configure (read-only) properties for test to ensure Consensus is computed
   * for colour Above PID testing
   */
  @BeforeMethod(alwaysRun = true)
  public void setUp()
  {
    Cache.loadProperties("test/jalview/io/testProps.jvprops");
    Cache.applicationProperties.setProperty("SHOW_IDENTITY",
            Boolean.TRUE.toString());

  }

  public static void setUpJvOptionPane()
  {
    JvOptionPane.setInteractiveMode(false);
    JvOptionPane.setMockResponse(JvOptionPane.CANCEL_OPTION);
  }

  @Test(
    groups =
    { "Functional", "testTask1" },
    dataProvider = "openFilesWithIdWidthChanges")
  public void testIdWidthChanges(String alignmentFilename, boolean wrap,
          int idWidth1min, int idWidth1max, int manualWidth,
          String structureFilename, String paeFilename,
          boolean secondaryStructureView, TFType temperatureFactorType,
          ViewerType viewerType, int idWidth2min, int idWidth2max)
  {
    AlignFrame af = new FileLoader()
            .LoadFileWaitTillLoaded(alignmentFilename, DataSourceType.FILE);

    try
    {
      Thread.sleep(200); // to allow alignment annotations to open
    } catch (InterruptedException e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    AlignViewport av = af.getCurrentView();

    int idWidth = 0;

    idWidth = av.getIdWidth();
    assertTrue(idWidth > idWidth1min,
            "idWidth (" + idWidth + ") is not greater than " + idWidth1min);
    assertTrue(idWidth < idWidth1max, "idWidth (" + idWidth
            + ") is not narrower than " + idWidth1max);

    // set wrap
    if (wrap)
    {
      af.setWrapFormat(true, false);
      idWidth = av.getIdWidth();
      assertTrue(idWidth > idWidth1min, "After wrap idWidth (" + idWidth
              + ") is not greater than " + idWidth1min);
      assertTrue(idWidth < idWidth1max, "After wrap idWidth (" + idWidth
              + ") is not narrower than" + idWidth1max);
    }

    AlignmentI al = av.getAlignment();
    SequenceI s = al.getSequenceAt(0);
    AlignmentPanel ap = af.alignPanel;

    File structureFile = new File(structureFilename);
    File paeFile = new File(paeFilename);

    StructureViewer sv = StructureChooser.openStructureFileForSequence(null,
            null, ap, s, false, structureFile.getAbsolutePath(),
            temperatureFactorType, paeFile.getAbsolutePath(), true,
            secondaryStructureView, false, viewerType);
    // give time for annotations to open
    try
    {
      Thread.sleep(200); // to allow alignment annotations to open
    } catch (InterruptedException e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    // idWidth = ap.getIdPanel().getWidth();
    idWidth = av.getIdWidth();
    assertTrue(idWidth >= idWidth2min,
            "idWidth (" + idWidth + ") is not greater than " + idWidth2min);
    assertTrue(idWidth <= idWidth2max, "idWidth (" + idWidth
            + ") is not narrower than " + idWidth2max);
  }

  @Test(
    groups =
    { "Functional", "testTask1" },
    dataProvider = "openFilesWithIdWidthChanges")
  public void testIdWidthNoChanges(String alignmentFilename, boolean wrap,
          int idWidth1min, int idWidth1max, int manualWidth,
          String structureFilename, String paeFilename,
          boolean secondaryStructureView, TFType temperatureFactorType,
          ViewerType viewerType, int idWidth2min, int idWidth2max)
  {
    AlignFrame af = new FileLoader()
            .LoadFileWaitTillLoaded(alignmentFilename, DataSourceType.FILE);
    try
    {
      Thread.sleep(200); // to allow alignment annotations to open
    } catch (InterruptedException e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    AlignViewport av = af.getCurrentView();

    int idWidth = 0;
    idWidth = av.getIdWidth();
    assertTrue(idWidth > idWidth1min,
            "idWidth (" + idWidth + ") is not greater than " + idWidth1min);
    assertTrue(idWidth < idWidth1max,
            "idWidth (" + idWidth + ") is not narrower than" + idWidth1max);

    AlignmentI al = av.getAlignment();
    SequenceI s = al.getSequenceAt(0);
    AlignmentPanel ap = af.alignPanel;

    // set width manually
    av.setIdWidth(manualWidth);
    ap.validateAnnotationDimensions(false);
    ap.paintAlignment(true, false);
    ap.getIdPanel().getIdCanvas().setManuallyAdjusted(true);

    idWidth = av.getIdWidth();
    assertEquals(idWidth, manualWidth,
            "idWidth is not set to the manually set width " + manualWidth);

    File structureFile = new File(structureFilename);
    File paeFile = new File(paeFilename);

    StructureViewer sv = StructureChooser.openStructureFileForSequence(null,
            null, ap, s, false, structureFile.getAbsolutePath(),
            temperatureFactorType, paeFile.getAbsolutePath(), false,
            secondaryStructureView, false, viewerType);

    idWidth = ap.getIdPanel().getWidth();// av.getIdWidth();
    idWidth = av.getIdWidth();
    assertEquals(idWidth, manualWidth,
            "idWidth is not set to the manually set width " + manualWidth
                    + " after adding structure annotations");
    assertFalse(idWidth > idWidth2min,
            "idWidth (" + idWidth + ") is greater than " + idWidth2min);
  }

  @DataProvider(name = "openFilesWithIdWidthChanges")
  public Object[][] openFilesWithIdWidthChanges()
  {
    /*
      String alignmentFilename,
      boolean wrap,
      int idWidth1min,
      int idWidth1max,
      int manualWidth, // ignored by testIdWidthChanges()
      String structureFilename,
      String paeFilename,
      boolean secondaryStructureView,
      TFType temperatureFactorType,
      ViewerType viewerType,
      int idWidth2min,
      int idWidth2max,
     */
    int idWidth2min = scaling == 1.0 ? 114 : 108;
    int idWidth2max = scaling == 1.0 ? 117 : 114; // was 130
    if (Platform.isMac())
    {
      idWidth2max = 122;
    }

    return new Object[][] {
        //
        /*
         */
        { "./test/files/annotation_label_width/sample.a2m", false, 50, 70,
            100,
            "./examples/test_fab41.result/test_fab41_unrelaxed_rank_1_model_3.pdb",
            "./examples/test_fab41.result/test_fab41_unrelaxed_rank_1_model_3_scores.json",
            true, TFType.PLDDT, null, idWidth2min, idWidth2max },
        { "./test/files/annotation_label_width/sample.a2m", true, 50, 70,
            100,
            "./examples/test_fab41.result/test_fab41_unrelaxed_rank_1_model_3.pdb",
            "./examples/test_fab41.result/test_fab41_unrelaxed_rank_1_model_3_scores.json",
            true, TFType.PLDDT, null, idWidth2min, idWidth2max },
        /*
         */
    };
  }

}
