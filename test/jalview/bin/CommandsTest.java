/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.bin;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.swing.SwingUtilities;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import jalview.gui.AlignFrame;
import jalview.gui.Desktop;
import jalview.gui.JvOptionPane;
import jalview.util.ArrayUtils;

public class CommandsTest
{
  private static final String testfiles = "test/jalview/bin/argparser/testfiles";

  @BeforeClass(alwaysRun = true)
  public static void setUpBeforeClass() throws Exception
  {
    Cache.loadProperties("test/jalview/gui/quitProps.jvprops");
    Date oneHourFromNow = new Date(
            System.currentTimeMillis() + 3600 * 1000);
    Cache.setDateProperty("JALVIEW_NEWS_RSS_LASTMODIFIED", oneHourFromNow);
  }

  @AfterClass(alwaysRun = true)
  public static void resetProps()
  {
    Cache.loadProperties("test/jalview/testProps.jvprops");
  }

  @BeforeClass(alwaysRun = true)
  public void setUpJvOptionPane()
  {
    JvOptionPane.setInteractiveMode(false);
    JvOptionPane.setMockResponse(JvOptionPane.CANCEL_OPTION);
  }

  @AfterMethod(alwaysRun = true)
  public void tearDown()
  {
    try
    {
      // occasionally we are blocked by Jmol redraws
      SwingUtilities.invokeAndWait(new Runnable()
      {

        @Override
        public void run()
        {
          Desktop.closeDesktop();
        }
      });
    } catch (Exception foo)
    {
      System.err.println("Failed during teardown with exception");
      foo.printStackTrace();
    }

  }

  public static void callJalviewMain(String[] args)
  {
    callJalviewMain(args, false);
  }

  public static void callJalviewMain(String[] args, boolean newJalview)
  {
    if (Jalview.getInstance() != null && !newJalview)
    {
      Jalview.getInstance().doMain(args);
    }
    else
    {
      Jalview.main(args);
    }
  }

  /* --setprops is currently disabled so this test won't work
  @Test(groups = "Functional")
  public void setpropsTest()
  {
    final String MOSTLY_HARMLESS = "MOSTLY_HARMLESS";
    String cmdLine = "--setprop=" + MOSTLY_HARMLESS + "=Earth";
    String[] args = cmdLine.split("\\s+");
    Jalview.main(args);
    Assert.assertEquals(Cache.getDefault(MOSTLY_HARMLESS, "Magrathea"),
            "Earth");
  }
  */

  @Test(
    groups =
    { "Functional", "testTask3" },
    dataProvider = "cmdLines",
    singleThreaded = true)

  public void commandsOpenTest(String cmdLine, boolean cmdArgs,
          int numFrames, String[] sequences)
  {
    try
    {
      String[] args = (cmdLine + " --gui").split("\\s+");
      callJalviewMain(args);
      Commands cmds = Jalview.getInstance().getCommands();
      Assert.assertNotNull(cmds);
      Assert.assertEquals(cmds.commandArgsProvided(), cmdArgs,
              "Commands were not provided in the args");
      Assert.assertEquals(cmds.argsWereParsed(), cmdArgs,
              "Overall command parse and operation is false");

      Assert.assertEquals(Desktop.getDesktopAlignFrames().length, numFrames,
              "Wrong number of AlignFrames");

      if (sequences != null)
      {
        Set<String> openedSequenceNames = new HashSet<>();
        AlignFrame[] afs = Desktop.getDesktopAlignFrames();
        for (AlignFrame af : afs)
        {
          openedSequenceNames.addAll(
                  af.getViewport().getAlignment().getSequenceNames());
        }
        for (String sequence : sequences)
        {
          Assert.assertTrue(openedSequenceNames.contains(sequence),
                  "Sequence '" + sequence
                          + "' was not found in opened alignment files: "
                          + cmdLine + ".\nOpened sequence names are:\n"
                          + String.join("\n", openedSequenceNames));
        }
      }

      Assert.assertFalse(
              lookForSequenceName("THIS_SEQUENCE_ID_DOESN'T_EXIST"));
    } catch (Exception x)
    {
      Assert.fail("Unexpected exception during commandsOpenTest", x);
    } finally
    {
      tearDown();

    }
  }

  @Test(
    groups =
    { "Functional", "testTask3" },
    dataProvider = "structureImageOutputFiles",
    singleThreaded = true)
  public void structureImageOutputTest(String cmdLine, String[] filenames)
          throws IOException
  {
    cleanupFiles(filenames);
    String[] args = (cmdLine).split("\\s+");
    try
    {
      callJalviewMain(args, true);
      Commands cmds = Jalview.getInstance().getCommands();
      Assert.assertNotNull(cmds);
      verifyIncreasingSize(cmdLine, filenames);
    } catch (Exception x)
    {
      Assert.fail("Unexpected exception during structureImageOutputTest",
              x);
    } finally
    {
      cleanupFiles(filenames);
      tearDown();
    }
  }

  /**
   * given two command lines, compare the output files produced - they should
   * exist and be equal in size
   */
  @Test(
    groups =
    { "Functional", "testTask3" },
    dataProvider = "compareHeadlessAndGUIOps",
    singleThreaded = true)
  public void headlessOrGuiImageOutputTest(String[] cmdLines,
          String[] filenames) throws IOException
  {
    cleanupFiles(filenames);
    try
    {
      for (String cmdLine : cmdLines)
      {
        CommandLineOperations.Worker runner = CommandLineOperations
                .getJalviewDesktopRunner(false, cmdLine, 1000);
        long timeOut = 10000;
        while (runner.isAlive() && timeOut > 0)
        {
          Thread.sleep(25);
          timeOut -= 25;
        }
      }

      verifyOrderedFileSet(cmdLines[0] + " vs " + cmdLines[1], filenames,
              false);

      verifySimilarEnoughImages(cmdLines[0] + " vs " + cmdLines[1],
              filenames, 0f, 0f);
    } catch (Exception x)
    {
      Assert.fail("Unexpected exception during structureImageOutputTest",
              x);
    } finally
    {
      cleanupFiles(filenames);
      tearDown();
    }
  }

  @DataProvider(name = "compareHeadlessAndGUIOps")
  public Object[][] compareHeadlessAndGUIOps()
  {
    return new Object[][] {
        new Object[]
        { new String[] { "--open examples/uniref50.fa "
                + "--structure [seqid=FER1_SPIOL,tempfac=plddt,showssannotations,structureviewer=jmol]"
                + "examples/AlphaFold/AF-P00221-F1-model_v4.pdb "
                + "--paematrix examples/AlphaFold/AF-P00221-F1-predicted_aligned_error_v4.json --image="
                + testfiles + "/"
                + "test-al-pae-ss-gui.png --overwrite --gui --props test/jalview/bin/commandsTest3.jvprops --quit",
            "--open examples/uniref50.fa "
                    + "--structure [seqid=FER1_SPIOL,tempfac=plddt,showssannotations,structureviewer=jmol]"
                    + "examples/AlphaFold/AF-P00221-F1-model_v4.pdb "
                    + "--paematrix examples/AlphaFold/AF-P00221-F1-predicted_aligned_error_v4.json --image="
                    + testfiles + "/"
                    + "test-al-pae-ss-nogui.png --overwrite --nogui --props test/jalview/bin/commandsTest3.jvprops" },
            new String[]
            { testfiles + "/test-al-pae-ss-gui.png",
                testfiles + "/test-al-pae-ss-nogui.png", } } };
  }

  private static void verifyIncreasingSize(String cmdLine,
          String[] filenames) throws Exception
  {
    verifyOrderedFileSet(cmdLine, filenames, true);
  }

  private static void verifyOrderedFileSet(String cmdLine,
          String[] filenames, boolean increasingSize) throws Exception
  {
    File lastFile = null;
    for (String filename : filenames)
    {
      File file = new File(filename);
      Assert.assertTrue(file.exists(), "File '" + filename
              + "' was not created by '" + cmdLine + "'");
      Assert.assertTrue(file.isFile(), "File '" + filename
              + "' is not a file from '" + cmdLine + "'");
      Assert.assertTrue(Files.size(file.toPath()) > 0, "File '" + filename
              + "' has no content from '" + cmdLine + "'");
      // make sure the successive output files get bigger!
      if (lastFile != null)
      {
        waitForLastWrite(file, 25);

        if (increasingSize)
        {
          Assert.assertTrue(
                  Files.size(file.toPath()) > Files.size(lastFile.toPath()),
                  "Expected " + file.toPath() + " to be larger than "
                          + lastFile.toPath());
        }
        else
        {
          Assert.assertEquals(Files.size(file.toPath()),
                  Files.size(lastFile.toPath()),
                  "New file " + file.toPath()
                          + " (actual size) not same as last file's size "
                          + lastFile.toString());
        }
      }
      // remember it for next file
      lastFile = file;
    }

  }

  private static void verifySimilarEnoughImages(String cmdLine,
          String[] filenames, float w_tolerance_pc, float h_tolerance_pc)
          throws Exception
  {
    int min_w = -1;
    int max_w = -1;
    int min_h = -1;
    int max_h = -1;
    for (String filename : filenames)
    {
      File file = new File(filename);
      Assert.assertTrue(file.exists(), "File '" + filename
              + "' was not created by '" + cmdLine + "'");
      Assert.assertTrue(file.isFile(), "File '" + filename
              + "' is not a file from '" + cmdLine + "'");
      Assert.assertTrue(Files.size(file.toPath()) > 0, "File '" + filename
              + "' has no content from '" + cmdLine + "'");

      BufferedImage img = ImageIO.read(file);
      if (img.getWidth() < min_w || min_w == -1)
      {
        min_w = img.getWidth();
      }
      if (img.getWidth() > max_w || max_w == -1)
      {
        max_w = img.getWidth();
      }
      if (img.getHeight() < min_h || min_h == -1)
      {
        min_h = img.getHeight();
      }
      if (img.getHeight() > max_h || max_h == -1)
      {
        max_h = img.getHeight();
      }
    }
    Assert.assertTrue(min_w > 0,
            "Minimum width is not positive (" + min_w + ")");
    Assert.assertTrue(max_w > 0,
            "Maximum width is not positive (" + max_w + ")");
    Assert.assertTrue(min_h > 0,
            "Minimum height is not positive (" + min_h + ")");
    Assert.assertTrue(max_h > 0,
            "Maximum height is not positive (" + max_h + ")");
    // tolerance
    Assert.assertTrue(100 * (max_w - min_w) / min_w <= w_tolerance_pc,
            "Width variation (" + (max_w - min_w)
                    + " not within tolerance (" + w_tolerance_pc
                    + "%) of minimum width (" + min_w + ")");
    if (max_w != min_w)
    {
      System.out.println("Widths within tolerance (" + w_tolerance_pc
              + "%), min_w=" + min_w + " < max_w=" + max_w);
    }
    Assert.assertTrue(100 * (max_h - min_h) / min_h <= h_tolerance_pc,
            "Height variation (" + (max_h - min_h)
                    + " not within tolerance (" + h_tolerance_pc
                    + "%) of minimum height (" + min_h + ")");
    if (max_h != min_h)
    {
      System.out.println("Heights within tolerance (" + h_tolerance_pc
              + "%), min_h=" + min_h + " < max_h=" + max_h);
    }
  }

  private static long waitForLastWrite(File file, int i) throws IOException
  {
    long lastSize, stableSize = Files.size(file.toPath());
    // wait around until we are sure the file has been completely written.
    do
    {
      lastSize = stableSize;
      try
      {
        Thread.sleep(i);
      } catch (Exception x)
      {
      }
      stableSize = Files.size(file.toPath());
    } while (stableSize != lastSize);
    return stableSize;
  }

  @Test(
    groups = "Functional",
    dataProvider = "argfileOutputFiles",
    singleThreaded = true)

  public void argFilesGlobAndSubstitutionsTest(String cmdLine,
          String[] filenames) throws IOException
  {
    cleanupFiles(filenames);
    String[] args = (cmdLine + " --gui").split("\\s+");
    try
    {
      callJalviewMain(args);
      Commands cmds = Jalview.getInstance().getCommands();
      Assert.assertNotNull(cmds);
      File lastFile = null;
      for (String filename : filenames)
      {
        File file = new File(filename);
        Assert.assertTrue(file.exists(), "File '" + filename
                + "' was not created by '" + cmdLine + "'");
        Assert.assertTrue(file.isFile(), "File '" + filename
                + "' is not a file from '" + cmdLine + "'");
        Assert.assertTrue(Files.size(file.toPath()) > 0, "File '" + filename
                + "' has no content from '" + cmdLine + "'");
        // make sure the successive output files get bigger!
        if (lastFile != null)
        {
          Assert.assertTrue(Files.size(file.toPath()) > Files
                  .size(lastFile.toPath()));
          System.out.println("this file: " + file + " +"
                  + Files.size(file.toPath()) + " greater than "
                  + Files.size(lastFile.toPath()));
        }
        // remember it for next file
        lastFile = file;
      }
    } catch (Exception x)
    {
      Assert.fail(
              "Unexpected exception during argFilesGlobAndSubstitutions",
              x);
    } finally
    {
      cleanupFiles(filenames);
      tearDown();
    }
  }

  @DataProvider(name = "structureImageOutputFiles")
  public Object[][] structureImageOutputFiles()
  {
    return new Object[][] {
        //
        /*
                */
        { "--gui --nonews --nosplash --open=./examples/test_fab41.result/sample.a2m "
                + "--structure=./examples/test_fab41.result/test_fab41_unrelaxed_rank_1_model_3.pdb "
                + "--structureimage=" + testfiles
                + "/structureimage0-1.png "
                + "--open=./examples/test_fab41.result/sample.a2m "
                + "--structure=./examples/test_fab41.result/test_fab41_unrelaxed_rank_1_model_3.pdb "
                + "--structureimage=" + testfiles
                + "/structureimage0-2.png --scale=1.5 "
                + "--open=./examples/test_fab41.result/sample.a2m "
                + "--structure=./examples/test_fab41.result/test_fab41_unrelaxed_rank_1_model_3.pdb "
                + "--structureimage=" + testfiles
                + "/structureimage0-3.png --scale=2.0 ",
            new String[]
            { testfiles + "/structureimage0-1.png",
                testfiles + "/structureimage0-2.png",
                testfiles + "/structureimage0-3.png" } },
        { "--headless --noquit --open=./examples/test_fab41.result/sample.a2m "
                + "--structure=./examples/test_fab41.result/test_fab41_unrelaxed_rank_1_model_3.pdb "
                + "--structureimage=" + testfiles
                + "/structureimage1-1.png "
                + "--open=./examples/test_fab41.result/sample.a2m "
                + "--structure=./examples/test_fab41.result/test_fab41_unrelaxed_rank_1_model_3.pdb "
                + "--structureimage=" + testfiles
                + "/structureimage1-2.png --scale=1.5 "
                + "--open=./examples/test_fab41.result/sample.a2m "
                + "--structure=./examples/test_fab41.result/test_fab41_unrelaxed_rank_1_model_3.pdb "
                + "--structureimage=" + testfiles
                + "/structureimage1-3.png --scale=2.0 ",
            new String[]
            { testfiles + "/structureimage1-1.png",
                testfiles + "/structureimage1-2.png",
                testfiles + "/structureimage1-3.png" } },
        { "--gui --nonews --nosplash --open examples/1gaq.txt --append ./examples/3W5V.pdb "
                + "--structure examples/1gaq.txt --seqid \"1GAQ|A\" "
                + "--structureimage " + testfiles
                + "/structureimage2-1gaq.png --structure examples/3W5V.pdb "
                + "--seqid \"3W5V|A\" --structureimage " + testfiles
                + "/structureimage2-3w5v.png --overwrite",

            new String[]
            { testfiles + "/structureimage2-3w5v.png",
                testfiles + "/structureimage2-1gaq.png", } },
        { "--headless --noquit --open ./examples/1gaq.txt --append ./examples/3W5V.pdb "
                + "--structure examples/1gaq.txt --seqid \"1GAQ|A\" "
                + "--structureimage " + testfiles
                + "/structureimage3-1gaq.png --structure examples/3W5V.pdb "
                + "--seqid \"3W5V|A\" --structureimage " + testfiles
                + "/structureimage3-3w5v.png --overwrite",

            new String[]
            { testfiles + "/structureimage3-3w5v.png",
                testfiles + "/structureimage3-1gaq.png", } }
        /*
                */
        //
    };

  }

  @DataProvider(name = "argfileOutputFiles")
  public Object[][] argfileOutputFiles()
  {
    return new Object[][] {
        //
        { "--gui --argfile=" + testfiles + "/**/*.txt", new String[]
        { testfiles + "/dir1/test1.png", testfiles + "/dir2/test1.png",
            testfiles + "/dir3/subdir/test0.png" } },
        { "--gui --argfile=" + testfiles + "/**/argfile.txt", new String[]
        { testfiles + "/dir1/test1.png", testfiles + "/dir2/test1.png" } },
        { "--gui --argfile=" + testfiles + "/dir*/argfile.txt", new String[]
        { testfiles + "/dir1/test1.png", testfiles + "/dir2/test1.png" } },
        { "--gui --initsubstitutions --append examples/uniref50.fa --image "
                + testfiles + "/{basename}.png",
            new String[]
            { testfiles + "/uniref50.png" } },
        { "--gui --append examples/uniref50.fa --nosubstitutions --image "
                + testfiles + "/{basename}.png",
            new String[]
            { testfiles + "/{basename}.png" } }
        //
    };

  }

  @DataProvider(name = "cmdLines")
  public Object[][] cmdLines()
  {
    String[] someUniref50Seqs = new String[] { "FER_CAPAA", "FER_CAPAN",
        "FER1_MAIZE", "FER1_SPIOL", "O80429_MAIZE" };
    String[] t1 = new String[] { "TEST1" };
    String[] t2 = new String[] { "TEST2" };
    String[] t3 = new String[] { "TEST3" };
    return new Object[][] {
        /*
        */
        { "--append=examples/uniref50.fa", true, 1, someUniref50Seqs },
        { "--append examples/uniref50.fa", true, 1, someUniref50Seqs },
        { "--append=examples/uniref50*.fa", true, 1, someUniref50Seqs },
        // NOTE we cannot use shell expansion in tests, so list all files!
        { "--append examples/uniref50.fa examples/uniref50_mz.fa", true, 1,
            someUniref50Seqs },
        { "--append=[new]examples/uniref50*.fa", true, 2,
            someUniref50Seqs },
        { "--open=examples/uniref50*.fa", true, 2, someUniref50Seqs },
        { "examples/uniref50.fa", true, 1, someUniref50Seqs },
        { "examples/uniref50.fa " + testfiles + "/test1.fa", true, 2,
            ArrayUtils.concatArrays(someUniref50Seqs, t1) },
        { "examples/uniref50.fa " + testfiles + "/test1.fa", true, 2, t1 },
        { "--gui --argfile=" + testfiles + "/argfile0.txt", true, 1,
            ArrayUtils.concatArrays(t1, t3) },
        { "--gui --argfile=" + testfiles + "/argfile*.txt", true, 5,
            ArrayUtils.concatArrays(t1, t2, t3) },
        { "--gui --argfile=" + testfiles + "/argfile.autocounter", true, 3,
            ArrayUtils.concatArrays(t1, t2) } };

  }

  public static boolean lookForSequenceName(String sequenceName)
  {
    AlignFrame[] afs = Desktop.getDesktopAlignFrames();
    for (AlignFrame af : afs)
    {
      for (String name : af.getViewport().getAlignment().getSequenceNames())
      {
        if (sequenceName.equals(name))
        {
          return true;
        }
      }
    }
    return false;
  }

  public static void cleanupFiles(String[] filenames)
  {
    for (String filename : filenames)
    {
      File file = new File(filename);
      if (file.exists())
      {
        file.delete();
      }
    }
  }

  private final String deleteDir = "test/deleteAfter";

  @Test(
    groups = "Functional",
    dataProvider = "allLinkedIdsData",
    singleThreaded = true)
  public void allLinkedIdsTest(String cmdLine, String[] filenames,
          String[] nonfilenames)
  {
    String[] args = (cmdLine + " --gui").split("\\s+");
    callJalviewMain(args);
    Commands cmds = Jalview.getInstance().getCommands();
    Assert.assertNotNull(cmds);
    for (String filename : filenames)
    {
      Assert.assertTrue(new File(filename).exists(),
              "File '" + filename + "' was not created");
    }
    cleanupFiles(filenames);
    if (nonfilenames != null)
    {
      for (String nonfilename : nonfilenames)
      {
        File nonfile = new File(nonfilename);
        Assert.assertFalse(nonfile.exists(),
                "File " + nonfilename + " exists when it shouldn't!");
      }
    }

    File deleteDirF = new File(deleteDir);
    if (deleteDirF.exists())
    {
      deleteDirF.delete();
    }
  }

  @DataProvider(name = "allLinkedIdsData")
  public Object[][] allLinkedIdsData()
  {
    return new Object[][] {
        //
        { "--gui --open=test/jalview/bin/argparser/testfiles/*.fa --substitutions --all --output={dirname}/{basename}.stk --close",
            new String[]
            { "test/jalview/bin/argparser/testfiles/test1.stk",
                "test/jalview/bin/argparser/testfiles/test2.stk",
                "test/jalview/bin/argparser/testfiles/test3.stk", },
            null },
        { "--gui --open=test/jalview/bin/argparser/testfiles/*.fa --substitutions --all --image={dirname}/{basename}.png --close",
            new String[]
            { "test/jalview/bin/argparser/testfiles/test1.png",
                "test/jalview/bin/argparser/testfiles/test2.png",
                "test/jalview/bin/argparser/testfiles/test3.png", },
            null },
        { "--gui --open=test/jalview/bin/argparser/testfiles/*.fa --all --output={dirname}/{basename}.stk --close",
            new String[]
            { "test/jalview/bin/argparser/testfiles/test1.stk",
                "test/jalview/bin/argparser/testfiles/test2.stk",
                "test/jalview/bin/argparser/testfiles/test3.stk", },
            new String[]
            { "test/jalview/bin/argparser/testfiles/dir1/test1.stk",
                "test/jalview/bin/argparser/testfiles/dir1/test2.stk",
                "test/jalview/bin/argparser/testfiles/dir2/test1.stk",
                "test/jalview/bin/argparser/testfiles/dir2/test2.stk",
                "test/jalview/bin/argparser/testfiles/dir2/test3.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test0.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test1.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test2.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test3.stk", }, },
        { "--gui --open=test/jalview/bin/argparser/**/*.fa --all --output={dirname}/{basename}.stk --close",
            new String[]
            { "test/jalview/bin/argparser/testfiles/test1.stk",
                "test/jalview/bin/argparser/testfiles/test2.stk",
                "test/jalview/bin/argparser/testfiles/test3.stk",
                "test/jalview/bin/argparser/testfiles/dir1/test1.stk",
                "test/jalview/bin/argparser/testfiles/dir1/test2.stk",
                "test/jalview/bin/argparser/testfiles/dir2/test1.stk",
                "test/jalview/bin/argparser/testfiles/dir2/test2.stk",
                "test/jalview/bin/argparser/testfiles/dir2/test3.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test0.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test1.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test2.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test3.stk", },
            null },
        { "--gui --open=test/jalview/bin/argparser/**/*.fa --output=*/*.stk --close",
            new String[]
            { "test/jalview/bin/argparser/testfiles/test1.stk",
                "test/jalview/bin/argparser/testfiles/test2.stk",
                "test/jalview/bin/argparser/testfiles/test3.stk",
                "test/jalview/bin/argparser/testfiles/dir1/test1.stk",
                "test/jalview/bin/argparser/testfiles/dir1/test2.stk",
                "test/jalview/bin/argparser/testfiles/dir2/test1.stk",
                "test/jalview/bin/argparser/testfiles/dir2/test2.stk",
                "test/jalview/bin/argparser/testfiles/dir2/test3.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test0.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test1.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test2.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test3.stk", },
            null },
        { "--gui --open=test/jalview/bin/argparser/testfiles/dir1/*.fa --open=test/jalview/bin/argparser/testfiles/dir2/*.fa --all --output=*/*.stk --close",
            new String[]
            { "test/jalview/bin/argparser/testfiles/dir1/test1.stk",
                "test/jalview/bin/argparser/testfiles/dir1/test2.stk",
                "test/jalview/bin/argparser/testfiles/dir2/test1.stk",
                "test/jalview/bin/argparser/testfiles/dir2/test2.stk",
                "test/jalview/bin/argparser/testfiles/dir2/test3.stk", },
            new String[]
            { "test/jalview/bin/argparser/testfiles/test1.stk",
                "test/jalview/bin/argparser/testfiles/test2.stk",
                "test/jalview/bin/argparser/testfiles/test3.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test0.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test1.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test2.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test3.stk", }, },
        { "--gui --open=test/jalview/bin/argparser/testfiles/dir1/*.fa --open=test/jalview/bin/argparser/testfiles/dir2/*.fa --output=*/*.stk --close",
            new String[]
            { "test/jalview/bin/argparser/testfiles/dir2/test1.stk",
                "test/jalview/bin/argparser/testfiles/dir2/test2.stk",
                "test/jalview/bin/argparser/testfiles/dir2/test3.stk", },
            new String[]
            { "test/jalview/bin/argparser/testfiles/test1.stk",
                "test/jalview/bin/argparser/testfiles/test2.stk",
                "test/jalview/bin/argparser/testfiles/test3.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test0.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test1.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test2.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test3.stk", }, },
        { "--gui --open=test/jalview/bin/argparser/testfiles/dir1/*.fa --open=test/jalview/bin/argparser/testfiles/dir2/*.fa --output={dirname}/{basename}.stk --close",
            new String[]
            { "test/jalview/bin/argparser/testfiles/dir2/test1.stk",
                "test/jalview/bin/argparser/testfiles/dir2/test2.stk",
                "test/jalview/bin/argparser/testfiles/dir2/test3.stk", },
            new String[]
            { "test/jalview/bin/argparser/testfiles/test1.stk",
                "test/jalview/bin/argparser/testfiles/test2.stk",
                "test/jalview/bin/argparser/testfiles/test3.stk",
                "test/jalview/bin/argparser/testfiles/dir1/test1.stk",
                "test/jalview/bin/argparser/testfiles/dir1/test2.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test0.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test1.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test2.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test3.stk", }, },
        { "--gui --open=test/jalview/bin/argparser/testfiles/dir1/*.fa --open=test/jalview/bin/argparser/testfiles/dir2/*.fa --output={dirname}/{basename}.stk --close",
            new String[]
            { "test/jalview/bin/argparser/testfiles/dir2/test1.stk",
                "test/jalview/bin/argparser/testfiles/dir2/test2.stk",
                "test/jalview/bin/argparser/testfiles/dir2/test3.stk", },
            new String[]
            { "test/jalview/bin/argparser/testfiles/test1.stk",
                "test/jalview/bin/argparser/testfiles/test2.stk",
                "test/jalview/bin/argparser/testfiles/test3.stk",
                "test/jalview/bin/argparser/testfiles/dir1/test1.stk",
                "test/jalview/bin/argparser/testfiles/dir1/test2.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test0.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test1.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test2.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test3.stk", }, },
        { "--gui --open=test/jalview/bin/argparser/testfiles/dir1/*.fa --output {dirname}/{basename}.stk --open=test/jalview/bin/argparser/testfiles/dir2/*.fa --output={dirname}/{basename}.aln --close",
            new String[]
            { "test/jalview/bin/argparser/testfiles/dir1/test1.stk",
                "test/jalview/bin/argparser/testfiles/dir1/test2.stk",
                "test/jalview/bin/argparser/testfiles/dir2/test1.aln",
                "test/jalview/bin/argparser/testfiles/dir2/test2.aln",
                "test/jalview/bin/argparser/testfiles/dir2/test3.aln", },
            new String[]
            { "test/jalview/bin/argparser/testfiles/test1.stk",
                "test/jalview/bin/argparser/testfiles/test2.stk",
                "test/jalview/bin/argparser/testfiles/test3.stk",
                "test/jalview/bin/argparser/testfiles/dir2/test1.stk",
                "test/jalview/bin/argparser/testfiles/dir2/test2.stk",
                "test/jalview/bin/argparser/testfiles/dir2/test3.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test0.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test1.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test2.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test3.stk",
                "test/jalview/bin/argparser/testfiles/test1.aln",
                "test/jalview/bin/argparser/testfiles/test2.aln",
                "test/jalview/bin/argparser/testfiles/test3.aln",
                "test/jalview/bin/argparser/testfiles/dir1/test1.aln",
                "test/jalview/bin/argparser/testfiles/dir1/test2.aln",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test0.aln",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test1.aln",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test2.aln",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test3.aln", }, },
        // --mkdirs
        { "--headless --open=test/jalview/bin/argparser/testfiles/dir1/*.fa --output "
                + deleteDir
                + "/{dirname}/{basename}.stk --open=test/jalview/bin/argparser/testfiles/dir2/*.fa --output="
                + deleteDir
                + "/{dirname}/{basename}.aln --close --all --mkdirs",
            new String[]
            { deleteDir
                    + "/test/jalview/bin/argparser/testfiles/dir1/test1.stk",
                deleteDir
                        + "/test/jalview/bin/argparser/testfiles/dir1/test2.stk",
                deleteDir
                        + "/test/jalview/bin/argparser/testfiles/dir2/test1.aln",
                deleteDir
                        + "/test/jalview/bin/argparser/testfiles/dir2/test2.aln",
                deleteDir
                        + "/test/jalview/bin/argparser/testfiles/dir2/test3.aln", },
            new String[]
            { "test/jalview/bin/argparser/testfiles/test1.stk",
                "test/jalview/bin/argparser/testfiles/test2.stk",
                "test/jalview/bin/argparser/testfiles/test3.stk",
                "test/jalview/bin/argparser/testfiles/dir2/test1.stk",
                "test/jalview/bin/argparser/testfiles/dir2/test2.stk",
                "test/jalview/bin/argparser/testfiles/dir2/test3.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test0.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test1.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test2.stk",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test3.stk",
                "test/jalview/bin/argparser/testfiles/test1.aln",
                "test/jalview/bin/argparser/testfiles/test2.aln",
                "test/jalview/bin/argparser/testfiles/test3.aln",
                "test/jalview/bin/argparser/testfiles/dir1/test1.aln",
                "test/jalview/bin/argparser/testfiles/dir1/test2.aln",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test0.aln",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test1.aln",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test2.aln",
                "test/jalview/bin/argparser/testfiles/dir3/subdir/test3.aln",
                deleteDir
                        + "test/jalview/bin/argparser/testfiles/test1.stk",
                deleteDir
                        + "test/jalview/bin/argparser/testfiles/test2.stk",
                deleteDir
                        + "test/jalview/bin/argparser/testfiles/test3.stk",
                deleteDir
                        + "test/jalview/bin/argparser/testfiles/dir2/test1.stk",
                deleteDir
                        + "test/jalview/bin/argparser/testfiles/dir2/test2.stk",
                deleteDir
                        + "test/jalview/bin/argparser/testfiles/dir2/test3.stk",
                deleteDir
                        + "test/jalview/bin/argparser/testfiles/dir3/subdir/test0.stk",
                deleteDir
                        + "test/jalview/bin/argparser/testfiles/dir3/subdir/test1.stk",
                deleteDir
                        + "test/jalview/bin/argparser/testfiles/dir3/subdir/test2.stk",
                deleteDir
                        + "test/jalview/bin/argparser/testfiles/dir3/subdir/test3.stk",
                deleteDir
                        + "test/jalview/bin/argparser/testfiles/test1.aln",
                deleteDir
                        + "test/jalview/bin/argparser/testfiles/test2.aln",
                deleteDir
                        + "test/jalview/bin/argparser/testfiles/test3.aln",
                deleteDir
                        + "test/jalview/bin/argparser/testfiles/dir1/test1.aln",
                deleteDir
                        + "test/jalview/bin/argparser/testfiles/dir1/test2.aln",
                deleteDir
                        + "test/jalview/bin/argparser/testfiles/dir3/subdir/test0.aln",
                deleteDir
                        + "test/jalview/bin/argparser/testfiles/dir3/subdir/test1.aln",
                deleteDir
                        + "test/jalview/bin/argparser/testfiles/dir3/subdir/test2.aln",
                deleteDir
                        + "test/jalview/bin/argparser/testfiles/dir3/subdir/test3.aln", }, },
        //
    };
  }

  @Test(
    groups =
    { "Functional", "testTask3" },
    dataProvider = "structureImageAnnotationsOutputFiles",
    singleThreaded = true)
  public void structureImageAnnotationsOutputTest(String cmdLine,
          String filename, int height) throws IOException
  {
    cleanupFiles(new String[] { filename });
    String[] args = (cmdLine).split("\\s+");
    callJalviewMain(args, true); // Create new instance of Jalview each time for
                                 // linkedIds
    BufferedImage img = ImageIO.read(new File(filename));
    Assert.assertEquals(height, img.getHeight(), "Output image '" + filename
            + "' is not in the expected height range, possibly because of the wrong number of annotations");

    cleanupFiles(new String[] { filename });
    tearDown();
  }

  @DataProvider(name = "structureImageAnnotationsOutputFiles")
  public Object[][] structureImageAnnotationsOutputFiles()
  {
    String filename = "test/jalview/bin/argparser/testfiles/test_annotations.png";
    return new Object[][] {
        // MUST use --noquit with --headless to avoid a System.exit()
        { "--noquit --headless --nonews --nosplash --open=./examples/uniref50.fa "
                + "--structure=examples/AlphaFold/AF-P00221-F1-model_v4.pdb "
                + "--seqid=FER1_SPIOL --structureviewer=jmol "
                + "--paematrix examples/AlphaFold/AF-P00221-F1-predicted_aligned_error_v4.json "
                + "--image=" + filename + " " + "--tempfac=plddt "
                + "--overwrite " //
                + "--noshowssannotations " + "--noshowannotations", //
            filename, //
            252 }, //
        { "--noquit --headless --nonews --nosplash --open=./examples/uniref50.fa "
                + "--structure=examples/AlphaFold/AF-P00221-F1-model_v4.pdb "
                + "--seqid=FER1_SPIOL --structureviewer=jmol "
                + "--paematrix examples/AlphaFold/AF-P00221-F1-predicted_aligned_error_v4.json "
                + "--image=" + filename + " " + "--tempfac=plddt "
                + "--overwrite " //
                + "--showssannotations " + "--noshowannotations", //
            filename, //
            368 }, //
        { "--noquit --headless --nonews --nosplash --open=./examples/uniref50.fa "
                + "--structure=examples/AlphaFold/AF-P00221-F1-model_v4.pdb "
                + "--seqid=FER1_SPIOL --structureviewer=jmol "
                + "--paematrix examples/AlphaFold/AF-P00221-F1-predicted_aligned_error_v4.json "
                + "--image=" + filename + " " + "--tempfac=plddt "
                + "--overwrite " //
                + "--noshowssannotations " + "--showannotations", //
            filename, //
            524 }, //
        { "--noquit --headless --nonews --nosplash --open=./examples/uniref50.fa "
                + "--structure=examples/AlphaFold/AF-P00221-F1-model_v4.pdb "
                + "--seqid=FER1_SPIOL --structureviewer=jmol "
                + "--paematrix examples/AlphaFold/AF-P00221-F1-predicted_aligned_error_v4.json "
                + "--image=" + filename + " " + "--tempfac=plddt "
                + "--overwrite " //
                + "--showssannotations " + "--showannotations", //
            filename, //
            660 }, //
        { "--gui --nonews --nosplash --open=./examples/uniref50.fa "
                + "--structure=examples/AlphaFold/AF-P00221-F1-model_v4.pdb "
                + "--seqid=FER1_SPIOL --structureviewer=jmol "
                + "--paematrix examples/AlphaFold/AF-P00221-F1-predicted_aligned_error_v4.json "
                + "--image=" + filename + " " + "--tempfac=plddt "
                + "--overwrite " //
                + "--noshowssannotations " + "--noshowannotations", //
            filename, //
            252 }, //
        { "--gui --nonews --nosplash --open=./examples/uniref50.fa "
                + "--structure=examples/AlphaFold/AF-P00221-F1-model_v4.pdb "
                + "--seqid=FER1_SPIOL --structureviewer=jmol "
                + "--paematrix examples/AlphaFold/AF-P00221-F1-predicted_aligned_error_v4.json "
                + "--image=" + filename + " " + "--tempfac=plddt "
                + "--overwrite " //
                + "--showssannotations " + "--noshowannotations", //
            filename, //
            368 }, //
        { "--gui --nonews --nosplash --open=./examples/uniref50.fa "
                + "--structure=examples/AlphaFold/AF-P00221-F1-model_v4.pdb "
                + "--seqid=FER1_SPIOL --structureviewer=jmol "
                + "--paematrix examples/AlphaFold/AF-P00221-F1-predicted_aligned_error_v4.json "
                + "--image=" + filename + " " + "--tempfac=plddt "
                + "--overwrite " //
                + "--noshowssannotations " + "--showannotations", //
            filename, //
            524 }, //
        { "--gui --nonews --nosplash --open=./examples/uniref50.fa "
                + "--structure=examples/AlphaFold/AF-P00221-F1-model_v4.pdb "
                + "--seqid=FER1_SPIOL --structureviewer=jmol "
                + "--paematrix examples/AlphaFold/AF-P00221-F1-predicted_aligned_error_v4.json "
                + "--image=" + filename + " " + "--tempfac=plddt "
                + "--overwrite " //
                + "--showssannotations " + "--showannotations", //
            filename, //
            660 }, //
    };
  }

}
