/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.io;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import static org.testng.AssertJUnit.assertNotNull;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import jalview.datamodel.AlignmentAnnotation;
import jalview.gui.AlignFrame;
import jalview.gui.AnnotationExporter;
import jalview.gui.JvOptionPane;

public class AnnotationExporterTest
{

  @BeforeClass(alwaysRun = true)
  public void setUpJvOptionPane()
  {
    JvOptionPane.setInteractiveMode(false);
    JvOptionPane.setMockResponse(JvOptionPane.CANCEL_OPTION);
  }

  @Test(groups = "Functional")
  public void testAnnotationExportAsCSV()
  {
    /*
     * JAL-4342 test that export all alignment annotation as CSV or as an annotations file DOES NOT produce empty text box/file
     */
    AlignFrame af = new FileLoader().LoadFileWaitTillLoaded(
            ">Seq1\nQRSIL\n>Seq2\nFTHND\n>Seq3\nRPVSL\n",
            DataSourceType.PASTE);
    AlignmentAnnotation[] annots = af.alignPanel.getAlignment()
            .getAlignmentAnnotation();

    assertTrue(annots.length > 1);
    // set up exporter
    AnnotationExporter ae = new AnnotationExporter(af.alignPanel);
    ae.exportAnnotations();
    ae.setExportAsCSV();

    String exported;
    exported = ae.getAnnotationsText();
    assertNotNull(exported);
    assertTrue(exported.contains(annots[0].label));
    assertTrue(exported.contains(annots[1].label));

    // just one annotation row
    ae.exportAnnotation(annots[1]);
    exported = ae.getAnnotationsText();
    assertNotNull(exported);
    assertFalse(exported.contains(annots[0].label));
    assertTrue(exported.contains(annots[1].label));

  }
}
