package jalview.io;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import jalview.bin.Cache;
import jalview.gui.AlignFrame;

public class JalviewFileChooserTest
{
  @Test(groups = { "Functional" }, dataProvider = "propertiesAndFormats")
  public void checkDefaultSaveFormat(String DEFAULT_SAVE_FILE_FORMAT,
          String ALWAYS_USE_DEFAULT_SAVE_FORMAT,
          FileFormat alignmentCurrentFormat,
          FileFormat alignFrameLastSavedFormat,
          String expectedDefaultFormatName)
  {
    String alignmentCurrentFormatName = alignmentCurrentFormat == null
            ? null
            : alignmentCurrentFormat.getName();

    AlignFrame.setLastAlignmentSavedFormat(alignFrameLastSavedFormat);

    setOrRemoveProperty(JalviewFileChooser.DEFAULT_SAVE_FORMAT_PROPERTY,
            DEFAULT_SAVE_FILE_FORMAT);
    setOrRemoveProperty(
            JalviewFileChooser.ALWAYS_USE_DEFAULT_SAVED_FORMAT_PROPERTY,
            ALWAYS_USE_DEFAULT_SAVE_FORMAT);

    String defaultFormat = JalviewFileChooser
            .defaultSaveFileFormat(alignmentCurrentFormatName);

    Assert.assertEquals(defaultFormat, expectedDefaultFormatName,
            "The calculated default format did not match the expected default format.");
  }

  @DataProvider(name = "propertiesAndFormats")
  public Object[][] propertiesAndFormats()
  {
    /**
     * String DEFAULT_SAVE_FILE_FORMAT, boolean/String
     * ALWAYS_USE_DEFAULT_SAVE_FORMAT, FileFormat alignmentCurrentFormat,
     * FileFormat alignFrameLastSavedFormat, String expectedDefaultFormat
     */
    return new Object[][] {
        // no properties

        { null, null, null, null, FileFormat.Fasta.getName() },
        { null, null, FileFormat.BLC, null, FileFormat.BLC.getName() },
        { null, null, FileFormat.BLC, FileFormat.PIR,
            FileFormat.BLC.getName() },
        { null, null, null, FileFormat.PIR, FileFormat.Fasta.getName() },
        // definite format property
        { "Stockholm", null, null, null, FileFormat.Stockholm.getName() },
        { "Stockholm", null, FileFormat.BLC, null,
            FileFormat.BLC.getName() },
        { "Stockholm", null, FileFormat.BLC, FileFormat.PIR,
            FileFormat.BLC.getName() },
        { "Stockholm", null, null, FileFormat.PIR,
            FileFormat.Stockholm.getName() },
        // always use definite format property
        { "Stockholm", "true", null, null, FileFormat.Stockholm.getName() },
        { "Stockholm", "true", FileFormat.BLC, null,
            FileFormat.Stockholm.getName() },
        { "Stockholm", "true", FileFormat.BLC, FileFormat.PIR,
            FileFormat.Stockholm.getName() },
        { "Stockholm", "true", null, FileFormat.PIR,
            FileFormat.Stockholm.getName() },
        // use last saved
        { JalviewFileChooser.USE_LAST_SAVED_FORMAT_VALUE, null, null, null,
            FileFormat.Fasta.getName() },
        { JalviewFileChooser.USE_LAST_SAVED_FORMAT_VALUE, null,
            FileFormat.BLC, null, FileFormat.BLC.getName() },
        { JalviewFileChooser.USE_LAST_SAVED_FORMAT_VALUE, null,
            FileFormat.BLC, FileFormat.PIR, FileFormat.BLC.getName() },
        { JalviewFileChooser.USE_LAST_SAVED_FORMAT_VALUE, null, null,
            FileFormat.PIR, FileFormat.PIR.getName() },
        // always use last saved
        { JalviewFileChooser.USE_LAST_SAVED_FORMAT_VALUE, "true", null,
            null, FileFormat.Fasta.getName() },
        { JalviewFileChooser.USE_LAST_SAVED_FORMAT_VALUE, "true",
            FileFormat.BLC, null, FileFormat.BLC.getName() },
        { JalviewFileChooser.USE_LAST_SAVED_FORMAT_VALUE, "true",
            FileFormat.BLC, FileFormat.PIR, FileFormat.PIR.getName() },
        { JalviewFileChooser.USE_LAST_SAVED_FORMAT_VALUE, "true", null,
            FileFormat.PIR, FileFormat.PIR.getName() },

        // by extension
        { "ALL_EXTENSIONS", null, null, null, "ALL_EXTENSIONS" },
        { "ALL_EXTENSIONS", null, FileFormat.BLC, null,
            FileFormat.BLC.getName() },
        { "ALL_EXTENSIONS", null, FileFormat.BLC, FileFormat.PIR,
            FileFormat.BLC.getName() },
        { "ALL_EXTENSIONS", null, null, FileFormat.PIR, "ALL_EXTENSIONS" },
        // always by extension
        { "ALL_EXTENSIONS", "true", null, null, "ALL_EXTENSIONS" },
        { "ALL_EXTENSIONS", "true", FileFormat.BLC, null,
            "ALL_EXTENSIONS" },
        { "ALL_EXTENSIONS", "true", FileFormat.BLC, FileFormat.PIR,
            "ALL_EXTENSIONS" },
        { "ALL_EXTENSIONS", "true", null, FileFormat.PIR,
            "ALL_EXTENSIONS" },

        //
    };
  }

  private static void setOrRemoveProperty(String key, String value)
  {
    if (value == null)
    {
      Cache.removeProperty(key);
    }
    else
    {
      Cache.setProperty(key, value);
    }
  }

}
