package jalview.util;

import java.io.File;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class LaunchUtilsTest
{
  @Test(groups = "Functional", dataProvider = "versionComparisons")
  public void testVersionComparisons(String v0, String v1, int parity)
  {
    int comparison = LaunchUtils.compareVersions(v0, v1);
    if (parity == 0)
    {
      Assert.assertEquals(comparison, parity,
              "These should be parsed as the same.");
    }
    else if (parity < 0)
    {
      Assert.assertTrue(comparison < 0, "These should be v0 < v1");
    }
    else if (parity > 0)
    {
      Assert.assertTrue(comparison > 0, "These should be v0 > v1");
    }
  }

  @DataProvider(name = "versionComparisons")
  public Object[][] versionComparisons()
  {
    /*
    String v0, // a version number
    String v1, // a version number
    int comparison, // -1 if v0 < v1, 0 if v0 == v1, +1 if v0 > v1
    */
    return new Object[][] {
        //
        /*
         */
        { "2", "2", 0 },
        { "1.2.3.4", "1.2.3.4", 0 },
        { "1.1.1.1", "1.1.1.1", 0 },
        { "1.2.3", "1.2", 1 },
        { "1.2.3", "1.2.3.4", -1 },
        { "1.2.hello.4", "1.2.world.4", -1 },
        { "2.0a", "2.0b", -1 },
        { "1", "2", -1 },
        { "2", "1", 1 },
        /*
         */
        //
    };
  }

  @Test(groups = "Functional", dataProvider = "getdownVersionComparisons")
  public void testGetdownVersionComparisons(String v0, String v1,
          int parity)
  {
    int comparison = LaunchUtils.compareGetdownLauncherJarVersions(v0, v1);
    if (parity == 0)
    {
      Assert.assertEquals(comparison, parity,
              "These should be parsed as the same.");
    }
    else if (parity < 0)
    {
      Assert.assertTrue(comparison < 0, "These should be v0 < v1");
    }
    else if (parity > 0)
    {
      Assert.assertTrue(comparison > 0, "These should be v0 > v1");
    }
  }

  @DataProvider(name = "getdownVersionComparisons")
  public Object[][] getdownVersionComparisons()
  {
    /*
    String v0, // a getdown style version number
    String v1, // a getdown style version number
    int comparison, // -1 if v0 < v1, 0 if v0 == v1, +1 if v0 > v1
    */
    return new Object[][] {
        //
        /*
         */
        { "2", "2", 0 },
        { "1.2.3.4", "1.2.3.4", 0 },
        { "1.1.1.1", "1.1.1.1", 0 },
        { "1.2.3", "1.2", 1 },
        { "1.2.3", "1.2.3.4", -1 },
        { "1.2.hello.4", "1.2.world.4", -1 },
        { "2.0a", "2.0b", -1 },
        { "1", "2", -1 },
        { "2", "1", 1 },
        { "1.8.3-1.4.0_JVL", "1.8.3-1.4.0_JVL", 0 },
        { "1.2.3-4.5.6-7.8.9_NOJVL", "1.2.3-4.5.6-7.8.9_JVL", 0 },
        { "1.2.3-4.6.0-7.8.9", "1.2.3-4.5.6-9.0.0", 1 },
        { "1.2.3-4.5.6-7.8.9", "1.2.3-4.5.6-9.0.0", -1 },
        { "1.3.0-4.5.6-7.8.9", "1.2.3-4.5.6-9.0.0", 1 },
        /*
         */
        //
    };
  }

  @Test(groups = "Functional", dataProvider = "jarVersions")
  public void testJarVersions(File f0, String version)
  {
    String implementationVersion = LaunchUtils
            .getJarImplementationVersion(f0);
    Assert.assertEquals(implementationVersion, version,
            "These should be the same.");
  }

  @DataProvider(name = "jarVersions")
  public Object[][] jarVersions()
  {
    File f0 = new File(
            "test/jalview/util/fake-getdown-launchers/fake-getdown-launcher-1.8.3-1.4.3.jar");
    File f1 = new File(
            "test/jalview/util/fake-getdown-launchers/fake-getdown-launcher-1.8.3-1.4.4.jar");
    File f2 = new File(
            "test/jalview/util/fake-getdown-launchers/fake-getdown-launcher-1.8.3-1.5.0.jar");
    File f3 = new File(
            "test/jalview/util/fake-getdown-launchers/fake-getdown-launcher-1.8.4-1.5.0.jar");
    /*
    File f0, // a jar file
    String version, // the Implementation-Version found in the jar's META-INF/MANIFEST.MF
    */
    return new Object[][] {
        //
        /*
         */
        { f0, "1.8.3-1.4.3_JVL" },
        { f1, "1.8.3-1.4.4_JVL" },
        { f2, "1.8.3-1.5.0_JVL" },
        { f3, "1.8.4-1.5.0_JVL" },
        /*
         */
        //
    };
  }

  @Test(groups = "Functional", dataProvider = "jarVersionComparisons")
  public void testJarVersionComparisons(File f0, File f1, int parity)
  {
    int comparison = LaunchUtils.compareGetdownLauncherJarVersions(f0, f1);
    if (parity == 0)
    {
      Assert.assertEquals(comparison, parity,
              "These should be parsed as the same.");
    }
    else if (parity < 0)
    {
      Assert.assertTrue(comparison < 0, "These should be f0 < f1");
    }
    else if (parity > 0)
    {
      Assert.assertTrue(comparison > 0, "These should be f0 > f1");
    }
  }

  @DataProvider(name = "jarVersionComparisons")
  public Object[][] jarVersionComparisons()
  {
    File f0 = new File(
            "test/jalview/util/fake-getdown-launchers/fake-getdown-launcher-1.8.3-1.4.3.jar");
    File f1 = new File(
            "test/jalview/util/fake-getdown-launchers/fake-getdown-launcher-1.8.3-1.4.4.jar");
    File f2 = new File(
            "test/jalview/util/fake-getdown-launchers/fake-getdown-launcher-1.8.3-1.5.0.jar");
    File f3 = new File(
            "test/jalview/util/fake-getdown-launchers/fake-getdown-launcher-1.8.4-1.5.0.jar");
    /*
    File f0, // a jar file (with getdown style Implementation-Version in the META-INF/MANIFEST.MF)
    File f1, // a jar file (with getdown style Implementation-Version in the META-INF/MANIFEST.MF)
    int comparison, // -1 if f0 < f1, 0 if f0 == f1, +1 if f0 > f1
    */
    return new Object[][] {
        //
        /*
         */
        { f0, f0, 0 },
        { f1, f1, 0 },
        { f2, f2, 0 },
        { f3, f3, 0 },
        { f1, f0, 1 },
        { f2, f1, 1 },
        { f3, f2, 1 },
        { f3, f0, 1 },
        { f0, f1, -1 },
        { f1, f2, -1 },
        { f2, f3, -1 },
        { f0, f3, -1 },
        /*
         */
        //
    };
  }
}
