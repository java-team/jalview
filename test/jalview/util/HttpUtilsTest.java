package jalview.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Locale;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class HttpUtilsTest
{
  @Test(groups = { "Network" }, dataProvider = "urlTargetsAndDestinations")
  public void testFollowConnection(String targetUrl, String finalUrl,
          String notUsed0, String notUsed1, boolean followAnyway)
          throws IOException
  {
    URL tUrl = new URL(targetUrl);
    URL fUrl = new URL(finalUrl);
    HttpURLConnection conn1 = HttpUtils.followConnection(
            (HttpURLConnection) tUrl.openConnection(), followAnyway);
    URL url1 = conn1.getURL();
    Assert.assertEquals(url1, fUrl, "Final URL is not the same.");
  }

  @Test(groups = { "Network" }, dataProvider = "urlTargetsAndDestinations")
  public void testOpenConnection(String targetUrl, String finalUrl,
          String notUsed0, String notUsed1, boolean followAnyway)
          throws IOException
  {
    URL tUrl = new URL(targetUrl);
    URL fUrl = new URL(finalUrl);
    HttpURLConnection conn1 = (HttpURLConnection) HttpUtils
            .openConnection(tUrl, followAnyway);
    URL url1 = conn1.getURL();
    Assert.assertEquals(url1, fUrl, "Final URL is not the same.");
  }

  @Test(groups = { "Network" }, dataProvider = "urlTargetsAndDestinations")
  public void testOpenStream(String targetUrl, String finalUrl,
          String inFirstLine, String inDocument, boolean followAnyway)
          throws IOException
  {
    URL tUrl = new URL(targetUrl);
    URL fUrl = new URL(finalUrl);
    InputStream is1 = HttpUtils.openStream(tUrl, followAnyway);
    BufferedReader br1 = new BufferedReader(new InputStreamReader(is1));
    String firstLine = br1.readLine().toLowerCase(Locale.ROOT);
    Assert.assertTrue(
            firstLine.contains(inFirstLine.toLowerCase(Locale.ROOT)),
            "First line of text '" + firstLine + "' does not contain '"
                    + inFirstLine + "'");
    String inDocumentLC = inDocument.toLowerCase(Locale.ROOT);
    boolean found = false;
    String line = null;
    while ((line = br1.readLine()) != null)
    {
      if (line.toLowerCase(Locale.ROOT).contains(inDocumentLC))
      {
        found = true;
        break;
      }
    }
    Assert.assertTrue(found,
            "Text '" + inDocument + "' not found in '" + finalUrl + "'");
  }

  @DataProvider(name = "urlTargetsAndDestinations")
  public Object[][] urlTargetsAndDestinations()
  {
    /*
    String targetUrl, // the URL you ask for
    String finalUrl, // the URL you end up at
    String foundInFirstLine, // some text found in the first line
    String foundInDocument, // some text found in the document (and won't be in an error page)
    boolean followAnyway, // whether to follow redirects even if they're not http->https
    */
    return new Object[][] {
        //
        /*
         */
        { "http://jalview.org/", "https://www.jalview.org/", "<!doctype",
            "Jalview is a", false },
        { "http://www.jalview.org/", "https://www.jalview.org/",
            "<!doctype", "Jalview is a", false },
        { "https://jalview.org/", "https://jalview.org/", "<!doctype",
            "Jalview is a", false },
        { "http://jalview.org/", "https://www.jalview.org/", "<!doctype",
            "Jalview is a", true },
        { "http://www.jalview.org/", "https://www.jalview.org/",
            "<!doctype", "Jalview is a", true },
        { "https://jalview.org", "https://www.jalview.org/", "<!doctype",
            "Jalview is a", true },
        /*
         */
        //
    };
  }

}
