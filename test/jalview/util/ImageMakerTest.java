/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.util;

import org.testng.Assert;
import org.testng.annotations.Test;

import jalview.bin.Cache;
import jalview.util.imagemaker.BitmapImageSizing;

public class ImageMakerTest
{
  @Test(groups = { "Functional" })
  public void testParseScaleWidthHeightStrings()
  {
    Cache.setPropsAreReadOnly(true);
    Cache.loadProperties("test/jalview/bin/testProps.jvprops");

    Cache.removeProperty(BitmapImageSizing.BITMAP_SCALE);
    Cache.removeProperty(BitmapImageSizing.BITMAP_HEIGHT);
    Cache.removeProperty(BitmapImageSizing.BITMAP_WIDTH);

    BitmapImageSizing bis = null;

    // No defaults set, 3 values given. Should be the 3 values.
    bis = ImageMaker.parseScaleWidthHeightStrings("1.2", "3", "4");
    Assert.assertEquals(bis.scale(), 1.2f,
            "scale not parsed and set to value given");
    Assert.assertEquals(bis.width(), 3,
            "width not parsed and set to value given");
    Assert.assertEquals(bis.height(), 4,
            "height not parsed and set to value given");

    // No defaults set, 1 value given. Should be the 1 value and 2 0s.
    bis = ImageMaker.parseScaleWidthHeightStrings("1.2", null, null);
    Assert.assertEquals(bis.scale(), 1.2f,
            "scale not parsed and set to value given");
    Assert.assertEquals(bis.width(), 0, "width not parsed and set to 0");
    Assert.assertEquals(bis.height(), 0, "height not parsed and set to 0");

    // No defaults set, 1 value given. Should be the 1 value and 2 0s. (checking
    // the other value)
    bis = ImageMaker.parseScaleWidthHeightStrings(null, "1", null);
    Assert.assertEquals(bis.scale(), 0f, "scale not parsed and set to 0f");
    Assert.assertEquals(bis.width(), 1,
            "width not parsed and set to value given");
    Assert.assertEquals(bis.height(), 0, "height not parsed and set to 0");

    // No defaults set, no values given, these should first look at defaults and
    // then set all to 0
    bis = ImageMaker.parseScaleWidthHeightStrings(null, null, null);
    Assert.assertEquals(bis.scale(), 0f,
            "scale not parsed and set to undefined default 0f");
    Assert.assertEquals(bis.width(), 0,
            "width not parsed and set to undefined default 0");
    Assert.assertEquals(bis.height(), 0,
            "height not parsed and set to undefined default 0");

    Cache.setProperty(BitmapImageSizing.BITMAP_HEIGHT, "1");
    // 1 default set, bis should detect this
    Assert.assertEquals(bis.scale(), 0f,
            "scale not parsed and set to undefined default 0f");
    Assert.assertEquals(bis.width(), 0,
            "width not parsed and set to undefined default 0");
    Assert.assertEquals(bis.height(), 1,
            "height not parsed and set to default 1");

    Cache.setProperty(BitmapImageSizing.BITMAP_SCALE, "3.4");
    Cache.setProperty(BitmapImageSizing.BITMAP_WIDTH, "2");
    // Now all 3 defaults set, bis should detect this
    Assert.assertEquals(bis.scale(), 3.4f,
            "scale not parsed and set to undefined default 3.2f");
    Assert.assertEquals(bis.width(), 2,
            "width not parsed and set to undefined default 2");
    Assert.assertEquals(bis.height(), 1,
            "height not parsed and set to default 1");

    // 3 defaults set, and 3 values given, should use the 3 values
    bis = ImageMaker.parseScaleWidthHeightStrings("1.2", "3", "4");
    Assert.assertEquals(bis.scale(), 1.2f,
            "scale not parsed and set to value given");
    Assert.assertEquals(bis.width(), 3,
            "width not parsed and set to value given");
    Assert.assertEquals(bis.height(), 4,
            "height not parsed and set to value given");

    // 3 defaults set, and 1 value given, should use the 1 value and 2 0s
    bis = ImageMaker.parseScaleWidthHeightStrings("1.2", null, null);
    Assert.assertEquals(bis.scale(), 1.2f,
            "scale not parsed and set to value given");
    Assert.assertEquals(bis.width(), 0,
            "width not parsed and set to undefined 0");
    Assert.assertEquals(bis.height(), 0,
            "height not parsed and set to undefined 0");

    // 3 defaults set, and 1 value given, should use the 1 value and 2 0s
    bis = ImageMaker.parseScaleWidthHeightStrings(null, null, "5");
    Assert.assertEquals(bis.scale(), 0f,
            "scale not parsed and set to undefined 0f");
    Assert.assertEquals(bis.width(), 0,
            "width not parsed and set to undefined 0");
    Assert.assertEquals(bis.height(), 5,
            "height not parsed and set to value given");

    // 3 defaults set, and no values given, should use the 3 default values
    bis = ImageMaker.parseScaleWidthHeightStrings(null, null, null);
    Assert.assertEquals(bis.scale(), 3.4f,
            "scale not parsed and set to undefined default 3.4f");
    Assert.assertEquals(bis.width(), 2,
            "width not parsed and set to undefined default 2");
    Assert.assertEquals(bis.height(), 1,
            "height not parsed and set to default 1");
  }
}
