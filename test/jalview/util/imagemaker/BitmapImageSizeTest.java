/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.util.imagemaker;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

import jalview.bin.Cache;

public class BitmapImageSizeTest
{
  @Test(groups = { "Functional" })
  public void testCacheSettingsRecovery()
  {
    Cache.setPropsAreReadOnly(true);
    Cache.loadProperties("test/jalview/bin/testProps.jvprops");

    Cache.removeProperty(BitmapImageSizing.BITMAP_HEIGHT);
    Cache.removeProperty(BitmapImageSizing.BITMAP_SCALE);
    Cache.removeProperty(BitmapImageSizing.BITMAP_WIDTH);

    BitmapImageSizing def = BitmapImageSizing.defaultBitmapImageSizing();
    BitmapImageSizing zero = BitmapImageSizing.nullBitmapImageSizing();

    assertEquals(def.height(), zero.height());
    assertEquals(def.width(), zero.width());
    assertEquals(def.scale(), zero.scale());

    Cache.setProperty(BitmapImageSizing.BITMAP_HEIGHT, "120");
    Cache.setProperty(BitmapImageSizing.BITMAP_SCALE, "24");
    Cache.setProperty(BitmapImageSizing.BITMAP_WIDTH, "360");

    // default now updates dynamically
    // def = BitmapImageSizing.defaultBitmapImageSizing();

    assertEquals(def.height(), 120);
    assertEquals(def.width(), 360);
    assertEquals(def.scale(), 24f);

    Cache.removeProperty(BitmapImageSizing.BITMAP_WIDTH);

    // def = BitmapImageSizing.defaultBitmapImageSizing();
    assertEquals(def.height(), 120);
    assertEquals(def.width(), zero.width());
    assertEquals(def.scale(), 24f);
  }
}
