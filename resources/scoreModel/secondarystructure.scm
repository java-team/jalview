ScoreMatrix SECONDARYSTRUCTURE
#
# The SECONDARY STRUCTURE substitution matrix, as in <paper name>
# The first line declares a ScoreMatrix with the name SECONDARYSTRUCTURE (shown in menus)
#
# Scores are not symbol case sensitive, unless column(s) are provided for lower case characters
# The 'guide symbol' at the start of each row of score values is optional
# Values may be integer or floating point, delimited by tab, space, comma or combinations
#
	 E	 H	 C	 * 
E	 1	 0	 0	 0
H	 0	 1	 0	 0
C	 0	 0	 1	 0
*	 0	 0	 0	 1