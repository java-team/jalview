/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import jalview.bin.Cache;
import jalview.bin.argparser.Arg;
import jalview.io.FileFormatI;
import jalview.io.FileFormats;

public class ArgParserUtils
{
  public static Set<String> alignmentExtensions = null;

  public static Set<String> annotationsExtensions = null;

  public static Set<String> featuresExtensions = null;

  public static Set<String> treeExtensions = null;

  public static List<Arg> argSet = null;

  public static Map<Arg, Set<String>> argExtensionsMap = null;

  public static void preProcessArgs(List<String> filenames)
  {
    processFilenames(filenames, true);
  }

  public static Map<String, BaseInfo> processFilenames(
          List<String> filenames, boolean addArgs)
  {
    return processFilenames(filenames, addArgs, null);
  }

  public static Map<String, BaseInfo> processFilenames(
          List<String> filenames, boolean addArgs, List<Object> files)
  {
    // Running through the arguments to look for '-arg' or '--arg' should
    // already have happened, not doing it again.
    if (alignmentExtensions == null)
    {
      setValidExtensions();
    }

    Set<String> filesSet = new HashSet<>(filenames);

    Map<String, BaseInfo> baseInfoMap = new HashMap<>();

    // we make a copy to run through, and delete associated filenames from the
    // original
    final List<String> filenamesCopy = new ArrayList<>(filenames);
    for (String filename : filenamesCopy)
    {
      if (filename == null)
      {
        continue;
      }
      String ext = FileUtils.getExtension(filename);
      if (ext != null && ext.length() > 0
              && alignmentExtensions.contains(ext))
      {
        BaseInfo bi = new BaseInfo(filename);

        // this includes the dot at the end of the basename
        String base = FileUtils.getBase(filename);

        for (Arg arg : argSet)
        {
          for (String possibleExt : argExtensionsMap.get(arg))
          {
            String possibleFile = base + possibleExt;
            if (filesSet.contains(possibleFile))
            {
              bi.putAssociatedFile(arg, possibleFile);
              int filePos = filenames.indexOf(possibleFile);
              filenames.remove(possibleFile);

              // also remove File/String object from files if given
              if (files != null && files.get(filePos) != null
                      && possibleFile.equals(files.get(filePos).toString()))
              {
                files.remove(filePos);
              }
              break;
            }
          }
        }

        baseInfoMap.put(filename, bi);
      }
    }

    if (addArgs)
    {
      // now we go through the saved associated files and add them back in to
      // the right places with the appropriate argument
      for (String filename : baseInfoMap.keySet())
      {
        BaseInfo bi = baseInfoMap.get(filename);

        int pos = filenames.indexOf(filename);
        if (bi.associatedFiles != null)
        {
          for (Arg a : bi.associatedFiles.keySet())
          {
            String associatedFile = bi.associatedFiles.get(a);
            if (associatedFile == null)
            {
              // shouldn't happen!
              continue;
            }
            filenames.add(pos + 1, a.argString());
            filenames.add(pos + 2,
                    HttpUtils.equivalentJalviewUrl(associatedFile));
          }
        }
        // add an --open arg to separate from other files
        filenames.add(pos, Arg.OPEN.argString());
      }
    }

    return baseInfoMap;
  }

  private static void setValidExtensions()
  {
    alignmentExtensions = new HashSet<>();
    FileFormats ffs = FileFormats.getInstance();
    List<String> validFormats = ffs.getReadableFormats();

    for (String fname : validFormats)
    {
      FileFormatI tff = ffs.forName(fname);
      String[] extensions = tff.getExtensions().split(",");
      for (String ext : extensions)
      {
        alignmentExtensions.add(ext.toLowerCase(Locale.ROOT));
      }
    }

    annotationsExtensions = new HashSet<>();
    for (String ext : Cache.getDefault(
            "ARGPREPROCESSORANNOTATIONSEXTENSIONS",
            "annotation,annotations,jvannotation,jvannotations,gff,gff2,gff3")
            .split(","))
    {
      annotationsExtensions.add(ext);
    }

    featuresExtensions = new HashSet<>();
    for (String ext : Cache.getDefault("ARGPREPROCESSORFEATURESEXTENSIONS",
            "feature,features,jvfeature,jvfeatures").split(","))
    {
      featuresExtensions.add(ext);
    }

    treeExtensions = new HashSet<>();
    for (String ext : Cache.getDefault("ARGPREPROCESSORTREEEXTENSIONS",
            "tree,tre,newick,nwk").split(","))
    {
      treeExtensions.add(ext);
    }

    argSet = new ArrayList<>();
    argSet.add(Arg.ANNOTATIONS);
    argSet.add(Arg.FEATURES);
    argSet.add(Arg.TREE);

    argExtensionsMap = new HashMap<>();
    argExtensionsMap.put(Arg.ANNOTATIONS, annotationsExtensions);
    argExtensionsMap.put(Arg.FEATURES, featuresExtensions);
    argExtensionsMap.put(Arg.TREE, treeExtensions);
  }

  public static class BaseInfo
  {
    String filename;

    Map<Arg, String> associatedFiles = null;

    BaseInfo(String filename)
    {
      this.filename = filename;
    }

    void putAssociatedFile(Arg a, String file)
    {
      if (associatedFiles == null)
      {
        associatedFiles = new HashMap<>();
      }
      associatedFiles.put(a, file);
    }

    public Map<Arg, String> getAssociatedFilesMap()
    {
      return associatedFiles;
    }
  }

}