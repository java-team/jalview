/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ErrorLog
{
  private static boolean hasConsole = true;

  private static Class console = null;

  private static Method initLogger = null;

  private static Method errPrintln = null;

  private static Method outPrintln = null;

  private static String prefix = null;

  private static boolean quiet = false;

  public static void setHasConsole(boolean b)
  {
    hasConsole = b;
  }

  public static void setQuiet(boolean b)
  {
    quiet = b;
  }

  public static void setPrefix(String s)
  {
    prefix = s;
  }

  public static void outPrintln(String message)
  {
    println(message, false);
  }

  public static void errPrintln(String message)
  {
    println(message, true);
  }

  public static void println(String message, boolean err)
  {
    println(message, err, true);
  }

  public static void println(String message0, boolean err,
          boolean thisHasConsole)
  {
    if (!err && quiet)
    {
      return;
    }
    String message = prefix == null ? message0 : prefix + message0;
    if (thisHasConsole && hasConsole)
    {
      try
      {
        if (console == null)
        {
          Class console = Class.forName("jalview.bin.Console");
        }
        if (console == null)
        {
          hasConsole = false;
        }
        else
        {
          if (initLogger == null && console != null)
          {
            initLogger = console.getMethod("initLogger");
          }
          hasConsole = console == null || initLogger == null
                  || (Boolean) initLogger.invoke(null);
          if (hasConsole && console != null)
          {
            if (err)
            {
              if (errPrintln == null)
              {
                errPrintln = console.getMethod("errPrintln", String.class);
              }
              errPrintln.invoke(null, message);
            }
            else
            {
              if (outPrintln == null)
              {
                outPrintln = console.getMethod("outPrintln", String.class);
              }
              outPrintln.invoke(null, message);
            }
          }
        }
      } catch (ClassNotFoundException | NoSuchMethodException e)
      {
        hasConsole = false;
        System.err.println(
                "jalview.util.ErrorLog has no jalview.bin.Console.initLogger(). Using System.err and System.out.");
      } catch (IllegalAccessException | IllegalArgumentException
              | InvocationTargetException e)
      {
        hasConsole = false;
        System.err.println(
                "jalview.util.ErrorLog had a problem calling a method of jalview.bin.Console.  Using System.err and System.out.");
      } catch (Exception e)
      {
        e.printStackTrace();
      } catch (NoClassDefFoundError t)
      {
        hasConsole = false;
        System.err.println(
                "jalview.util.ErrorLog has no jalview.bin.Console. Using System.err and System.out.");
      }
    }
    if (!(thisHasConsole && hasConsole))
    {
      if (err)
      {
        System.err.println(message);
      }
      else
      {
        System.out.println(message);

      }
    }
  }
}
