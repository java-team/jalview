/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.util;

import java.util.Map;

public class MapUtils
{
  /**
   * Return the value of the first key that exists in the map and has a non-null
   * value
   */
  public static <K, V> V getFirst(Map<K, V> map, K... keys)
  {
    return getFirst(false, map, keys);
  }

  /**
   * Return the value of the first key that exists in the map - optionally
   * limiting to only returning non-null values for first extant key encountered
   */
  public static <K, V> V getFirst(boolean nonNull, Map<K, V> map, K... keys)
  {
    for (K key : keys)
    {
      if (map.containsKey(key))
      {
        if (!(nonNull && (map.get(key) == null)))
        {
          return map.get(key);
        }
        else if (!nonNull)
        {
          return map.get(key);
        }
      }
    }
    return null;
  }

  /**
   * peeks in to the map and returns true if one of a bunch of keys is contained
   * in it
   * 
   * @param <K>
   * @param map
   * @param keys
   * @return
   */
  public static <K> boolean containsAKey(Map<K, ?> map, K... keys)
  {
    for (K key : keys)
    {
      if (map.containsKey(key))
        return true;
    }
    return false;
  }
}
