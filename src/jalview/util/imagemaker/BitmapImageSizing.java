/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.util.imagemaker;

import jalview.bin.Cache;

public class BitmapImageSizing
{
  private final float scale;

  private final int width;

  private final int height;

  private boolean isDefault = false;

  public BitmapImageSizing(float scale, int width, int height,
          boolean isDefault)
  {
    this.scale = scale;
    this.width = width;
    this.height = height;
    this.isDefault = isDefault;
  }

  public boolean isNull()
  {
    return scale == 0.0f && width == 0 && height == 0;
  }

  public static BitmapImageSizing nullBitmapImageSizing()
  {
    return new BitmapImageSizing(0.0f, 0, 0, false);
  }

  public static final String BITMAP_SCALE = "BITMAP_SCALE";

  public static final String BITMAP_HEIGHT = "BITMAP_HEIGHT";

  public static final String BITMAP_WIDTH = "BITMAP_WIDTH";

  /**
   * 
   * @return bean configured from Cache keys
   */
  public static BitmapImageSizing defaultBitmapImageSizing()
  {
    return new BitmapImageSizing(0f, 0, 0, true);
  }

  private float defaultScale()
  {
    return Cache.getDefault(BITMAP_SCALE, 0f);
  }

  private int defaultWidth()
  {
    return Cache.getDefault(BITMAP_WIDTH, 0);
  }

  private int defaultHeight()
  {
    return Cache.getDefault(BITMAP_HEIGHT, 0);
  }

  public float scale()
  {
    return isDefault() ? defaultScale() : scale;
  }

  public int width()
  {
    return isDefault() ? defaultWidth() : width;
  }

  public int height()
  {
    return isDefault() ? defaultHeight() : height;
  }

  public boolean isDefault()
  {
    return isDefault;
  }
}
