/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.util;

import jalview.bin.Cache;

public class UserAgent
{

  public static String getUserAgent(String className)
  {
    StringBuilder sb = new StringBuilder();
    sb.append("Jalview");
    sb.append('/');
    sb.append(Cache.getDefault("VERSION", "Unknown"));
    sb.append(" (");
    sb.append(System.getProperty("os.name"));
    sb.append("; ");
    sb.append(System.getProperty("os.arch"));
    sb.append(' ');
    sb.append(System.getProperty("os.name"));
    sb.append(' ');
    sb.append(System.getProperty("os.version"));
    sb.append("; ");
    sb.append("java/");
    sb.append(System.getProperty("java.version"));
    sb.append("; ");
    sb.append("jalview/");
    sb.append(ChannelProperties.getProperty("channel"));
    if (className != null)
    {
      sb.append("; ");
      sb.append(className);
    }
    String installation = Cache.applicationProperties
            .getProperty("INSTALLATION");
    if (installation != null)
    {
      sb.append("; ");
      sb.append(installation);
    }
    sb.append(')');
    sb.append(" help@jalview.org");
    return sb.toString();
  }

  public static String getUserAgent()
  {
    return getUserAgent(null);
  }

}
