/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.util;

import java.util.HashMap;
import java.util.Map;

/**
 * A class to hold constants relating to Url links used in Jalview
 */
public class Constants
{

  // character used to represent secondary structures
  public static final char HELIX = 'H';

  public static final char SHEET = 'E';

  public static final char COIL = 'C';

  // label in secondary structure annotation data model from 3d structures
  public static final String SS_ANNOTATION_LABEL = "Secondary Structure";

  // label in secondary structure annotation data model from JPred
  public static final String SS_ANNOTATION_FROM_JPRED_LABEL = "jnetpred";

  public static final Map<String, String> SECONDARY_STRUCTURE_LABELS = new HashMap<>();
  static
  {
    SECONDARY_STRUCTURE_LABELS.put(SS_ANNOTATION_LABEL, "3D Structures");
    SECONDARY_STRUCTURE_LABELS.put(SS_ANNOTATION_FROM_JPRED_LABEL, "JPred");
    // Add other secondary structure labels here if needed
  }
}
