/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.io.exceptions;

/**
 * wrapper for passing error messages and exceptions back to UI when image io
 * goes wrong
 * 
 * @author jprocter
 *
 */
public class ImageOutputException extends Exception
{

  public ImageOutputException()
  {
  }

  public ImageOutputException(String message)
  {
    super(message);
  }

  public ImageOutputException(Throwable cause)
  {
    super(cause);
  }

  public ImageOutputException(String message, Throwable cause)
  {
    super(message, cause);
  }

  public ImageOutputException(String message, Throwable cause,
          boolean enableSuppression, boolean writableStackTrace)
  {
    super(message, cause, enableSuppression, writableStackTrace);
  }

}
