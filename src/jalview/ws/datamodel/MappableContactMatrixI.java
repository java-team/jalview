/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.ws.datamodel;

import jalview.datamodel.ContactListI;
import jalview.datamodel.ContactMatrixI;
import jalview.datamodel.Mapping;
import jalview.datamodel.SequenceI;
import jalview.util.MapList;

public interface MappableContactMatrixI extends ContactMatrixI
{

  boolean hasReferenceSeq();

  SequenceI getReferenceSeq();

  /**
   * remaps the matrix to a new reference sequence
   * 
   * @param dsq
   * @param sqmpping
   *          - mapping from current reference to new reference - 1:1 only
   * @return new ContactMatrixI instance with updated mapping
   */
  MappableContactMatrixI liftOver(SequenceI dsq, Mapping sqmpping);

  /**
   * like ContactMatrixI.getContactList(int column) but
   * 
   * @param localFrame
   *          - sequence or other object that this contact matrix is associated
   *          with
   * @param column
   *          - position in localFrame
   * @return ContactListI that returns contacts w.r.t. localFrame
   */

  ContactListI getMappableContactList(SequenceI localFrame, int column);

  /**
   * 
   * Similar to AlignedCodonFrame.getMappingBetween
   * 
   * @param sequenceRef
   *          - a reference sequence mappable to this contactMatrix - may be
   *          null
   * @return null or the MapList mapping to the coordinates of the reference
   *         sequence (or if hasReferenceSeq() is false, and sequenceRef is
   *         null, any mapping present)
   * 
   */
  MapList getMapFor(SequenceI sequenceRef);

  /**
   * Locate a position in the mapped sequence for a single column in the matrix.
   * this to resolve positions corresponding to column clusters
   * 
   * @param localFrame
   *          - sequence derivced from reference sequence
   * @param column
   *          - matrix row/column
   * @return sequence position(s) corresponding to column in contact matrix
   */
  int[] getMappedPositionsFor(SequenceI localFrame, int column);

  /**
   * Locate a position in the mapped sequence for a contiguous range of columns
   * in the matrix use this to resolve positions corresponding to column
   * clusters
   * 
   * @param localFrame
   *          - sequence derivced from reference sequence
   * @param column
   *          - matrix row/column
   * @return sequence position(s) corresponding to column in contact matrix
   */
  int[] getMappedPositionsFor(SequenceI localFrame, int from, int to);

  ContactMatrixI getMappedMatrix();
}
