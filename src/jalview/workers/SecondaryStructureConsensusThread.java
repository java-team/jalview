/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.workers;

import jalview.analysis.AAFrequency;
import jalview.api.AlignViewportI;
import jalview.api.AlignmentViewPanel;
import jalview.datamodel.AlignmentAnnotation;
import jalview.datamodel.AlignmentI;
import jalview.datamodel.Annotation;
import jalview.datamodel.ProfilesI;
import jalview.datamodel.SequenceI;
import jalview.renderer.ResidueShaderI;

public class SecondaryStructureConsensusThread extends AlignCalcWorker
{
  public SecondaryStructureConsensusThread(AlignViewportI alignViewport,
          AlignmentViewPanel alignPanel)
  {
    super(alignViewport, alignPanel);
  }

  @Override
  public void run()
  {
    if (calcMan.isPending(this))
    {
      return;
    }
    calcMan.notifyStart(this);
    // long started = System.currentTimeMillis();
    try
    {
      AlignmentAnnotation ssConsensus = getSSConsensusAnnotation();
      if ((ssConsensus == null) || calcMan.isPending(this))
      {
        calcMan.workerComplete(this);
        return;
      }
      while (!calcMan.notifyWorking(this))
      {
        try
        {
          if (ap != null)
          {
            ap.paintAlignment(false, false);
          }
          Thread.sleep(200);
        } catch (Exception ex)
        {
          ex.printStackTrace();
        }
      }
      if (alignViewport.isClosed())
      {
        abortAndDestroy();
        return;
      }
      AlignmentI alignment = alignViewport.getAlignment();

      int aWidth = -1;

      if (alignment == null || (aWidth = alignment.getWidth()) < 0)
      {
        calcMan.workerComplete(this);
        return;
      }

      eraseSSConsensus(aWidth);
      computeSSConsensus(alignment);
      updateResultAnnotation(true);

      if (ap != null)
      {
        ap.paintAlignment(true, true);
      }
    } catch (OutOfMemoryError error)
    {
      calcMan.disableWorker(this);
      ap.raiseOOMWarning("calculating consensus", error);
    } finally
    {
      /*
       * e.g. ArrayIndexOutOfBoundsException can happen due to a race condition
       * - alignment was edited at same time as calculation was running
       */
      calcMan.workerComplete(this);
    }
  }

  /**
   * Clear out any existing consensus annotations
   * 
   * @param aWidth
   *          the width (number of columns) of the annotated alignment
   */
  protected void eraseSSConsensus(int aWidth)
  {
    AlignmentAnnotation ssConsensus = getSSConsensusAnnotation();
    if (ssConsensus != null)
    {
      ssConsensus.annotations = new Annotation[aWidth];
    }
  }

  /**
   * @param alignment
   */
  protected void computeSSConsensus(AlignmentI alignment)
  {

    SequenceI[] aseqs = getSequences();
    int width = alignment.getWidth();
    ProfilesI hSSConsensus = AAFrequency.calculateSS(aseqs, width, 0, width,
            true);

    alignViewport.setSequenceSSConsensusHash(hSSConsensus);
    setColourSchemeConsensus(hSSConsensus);
  }

  /**
   * @return
   */
  protected SequenceI[] getSequences()
  {
    return alignViewport.getAlignment().getSequencesArray();
  }

  /**
   * @param hconsensus
   */
  protected void setColourSchemeConsensus(ProfilesI hSSconsensus)
  {
    ResidueShaderI cs = alignViewport.getResidueShading();
    if (cs != null)
    {
      cs.setSsConsensus(hSSconsensus);
    }
  }

  /**
   * Get the Consensus annotation for the alignment
   * 
   * @return
   */
  protected AlignmentAnnotation getSSConsensusAnnotation()
  {
    return alignViewport
            .getAlignmentSecondaryStructureConsensusAnnotation();
  }

  /**
   * update the consensus annotation from the sequence profile data using
   * current visualization settings.
   */
  @Override
  public void updateAnnotation()
  {
    updateResultAnnotation(false);
  }

  public void updateResultAnnotation(boolean immediate)
  {
    AlignmentAnnotation ssConsensus = getSSConsensusAnnotation();
    ProfilesI hSSConsensus = (ProfilesI) getViewportSSConsensus();
    if (immediate || !calcMan.isWorking(this) && ssConsensus != null
            && hSSConsensus != null)
    {
      deriveSSConsensus(ssConsensus, hSSConsensus);
      
      ssConsensus.hasData=hSSConsensus.getCount()>0;
    }
  }

  /**
   * Convert the computed consensus data into the desired annotation for
   * display.
   * 
   * @param consensusAnnotation
   *          the annotation to be populated
   * @param hconsensus
   *          the computed consensus data
   */
  protected void deriveSSConsensus(
          AlignmentAnnotation ssConsensusAnnotation, ProfilesI hSSConsensus)
  {

    long nseq = getSequences().length;
    AAFrequency.completeSSConsensus(ssConsensusAnnotation, hSSConsensus,
            hSSConsensus.getStartColumn(), hSSConsensus.getEndColumn() + 1,
            alignViewport.isIgnoreGapsConsensus(),
            alignViewport.isShowSequenceLogo(), nseq);
  }

  /**
   * Get the consensus data stored on the viewport.
   * 
   * @return
   */
  protected Object getViewportSSConsensus()
  {
    // TODO convert ComplementConsensusThread to use Profile
    return alignViewport.getSequenceSSConsensusHash();
  }
}
