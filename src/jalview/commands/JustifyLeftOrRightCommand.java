/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.commands;

import java.util.List;

import jalview.datamodel.AlignmentI;
import jalview.datamodel.ContiguousI;
import jalview.datamodel.SequenceI;
import jalview.util.Comparison;

public class JustifyLeftOrRightCommand extends EditCommand
{
  /**
   * Constructs and performs a trim alignment command
   * 
   * @param description
   *          (to show in Undo/Redo menu)
   * @param justifyLeft
   *          if true left justify, otherwise right
   * @param seqs
   *          the sequences to justify
   * @param start
   *          - leftmost column (base 0) to justify
   * 
   * @param end
   *          - rightmost column (base 0) to justify
   * 
   * @param al
   */
  public JustifyLeftOrRightCommand(String description, boolean left,
          List<SequenceI> seqs, int from, int to, AlignmentI al)
  {
    this.description = description;

    for (SequenceI seq : seqs)
    {
      ContiguousI cont = seq.findPositions(from + 1, to + 1);
      if (cont == null)
      {
        continue;
      }
      char[] range = seq.getSequence(from, to + 1);
      if (range == null || range.length == 0)
      {
        continue;
      }
      int dsstart = seq.getDatasetSequence().getStart();
      char[] sqchar = seq.getDatasetSequence().getSequence(
              -dsstart + cont.getBegin(), -dsstart + cont.getEnd() + 1);
      // debug
      // println sqchar;
      char[] alseq = new char[to - from + 1];
      int sqstart = left ? 0 : alseq.length - sqchar.length;
      int gaps = alseq.length - sqchar.length;
      int gapstart = left ? sqchar.length : 0;
      char gc = al.getGapCharacter();
      for (int gp = 0; gp < gaps; gp++)
      {
        alseq[gapstart + gp] = gc;
      }

      for (int sqp = 0, insp = 0; sqp < alseq.length; sqp++)
      {
        if (sqp < range.length && !Comparison.isGap(range[sqp]))
        {
          alseq[insp++ + sqstart] = range[sqp];
        }
      }
      SequenceI[] sqa = new SequenceI[1];
      sqa[0] = seq;

      addEdit(new jalview.commands.EditCommand.Edit(
              jalview.commands.EditCommand.Action.REPLACE, sqa, from,
              to + 1, al, new String(alseq)));
    }

    performEdit(0, null);
  }
}
