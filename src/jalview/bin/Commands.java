/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.bin;

import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.swing.SwingUtilities;

import jalview.analysis.AlignmentUtils;
import jalview.api.structures.JalviewStructureDisplayI;
import jalview.bin.Jalview.ExitCode;
import jalview.bin.argparser.Arg;
import jalview.bin.argparser.ArgParser;
import jalview.bin.argparser.ArgValue;
import jalview.bin.argparser.ArgValuesMap;
import jalview.bin.argparser.SubVals;
import jalview.datamodel.AlignmentI;
import jalview.datamodel.SequenceI;
import jalview.datamodel.annotations.AlphaFoldAnnotationRowBuilder;
import jalview.gui.AlignFrame;
import jalview.gui.AlignmentPanel;
import jalview.gui.AppJmol;
import jalview.gui.Desktop;
import jalview.gui.Preferences;
import jalview.gui.StructureChooser;
import jalview.gui.StructureViewer;
import jalview.gui.StructureViewer.ViewerType;
import jalview.io.AppletFormatAdapter;
import jalview.io.BackupFiles;
import jalview.io.BioJsHTMLOutput;
import jalview.io.DataSourceType;
import jalview.io.FileFormat;
import jalview.io.FileFormatException;
import jalview.io.FileFormatI;
import jalview.io.FileFormats;
import jalview.io.FileLoader;
import jalview.io.HtmlSvgOutput;
import jalview.io.IdentifyFile;
import jalview.io.NewickFile;
import jalview.io.exceptions.ImageOutputException;
import jalview.schemes.ColourSchemeI;
import jalview.schemes.ColourSchemeProperty;
import jalview.structure.StructureCommandI;
import jalview.structure.StructureImportSettings.TFType;
import jalview.structure.StructureSelectionManager;
import jalview.util.ColorUtils;
import jalview.util.FileUtils;
import jalview.util.HttpUtils;
import jalview.util.ImageMaker;
import jalview.util.ImageMaker.TYPE;
import jalview.util.MessageManager;
import jalview.util.Platform;
import jalview.util.StringUtils;
import jalview.util.imagemaker.BitmapImageSizing;

public class Commands
{
  Desktop desktop;

  private boolean headless;

  private ArgParser argParser;

  private Map<String, AlignFrame> afMap;

  private Map<String, List<StructureViewer>> svMap;

  private boolean commandArgsProvided = false;

  private boolean argsWereParsed = false;

  private List<String> errors = new ArrayList<>();

  public Commands(ArgParser argparser, boolean headless)
  {
    this(Desktop.instance, argparser, headless);
  }

  public Commands(Desktop d, ArgParser argparser, boolean h)
  {
    argParser = argparser;
    headless = h;
    desktop = d;
    afMap = new HashMap<>();
  }

  protected boolean processArgs()
  {
    if (argParser == null)
    {
      return true;
    }

    boolean theseArgsWereParsed = false;

    if (argParser != null && argParser.getLinkedIds() != null)
    {
      for (String id : argParser.getLinkedIds())
      {
        ArgValuesMap avm = argParser.getLinkedArgs(id);
        theseArgsWereParsed = true;
        boolean processLinkedOkay = processLinked(id);
        theseArgsWereParsed &= processLinkedOkay;

        processGroovyScript(id);

        // wait around until alignFrame isn't busy
        AlignFrame af = afMap.get(id);
        while (af != null && af.getViewport().isCalcInProgress())
        {
          try
          {
            Thread.sleep(25);
          } catch (Exception q)
          {
          }
          ;
        }

        theseArgsWereParsed &= processImages(id);

        if (processLinkedOkay)
        {
          theseArgsWereParsed &= processOutput(id);
        }

        // close ap
        if (avm.getBoolean(Arg.CLOSE))
        {
          af = afMap.get(id);
          if (af != null)
          {
            af.closeMenuItem_actionPerformed(true);
          }
        }

      }

    }

    // report errors - if any
    String errorsRaised = errorsToString();
    if (errorsRaised.trim().length() > 0)
    {
      Console.warn(
              "The following errors and warnings occurred whilst processing files:\n"
                      + errorsRaised);
    }
    // gui errors reported in Jalview

    if (argParser.getBoolean(Arg.QUIT))
    {
      Jalview.exit("Exiting due to " + Arg.QUIT.argString() + " argument.",
              ExitCode.OK);
      return true;
    }
    // carry on with jalview.bin.Jalview
    argsWereParsed = theseArgsWereParsed;
    return argsWereParsed;
  }

  public boolean commandArgsProvided()
  {
    return commandArgsProvided;
  }

  public boolean argsWereParsed()
  {
    return argsWereParsed;
  }

  protected boolean processLinked(String id)
  {
    boolean theseArgsWereParsed = false;
    ArgValuesMap avm = argParser.getLinkedArgs(id);
    if (avm == null)
    {
      return true;
    }

    Boolean isError = Boolean.valueOf(false);

    // set wrap, showSSAnnotations, showAnnotations and hideTFrows scope here so
    // it can be applied after structures are opened
    boolean wrap = false;
    boolean showSSAnnotations = false;
    boolean showAnnotations = false;
    boolean hideTFrows = false;
    AlignFrame af = null;

    if (avm.containsArg(Arg.APPEND) || avm.containsArg(Arg.OPEN))
    {
      commandArgsProvided = true;
      final long progress = System.currentTimeMillis();

      boolean first = true;
      boolean progressBarSet = false;
      // Combine the APPEND and OPEN files into one list, along with whether it
      // was APPEND or OPEN
      List<ArgValue> openAvList = new ArrayList<>();
      openAvList.addAll(avm.getArgValueList(Arg.OPEN));
      openAvList.addAll(avm.getArgValueList(Arg.APPEND));
      // sort avlist based on av.getArgIndex()
      Collections.sort(openAvList);
      for (ArgValue av : openAvList)
      {
        Arg a = av.getArg();
        SubVals sv = av.getSubVals();
        String openFile0 = av.getValue();
        String openFile = HttpUtils.equivalentJalviewUrl(openFile0);
        if (openFile == null)
          continue;

        theseArgsWereParsed = true;
        if (first)
        {
          first = false;
          if (!headless && desktop != null)
          {
            SwingUtilities.invokeLater(new Runnable()
            {
              @Override
              public void run()
              {
                desktop.setProgressBar(
                        MessageManager.getString(
                                "status.processing_commandline_args"),
                        progress);

              }
            });
            progressBarSet = true;
          }
        }

        if (!Platform.isJS())
        /**
         * ignore in JavaScript -- can't just file existence - could load it?
         * 
         * @j2sIgnore
         */
        {
          if (!HttpUtils.isPlausibleUri(openFile))
          {
            if (!(new File(openFile)).exists())
            {
              addError("Can't find file '" + openFile + "'");
              isError = true;
              continue;
            }
          }
        }

        DataSourceType protocol = AppletFormatAdapter
                .checkProtocol(openFile);

        FileFormatI format = null;
        try
        {
          format = new IdentifyFile().identify(openFile, protocol);
        } catch (FileNotFoundException e0)
        {
          addError((protocol == DataSourceType.URL ? "File at URL" : "File")
                  + " '" + openFile + "' not found");
          isError = true;
          continue;
        } catch (FileFormatException e1)
        {
          addError("Unknown file format for '" + openFile + "'");
          isError = true;
          continue;
        }

        af = afMap.get(id);
        // When to open a new AlignFrame
        if (af == null || "true".equals(av.getSubVal("new"))
                || a == Arg.OPEN || format == FileFormat.Jalview)
        {
          if (a == Arg.OPEN)
          {
            Jalview.testoutput(argParser, Arg.OPEN, "examples/uniref50.fa",
                    openFile);
          }

          Console.debug(
                  "Opening '" + openFile + "' in new alignment frame");
          FileLoader fileLoader = new FileLoader(!headless);
          boolean xception = false;
          try
          {
            af = fileLoader.LoadFileWaitTillLoaded(openFile, protocol,
                    format);
            if (!openFile.equals(openFile0))
            {
              af.setTitle(openFile0);
            }
          } catch (Throwable thr)
          {
            xception = true;
            addError("Couldn't open '" + openFile + "' as " + format + " "
                    + thr.getLocalizedMessage()
                    + " (Enable debug for full stack trace)");
            isError = true;
            Console.debug("Exception when opening '" + openFile + "'", thr);
          } finally
          {
            if (af == null && !xception)
            {
              addInfo("Ignoring '" + openFile
                      + "' - no alignment data found.");
              continue;
            }
          }

          // colour alignment
          String colour = null;
          if (avm.containsArg(Arg.COLOUR)
                  || !(format == FileFormat.Jalview))
          {
            colour = avm.getFromSubValArgOrPref(av, Arg.COLOUR, sv, null,
                    "DEFAULT_COLOUR_PROT", null);
          }
          if (colour != null)
          {
            this.colourAlignFrame(af, colour);
          }

          // Change alignment frame title
          String title = avm.getFromSubValArgOrPref(av, Arg.TITLE, sv, null,
                  null, null);
          if (title != null)
          {
            af.setTitle(title);
            Jalview.testoutput(argParser, Arg.TITLE, "test title", title);
          }

          // Add features
          String featuresfile = avm.getValueFromSubValOrArg(av,
                  Arg.FEATURES, sv);
          if (featuresfile != null)
          {
            af.parseFeaturesFile(featuresfile,
                    AppletFormatAdapter.checkProtocol(featuresfile));
            Jalview.testoutput(argParser, Arg.FEATURES,
                    "examples/testdata/plantfdx.features", featuresfile);
          }

          // Add annotations from file
          String annotationsfile = avm.getValueFromSubValOrArg(av,
                  Arg.ANNOTATIONS, sv);
          if (annotationsfile != null)
          {
            af.loadJalviewDataFile(annotationsfile, null, null, null);
            Jalview.testoutput(argParser, Arg.ANNOTATIONS,
                    "examples/testdata/plantfdx.annotations",
                    annotationsfile);
          }

          // Set or clear the sortbytree flag
          boolean sortbytree = avm.getBoolFromSubValOrArg(Arg.SORTBYTREE,
                  sv);
          if (sortbytree)
          {
            af.getViewport().setSortByTree(true);
            Jalview.testoutput(argParser, Arg.SORTBYTREE);
          }

          // Load tree from file
          String treefile = avm.getValueFromSubValOrArg(av, Arg.TREE, sv);
          if (treefile != null)
          {
            try
            {
              NewickFile nf = new NewickFile(treefile,
                      AppletFormatAdapter.checkProtocol(treefile));
              af.getViewport().setCurrentTree(
                      af.showNewickTree(nf, treefile).getTree());
              Jalview.testoutput(argParser, Arg.TREE,
                      "examples/testdata/uniref50_test_tree", treefile);
            } catch (IOException e)
            {
              addError("Couldn't add tree " + treefile, e);
              isError = true;
            }
          }

          // Show secondary structure annotations?
          showSSAnnotations = avm.getFromSubValArgOrPref(
                  Arg.SHOWSSANNOTATIONS, av.getSubVals(), null,
                  "STRUCT_FROM_PDB", true);
          // Show sequence annotations?
          showAnnotations = avm.getFromSubValArgOrPref(Arg.SHOWANNOTATIONS,
                  av.getSubVals(), null, "SHOW_ANNOTATIONS", true);
          // hide the Temperature Factor row?
          hideTFrows = (avm.getBoolean(Arg.NOTEMPFAC));

          // showSSAnnotations, showAnnotations, hideTFrows used after opening
          // structure

          // wrap alignment? do this last for formatting reasons
          wrap = avm.getFromSubValArgOrPref(Arg.WRAP, sv, null,
                  "WRAP_ALIGNMENT", false);
          // af.setWrapFormat(wrap) is applied after structures are opened for
          // annotation reasons

          // store the AlignFrame for this id
          afMap.put(id, af);

          // is it its own structure file?
          if (format.isStructureFile())
          {
            StructureSelectionManager ssm = StructureSelectionManager
                    .getStructureSelectionManager(Desktop.instance);
            SequenceI seq = af.alignPanel.getAlignment().getSequenceAt(0);
            ssm.computeMapping(false, new SequenceI[] { seq }, null,
                    openFile, DataSourceType.FILE, null, null, null, false);
          }
        }
        else
        {
          Console.debug(
                  "Opening '" + openFile + "' in existing alignment frame");

          DataSourceType dst = HttpUtils.startsWithHttpOrHttps(openFile)
                  ? DataSourceType.URL
                  : DataSourceType.FILE;

          FileLoader fileLoader = new FileLoader(!headless);
          fileLoader.LoadFile(af.getCurrentView(), openFile, dst, null,
                  false);
        }

        Console.debug("Command " + Arg.APPEND + " executed successfully!");

      }
      if (first) // first=true means nothing opened
      {
        if (headless)
        {
          Jalview.exit("Could not open any files in headless mode",
                  ExitCode.NO_FILES);
        }
        else
        {
          Console.info("No more files to open");
        }
      }
      if (progressBarSet && desktop != null)
        desktop.setProgressBar(null, progress);

    }

    // open the structure (from same PDB file or given PDBfile)
    if (!avm.getBoolean(Arg.NOSTRUCTURE))
    {
      if (af == null)
      {
        af = afMap.get(id);
      }
      if (avm.containsArg(Arg.STRUCTURE))
      {
        commandArgsProvided = true;
        for (ArgValue structureAv : avm.getArgValueList(Arg.STRUCTURE))
        {
          argParser.setStructureFilename(null);
          String val = structureAv.getValue();
          SubVals subVals = structureAv.getSubVals();
          int argIndex = structureAv.getArgIndex();
          SequenceI seq = getSpecifiedSequence(af, avm, structureAv);
          if (seq == null)
          {
            // Could not find sequence from subId, let's assume the first
            // sequence in the alignframe
            AlignmentI al = af.getCurrentView().getAlignment();
            seq = al.getSequenceAt(0);
          }

          if (seq == null)
          {
            addWarn("Could not find sequence for argument "
                    + Arg.STRUCTURE.argString() + "=" + val);
            continue;
          }
          String structureFilename = null;
          File structureFile = null;
          if (subVals.getContent() != null
                  && subVals.getContent().length() != 0)
          {
            structureFilename = subVals.getContent();
            Console.debug("Using structure file (from argument) '"
                    + structureFilename + "'");
            structureFile = new File(structureFilename);
          }
          /* THIS DOESN'T WORK */
          else if (seq.getAllPDBEntries() != null
                  && seq.getAllPDBEntries().size() > 0)
          {
            structureFile = new File(
                    seq.getAllPDBEntries().elementAt(0).getFile());
            if (structureFile != null)
            {
              Console.debug("Using structure file (from sequence) '"
                      + structureFile.getAbsolutePath() + "'");
            }
            structureFilename = structureFile.getAbsolutePath();
          }

          if (structureFilename == null || structureFile == null)
          {
            addWarn("Not provided structure file with '" + val + "'");
            continue;
          }

          if (!structureFile.exists())
          {
            addWarn("Structure file '" + structureFile.getAbsoluteFile()
                    + "' not found.");
            continue;
          }

          Console.debug("Using structure file "
                  + structureFile.getAbsolutePath());

          argParser.setStructureFilename(structureFilename);

          // open structure view
          AlignmentPanel ap = af.alignPanel;
          if (headless)
          {
            Cache.setProperty(Preferences.STRUCTURE_DISPLAY,
                    StructureViewer.ViewerType.JMOL.toString());
          }

          String structureFilepath = structureFile.getAbsolutePath();

          // get PAEMATRIX file and label from subvals or Arg.PAEMATRIX
          String paeFilepath = avm.getFromSubValArgOrPrefWithSubstitutions(
                  argParser, Arg.PAEMATRIX, ArgValuesMap.Position.AFTER,
                  structureAv, subVals, null, null, null);
          if (paeFilepath != null)
          {
            File paeFile = new File(paeFilepath);

            try
            {
              paeFilepath = paeFile.getCanonicalPath();
            } catch (IOException e)
            {
              paeFilepath = paeFile.getAbsolutePath();
              addWarn("Problem with the PAE file path: '"
                      + paeFile.getPath() + "'");
            }
          }

          // showing annotations from structure file or not
          boolean ssFromStructure = avm.getFromSubValArgOrPref(
                  Arg.SHOWSSANNOTATIONS, subVals, null, "STRUCT_FROM_PDB",
                  true);

          // get TEMPFAC type from subvals or Arg.TEMPFAC in case user Adds
          // reference annotations
          String tftString = avm.getFromSubValArgOrPrefWithSubstitutions(
                  argParser, Arg.TEMPFAC, ArgValuesMap.Position.AFTER,
                  structureAv, subVals, null, null, null);
          boolean notempfac = avm.getFromSubValArgOrPref(Arg.NOTEMPFAC,
                  subVals, null, "ADD_TEMPFACT_ANN", false, true);
          TFType tft = notempfac ? null : TFType.DEFAULT;
          if (tftString != null && !notempfac)
          {
            // get kind of temperature factor annotation
            try
            {
              tft = TFType.valueOf(tftString.toUpperCase(Locale.ROOT));
              Console.debug("Obtained Temperature Factor type of '" + tft
                      + "' for structure '" + structureFilepath + "'");
            } catch (IllegalArgumentException e)
            {
              // Just an error message!
              StringBuilder sb = new StringBuilder().append("Cannot set ")
                      .append(Arg.TEMPFAC.argString()).append(" to '")
                      .append(tft)
                      .append("', ignoring.  Valid values are: ");
              Iterator<TFType> it = Arrays.stream(TFType.values())
                      .iterator();
              while (it.hasNext())
              {
                sb.append(it.next().toString().toLowerCase(Locale.ROOT));
                if (it.hasNext())
                  sb.append(", ");
              }
              addWarn(sb.toString());
            }
          }

          String sViewerName = avm.getFromSubValArgOrPref(
                  Arg.STRUCTUREVIEWER, ArgValuesMap.Position.AFTER,
                  structureAv, subVals, null, null, "jmol");
          ViewerType viewerType = ViewerType.getFromString(sViewerName);

          // TODO use ssFromStructure
          StructureViewer structureViewer = StructureChooser
                  .openStructureFileForSequence(null, null, ap, seq, false,
                          structureFilepath, tft, paeFilepath, false,
                          ssFromStructure, false, viewerType);

          if (structureViewer == null)
          {
            if (!StringUtils.equalsIgnoreCase(sViewerName, "none"))
            {
              addError("Failed to import and open structure view for file '"
                      + structureFile + "'.");
            }
            continue;
          }
          try
          {
            long tries = 1000;
            while (structureViewer.isBusy() && tries > 0)
            {
              Thread.sleep(25);
              if (structureViewer.isBusy())
              {
                tries--;
                Console.debug(
                        "Waiting for viewer for " + structureFilepath);
              }
            }
            if (tries == 0 && structureViewer.isBusy())
            {
              addWarn("Gave up waiting for structure viewer to load file '"
                      + structureFile
                      + "'. Something may have gone wrong.");
            }
          } catch (Exception x)
          {
            addError("Exception whilst waiting for structure viewer "
                    + structureFilepath, x);
            isError = true;
          }

          // add StructureViewer to svMap list
          if (svMap == null)
          {
            svMap = new HashMap<>();
          }
          if (svMap.get(id) == null)
          {
            svMap.put(id, new ArrayList<>());
          }
          svMap.get(id).add(structureViewer);

          Console.debug(
                  "Successfully opened viewer for " + structureFilepath);

          if (avm.containsArg(Arg.STRUCTUREIMAGE))
          {
            for (ArgValue structureImageArgValue : avm
                    .getArgValueListFromSubValOrArg(structureAv,
                            Arg.STRUCTUREIMAGE, subVals))
            {
              String structureImageFilename = argParser.makeSubstitutions(
                      structureImageArgValue.getValue(), id, true);
              if (structureViewer != null && structureImageFilename != null)
              {
                SubVals structureImageSubVals = null;
                structureImageSubVals = structureImageArgValue.getSubVals();
                File structureImageFile = new File(structureImageFilename);
                String width = avm.getValueFromSubValOrArg(
                        structureImageArgValue, Arg.WIDTH,
                        structureImageSubVals);
                String height = avm.getValueFromSubValOrArg(
                        structureImageArgValue, Arg.HEIGHT,
                        structureImageSubVals);
                String scale = avm.getValueFromSubValOrArg(
                        structureImageArgValue, Arg.SCALE,
                        structureImageSubVals);
                String renderer = avm.getValueFromSubValOrArg(
                        structureImageArgValue, Arg.TEXTRENDERER,
                        structureImageSubVals);
                String typeS = avm.getValueFromSubValOrArg(
                        structureImageArgValue, Arg.TYPE,
                        structureImageSubVals);
                if (typeS == null || typeS.length() == 0)
                {
                  typeS = FileUtils.getExtension(structureImageFile);
                }
                TYPE imageType;
                try
                {
                  imageType = Enum.valueOf(TYPE.class,
                          typeS.toUpperCase(Locale.ROOT));
                } catch (IllegalArgumentException e)
                {
                  addWarn("Do not know image format '" + typeS
                          + "', using PNG");
                  imageType = TYPE.PNG;
                }
                BitmapImageSizing userBis = ImageMaker
                        .parseScaleWidthHeightStrings(scale, width, height);

                /////
                // DON'T TRY TO EXPORT IF VIEWER IS UNSUPPORTED
                if (viewerType != ViewerType.JMOL)
                {
                  addWarn("Cannot export image for structure viewer "
                          + viewerType.name() + " yet");
                  continue;
                }

                /////
                // Apply the temporary colourscheme to the linked alignment
                // TODO: enhance for multiple linked alignments.

                String imageColour = avm.getValueFromSubValOrArg(
                        structureImageArgValue, Arg.IMAGECOLOUR,
                        structureImageSubVals);
                ColourSchemeI originalColourScheme = this
                        .getColourScheme(af);
                this.colourAlignFrame(af, imageColour);

                /////
                // custom image background colour

                String bgcolourstring = avm.getValueFromSubValOrArg(
                        structureImageArgValue, Arg.BGCOLOUR,
                        structureImageSubVals);
                Color bgcolour = null;
                if (bgcolourstring != null && bgcolourstring.length() > 0)
                {
                  bgcolour = ColorUtils.parseColourString(bgcolourstring);
                  if (bgcolour == null)
                  {
                    Console.warn(
                            "Background colour string '" + bgcolourstring
                                    + "' not recognised -- using default");
                  }
                }

                JalviewStructureDisplayI sview = structureViewer
                        .getJalviewStructureDisplay();

                File sessionToRestore = null;

                List<StructureCommandI> extraCommands = new ArrayList<>();

                if (extraCommands.size() > 0 || bgcolour != null)
                {
                  try
                  {
                    sessionToRestore = sview.saveSession();
                  } catch (Throwable t)
                  {
                    Console.warn(
                            "Unable to save temporary session file before custom structure view export operation.");
                  }
                }

                ////
                // Do temporary ops

                if (bgcolour != null)
                {
                  sview.getBinding().setBackgroundColour(bgcolour);
                }

                sview.getBinding().executeCommands(extraCommands, false,
                        "Executing Custom Commands");

                // and export the view as an image
                boolean success = this.checksBeforeWritingToFile(avm,
                        subVals, false, structureImageFilename,
                        "structure image", isError);

                if (!success)
                {
                  continue;
                }
                Console.debug("Rendering image to " + structureImageFile);
                //
                // TODO - extend StructureViewer / Binding with makePDBImage so
                // we can do this with every viewer
                //

                try
                {
                  // We don't expect class cast exception
                  AppJmol jmol = (AppJmol) sview;
                  jmol.makePDBImage(structureImageFile, imageType, renderer,
                          userBis);
                  Console.info("Exported structure image to "
                          + structureImageFile);

                  // RESTORE SESSION AFTER EXPORT IF NEED BE
                  if (sessionToRestore != null)
                  {
                    Console.debug(
                            "Restoring session from " + sessionToRestore);

                    sview.getBinding().restoreSession(
                            sessionToRestore.getAbsolutePath());

                  }
                } catch (ImageOutputException ioexec)
                {
                  addError(
                          "Unexpected error when restoring structure viewer session after custom view operations.");
                  isError = true;
                  continue;
                } finally
                {
                  try
                  {
                    this.colourAlignFrame(af, originalColourScheme);
                  } catch (Exception t)
                  {
                    addError(
                            "Unexpected error when restoring colourscheme to alignment after temporary change for export.",
                            t);
                  }
                }
              }
            }
          }
          argParser.setStructureFilename(null);
        }
      }
    }

    if (af == null)
    {
      af = afMap.get(id);
    }
    // many of jalview's format/layout methods are only thread safe on the
    // swingworker thread.
    // all these methods should be on the alignViewController so it can
    // coordinate such details
    if (headless)
    {
      showOrHideAnnotations(af, showSSAnnotations, showAnnotations,
              hideTFrows);
    }
    else
    {
      try
      {
        AlignFrame _af = af;
        final boolean _showSSAnnotations = showSSAnnotations;
        final boolean _showAnnotations = showAnnotations;
        final boolean _hideTFrows = hideTFrows;
        SwingUtilities.invokeAndWait(() -> {
          showOrHideAnnotations(_af, _showSSAnnotations, _showAnnotations,
                  _hideTFrows);
        }

        );
      } catch (Exception x)
      {
        Console.warn(
                "Unexpected exception adjusting annotation row visibility.",
                x);
      }
    }

    if (wrap)
    {
      if (af == null)
      {
        af = afMap.get(id);
      }
      if (af != null)
      {
        af.setWrapFormat(wrap, true);
      }
    }

    /*
    boolean doShading = avm.getBoolean(Arg.TEMPFAC_SHADING);
    if (doShading)
    {
    AlignFrame af = afMap.get(id);
    for (AlignmentAnnotation aa : af.alignPanel.getAlignment()
            .findAnnotation(PDBChain.class.getName().toString()))
    {
      AnnotationColourGradient acg = new AnnotationColourGradient(aa,
              af.alignPanel.av.getGlobalColourScheme(), 0);
      acg.setSeqAssociated(true);
      af.changeColour(acg);
      Console.info("Changed colour " + acg.toString());
    }
    }
    */

    return theseArgsWereParsed && !isError;
  }

  private static void showOrHideAnnotations(AlignFrame af,
          boolean showSSAnnotations, boolean showAnnotations,
          boolean hideTFrows)
  {
    af.setAnnotationsVisibility(showSSAnnotations, true, false);
    af.setAnnotationsVisibility(showAnnotations, false, true);

    // show temperature factor annotations?
    if (hideTFrows)
    {
      // do this better (annotation types?)
      List<String> hideThese = new ArrayList<>();
      hideThese.add("Temperature Factor");
      hideThese.add(AlphaFoldAnnotationRowBuilder.LABEL);
      AlignmentUtils.showOrHideSequenceAnnotations(
              af.getCurrentView().getAlignment(), hideThese, null, false,
              false);
    }
  }

  protected void processGroovyScript(String id)
  {
    ArgValuesMap avm = argParser.getLinkedArgs(id);
    AlignFrame af = afMap.get(id);

    if (avm != null && !avm.containsArg(Arg.GROOVY))
    {
      // nothing to do
      return;
    }

    if (af == null)
    {
      addWarn("Groovy script does not have an alignment window.  Proceeding with caution!");
    }

    if (avm.containsArg(Arg.GROOVY))
    {
      for (ArgValue groovyAv : avm.getArgValueList(Arg.GROOVY))
      {
        String groovyscript = groovyAv.getValue();
        if (groovyscript != null)
        {
          // Execute the groovy script after we've done all the rendering stuff
          // and before any images or figures are generated.
          Console.info("Executing script " + groovyscript);
          Jalview.getInstance().executeGroovyScript(groovyscript, af);
        }
      }
    }
  }

  protected boolean processImages(String id)
  {
    ArgValuesMap avm = argParser.getLinkedArgs(id);
    AlignFrame af = afMap.get(id);

    if (avm != null && !avm.containsArg(Arg.IMAGE))
    {
      // nothing to do
      return true;
    }

    if (af == null)
    {
      addWarn("Do not have an alignment window to create image from (id="
              + id + ").  Not proceeding.");
      return false;
    }

    Boolean isError = Boolean.valueOf(false);
    if (avm.containsArg(Arg.IMAGE))
    {
      for (ArgValue imageAv : avm.getArgValueList(Arg.IMAGE))
      {
        String val = imageAv.getValue();
        SubVals imageSubVals = imageAv.getSubVals();
        String fileName = imageSubVals.getContent();
        File file = new File(fileName);
        String name = af.getName();
        String renderer = avm.getValueFromSubValOrArg(imageAv,
                Arg.TEXTRENDERER, imageSubVals);
        if (renderer == null)
          renderer = "text";
        String type = "png"; // default

        String scale = avm.getValueFromSubValOrArg(imageAv, Arg.SCALE,
                imageSubVals);
        String width = avm.getValueFromSubValOrArg(imageAv, Arg.WIDTH,
                imageSubVals);
        String height = avm.getValueFromSubValOrArg(imageAv, Arg.HEIGHT,
                imageSubVals);
        BitmapImageSizing userBis = ImageMaker
                .parseScaleWidthHeightStrings(scale, width, height);

        type = avm.getValueFromSubValOrArg(imageAv, Arg.TYPE, imageSubVals);
        if (type == null && fileName != null)
        {
          for (String ext : new String[] { "svg", "png", "html", "eps" })
          {
            if (fileName.toLowerCase(Locale.ROOT).endsWith("." + ext))
            {
              type = ext;
            }
          }
        }
        // for moment we disable JSON export
        Cache.setPropsAreReadOnly(true);
        Cache.setProperty("EXPORT_EMBBED_BIOJSON", "false");

        String imageColour = avm.getValueFromSubValOrArg(imageAv,
                Arg.IMAGECOLOUR, imageSubVals);
        ColourSchemeI originalColourScheme = this.getColourScheme(af);
        this.colourAlignFrame(af, imageColour);

        Console.info("Writing " + file);

        boolean success = checksBeforeWritingToFile(avm, imageSubVals,
                false, fileName, "image", isError);
        if (!success)
        {
          continue;
        }

        try
        {
          switch (type)
          {

          case "svg":
            Console.debug("Outputting type '" + type + "' to " + fileName);
            af.createSVG(file, renderer);
            break;

          case "png":
            Console.debug("Outputting type '" + type + "' to " + fileName);
            af.createPNG(file, null, userBis);
            break;

          case "html":
            Console.debug("Outputting type '" + type + "' to " + fileName);
            HtmlSvgOutput htmlSVG = new HtmlSvgOutput(af.alignPanel);
            htmlSVG.exportHTML(fileName, renderer);
            break;

          case "biojs":
            Console.debug(
                    "Outputting BioJS MSA Viwer HTML file: " + fileName);
            try
            {
              BioJsHTMLOutput.refreshVersionInfo(
                      BioJsHTMLOutput.BJS_TEMPLATES_LOCAL_DIRECTORY);
            } catch (URISyntaxException e)
            {
              e.printStackTrace();
            }
            BioJsHTMLOutput bjs = new BioJsHTMLOutput(af.alignPanel);
            bjs.exportHTML(fileName);
            break;

          case "eps":
            Console.debug("Outputting EPS file: " + fileName);
            af.createEPS(file, renderer);
            break;

          case "imagemap":
            Console.debug("Outputting ImageMap file: " + fileName);
            af.createImageMap(file, name);
            break;

          default:
            addWarn(Arg.IMAGE.argString() + " type '" + type
                    + "' not known. Ignoring");
            break;
          }
        } catch (Exception ioex)
        {
          addError("Unexpected error during export to '" + fileName + "'",
                  ioex);
          isError = true;
        }

        this.colourAlignFrame(af, originalColourScheme);
      }
    }
    return !isError;
  }

  protected boolean processOutput(String id)
  {
    ArgValuesMap avm = argParser.getLinkedArgs(id);
    AlignFrame af = afMap.get(id);

    if (avm != null && !avm.containsArg(Arg.OUTPUT))
    {
      // nothing to do
      return true;
    }

    if (af == null)
    {
      addWarn("Do not have an alignment window (id=" + id
              + ").  Not proceeding.");
      return false;
    }

    Boolean isError = Boolean.valueOf(false);

    if (avm.containsArg(Arg.OUTPUT))
    {
      for (ArgValue av : avm.getArgValueList(Arg.OUTPUT))
      {
        String val = av.getValue();
        SubVals subVals = av.getSubVals();
        String fileName = subVals.getContent();
        boolean stdout = ArgParser.STDOUTFILENAME.equals(fileName);
        File file = new File(fileName);

        String name = af.getName();
        String format = avm.getValueFromSubValOrArg(av, Arg.FORMAT,
                subVals);
        FileFormats ffs = FileFormats.getInstance();
        List<String> validFormats = ffs.getWritableFormats(false);

        FileFormatI ff = null;
        if (format == null && fileName != null)
        {
          FORMAT: for (String fname : validFormats)
          {
            FileFormatI tff = ffs.forName(fname);
            String[] extensions = tff.getExtensions().split(",");
            for (String ext : extensions)
            {
              if (fileName.toLowerCase(Locale.ROOT).endsWith("." + ext))
              {
                ff = tff;
                format = ff.getName();
                break FORMAT;
              }
            }
          }
        }
        if (ff == null && format != null)
        {
          ff = ffs.forName(format);
        }
        if (ff == null)
        {
          if (stdout)
          {
            ff = FileFormat.Fasta;
          }
          else
          {
            StringBuilder validSB = new StringBuilder();
            for (String f : validFormats)
            {
              if (validSB.length() > 0)
                validSB.append(", ");
              validSB.append(f);
              FileFormatI tff = ffs.forName(f);
              validSB.append(" (");
              validSB.append(tff.getExtensions());
              validSB.append(")");
            }

            addError("No valid format specified for "
                    + Arg.OUTPUT.argString() + ". Valid formats are "
                    + validSB.toString() + ".");
            continue;
          }
        }

        boolean success = checksBeforeWritingToFile(avm, subVals, true,
                fileName, ff.getName(), isError);
        if (!success)
        {
          continue;
        }

        boolean backups = avm.getFromSubValArgOrPref(Arg.BACKUPS, subVals,
                null, Platform.isHeadless() ? null : BackupFiles.ENABLED,
                !Platform.isHeadless());

        Console.info("Writing " + fileName);

        af.saveAlignment(fileName, ff, stdout, backups);
        if (af.isSaveAlignmentSuccessful())
        {
          Console.debug("Written alignment '" + name + "' in "
                  + ff.getName() + " format to '" + file + "'");
        }
        else
        {
          addError("Error writing file '" + file + "' in " + ff.getName()
                  + " format!");
          isError = true;
          continue;
        }

      }
    }
    return !isError;
  }

  private SequenceI getSpecifiedSequence(AlignFrame af, ArgValuesMap avm,
          ArgValue av)
  {
    SubVals subVals = av.getSubVals();
    ArgValue idAv = avm.getClosestNextArgValueOfArg(av, Arg.SEQID, true);
    SequenceI seq = null;
    if (subVals == null && idAv == null)
      return null;
    if (af == null || af.getCurrentView() == null)
    {
      return null;
    }
    AlignmentI al = af.getCurrentView().getAlignment();
    if (al == null)
    {
      return null;
    }
    if (subVals != null)
    {
      if (subVals.has(Arg.SEQID.getName()))
      {
        seq = al.findName(subVals.get(Arg.SEQID.getName()));
      }
      else if (-1 < subVals.getIndex()
              && subVals.getIndex() < al.getSequences().size())
      {
        seq = al.getSequenceAt(subVals.getIndex());
      }
    }
    if (seq == null && idAv != null)
    {
      seq = al.findName(idAv.getValue());
    }
    return seq;
  }

  public AlignFrame[] getAlignFrames()
  {
    AlignFrame[] afs = null;
    if (afMap != null)
    {
      afs = (AlignFrame[]) afMap.values().toArray();
    }

    return afs;
  }

  public List<StructureViewer> getStructureViewers()
  {
    List<StructureViewer> svs = null;
    if (svMap != null)
    {
      for (List<StructureViewer> svList : svMap.values())
      {
        if (svs == null)
        {
          svs = new ArrayList<>();
        }
        svs.addAll(svList);
      }
    }
    return svs;
  }

  private void colourAlignFrame(AlignFrame af, String colour)
  {
    // use string "none" to remove colour scheme
    if (colour != null && "" != colour)
    {
      ColourSchemeI cs = ColourSchemeProperty.getColourScheme(
              af.getViewport(), af.getViewport().getAlignment(), colour);
      if (cs == null && !StringUtils.equalsIgnoreCase(colour, "none"))
      {
        addWarn("Couldn't parse '" + colour + "' as a colourscheme.");
      }
      else
      {
        Jalview.testoutput(argParser, Arg.COLOUR, "zappo", colour);
        colourAlignFrame(af, cs);
      }
    }
  }

  private void colourAlignFrame(AlignFrame af, ColourSchemeI cs)
  {
    try
    {
      SwingUtilities.invokeAndWait(new Runnable()
      {
        @Override
        public void run()
        {
          // Note that cs == null removes colour scheme from af
          af.changeColour(cs);
        }
      });
    } catch (Exception x)
    {
      Console.trace("Interrupted whilst waiting for colorAlignFrame action",
              x);

    }
  }

  private ColourSchemeI getColourScheme(AlignFrame af)
  {
    return af.getViewport().getGlobalColourScheme();
  }

  private void addInfo(String errorMessage)
  {
    Console.info(errorMessage);
    errors.add(errorMessage);
  }

  private void addWarn(String errorMessage)
  {
    Console.warn(errorMessage);
    errors.add(errorMessage);
  }

  private void addError(String errorMessage)
  {
    addError(errorMessage, null);
  }

  private void addError(String errorMessage, Exception e)
  {
    Console.error(errorMessage, e);
    errors.add(errorMessage);
  }

  private boolean checksBeforeWritingToFile(ArgValuesMap avm,
          SubVals subVal, boolean includeBackups, String filename,
          String adjective, Boolean isError)
  {
    File file = new File(filename);

    boolean overwrite = avm.getFromSubValArgOrPref(Arg.OVERWRITE, subVal,
            null, "OVERWRITE_OUTPUT", false);
    boolean stdout = false;
    boolean backups = false;
    if (includeBackups)
    {
      stdout = ArgParser.STDOUTFILENAME.equals(filename);
      // backups. Use the Arg.BACKUPS or subval "backups" setting first,
      // otherwise if headless assume false, if not headless use the user
      // preference with default true.
      backups = avm.getFromSubValArgOrPref(Arg.BACKUPS, subVal, null,
              Platform.isHeadless() ? null : BackupFiles.ENABLED,
              !Platform.isHeadless());
    }

    if (file.exists() && !(overwrite || backups || stdout))
    {
      addWarn("Won't overwrite file '" + filename + "' without "
              + Arg.OVERWRITE.argString()
              + (includeBackups ? " or " + Arg.BACKUPS.argString() : "")
              + " set");
      return false;
    }

    boolean mkdirs = avm.getFromSubValArgOrPref(Arg.MKDIRS, subVal, null,
            "MKDIRS_OUTPUT", false);

    if (!FileUtils.checkParentDir(file, mkdirs))
    {
      addError("Directory '"
              + FileUtils.getParentDir(file).getAbsolutePath()
              + "' does not exist for " + adjective + " file '" + filename
              + "'."
              + (mkdirs ? "" : "  Try using " + Arg.MKDIRS.argString()));
      isError = true;
      return false;
    }

    return true;
  }

  public List<String> getErrors()
  {
    return errors;
  }

  public String errorsToString()
  {
    StringBuilder sb = new StringBuilder();
    for (String error : errors)
    {
      if (sb.length() > 0)
        sb.append("\n");
      sb.append("- " + error);
    }
    return sb.toString();
  }
}
