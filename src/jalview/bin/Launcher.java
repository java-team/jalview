/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.bin;

import java.io.File;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.List;

import jalview.bin.argparser.Arg;
import jalview.util.ChannelProperties;
import jalview.util.ErrorLog;
import jalview.util.LaunchUtils;

/**
 * A Launcher class for Jalview. This class is used to launch Jalview from the
 * shadowJar when Getdown is not used or available. It attempts to take all the
 * command line arguments to pass on to the jalview.bin.Jalview class, but to
 * insert a -Xmx memory setting to a sensible default, using the --jvmmempc and
 * -jvmmemmax application arguments if specified. If not specified then system
 * properties will be looked for by jalview.bin.MemorySetting. If the user has
 * provided the JVM with a -Xmx setting directly and not set --jvmmempc or
 * --jvmmemmax then this setting will be used and system properties ignored. If
 * -Xmx is set as well as --jvmmempc or --jvmmemmax as argument(s) then the -Xmx
 * argument will NOT be passed on to the main application launch.
 * 
 * @author bsoares
 *
 */
public class Launcher
{
  private final static String startClass = "jalview.bin.Jalview";

  // not setting this yet due to problem with Jmol
  private final static String headlessProperty = "java.awt.headless";

  // used for headless macOS
  private final static String macosHeadlessProperty = "apple.awt.UIElement";

  // arguments that assume headless mode
  private final static String[] assumeHeadlessArgs = { "headless", "output",
      "image", "structureimage" };

  /**
   * main method for jalview.bin.Launcher. This restarts the same JRE's JVM with
   * the same arguments but with memory adjusted based on extracted --jvmmempc
   * and --jvmmemmax application arguments. If on a Mac then extra dock:icon and
   * dock:name arguments are also set.
   * 
   * @param args
   */
  public static void main(String[] args)
  {
    if (!LaunchUtils.checkJavaVersion())
    {
      jalview.bin.Console
              .errPrintln("WARNING - The Java version being used (Java "
                      + LaunchUtils.getJavaVersion()
                      + ") may lead to problems. This installation of Jalview should be used with Java "
                      + LaunchUtils.getJavaCompileVersion() + ".");
    }

    ErrorLog.setPrefix("LAUNCHER - ");

    String jvmmempc = null;
    String jvmmemmax = null;
    boolean debug = false;
    boolean wait = true;
    boolean quiet = false;
    boolean headless = false;
    boolean assumeheadless = false;
    boolean gui = false;
    boolean help = false;
    boolean stdout = false;
    // must set --debug before --launcher...
    boolean launcherstop = false;
    boolean launcherprint = false;
    boolean launcherwait = false;
    ArrayList<String> arguments = new ArrayList<>();
    String previousArg = null;
    // set debug first
    for (String arg : args)
    {
      if (arg.equals("--debug"))
      {
        debug = true;
      }
    }
    for (String arg : args)
    {
      if (arg.equals("--quiet"))
      {
        quiet = true;
      }
      else if (arg.equals("--gui"))
      {
        gui = true;
      }
      else if (arg.equals("--help"))
      {
        help = true;
      }
      else if (arg.equals("--version"))
      {
        help = true;
      }

      if (!assumeheadless)
      {
        for (String a : assumeHeadlessArgs)
        {
          if (arg.equals("--" + a) || arg.startsWith("--" + a + "="))
          {
            assumeheadless = true;
          }
        }
      }

      if (arg.equals("--output=-")
              || (arg.equals("-") && "--output".equals(previousArg)))
      {
        stdout = true;
      }

      if (debug)
      {
        if (arg.equals("--launcherprint"))
        {
          launcherprint = true;
        }
        else if (arg.equals("--launcherstop"))
        {
          launcherstop = true;
        }
        else if (arg.equals("--launcherwait"))
        {
          launcherwait = true;
        }
        else
        // this ends the launcher immediately
        if (arg.equals("--launchernowait"))
        {
          wait = false;
        }
      }
      previousArg = arg;
      // Don't add the --launcher... args to Jalview launch
      if (arg.startsWith("--launcher"))
      {
        continue;
      }
      // jvmmempc and jvmmemmax args used to set memory and are not passed on to
      // startClass
      final String jvmmempcArg = Arg.JVMMEMPC.getName();
      final String jvmmemmaxArg = Arg.JVMMEMMAX.getName();
      if (arg.startsWith("-" + jvmmempcArg + "="))
      {
        jvmmempc = arg.substring(jvmmempcArg.length() + 2);
      }
      else if (arg.startsWith("-" + jvmmemmaxArg + "="))
      {
        jvmmemmax = arg.substring(jvmmemmaxArg.length() + 2);
      }
      // --doubledash versions
      else if (arg.startsWith("--" + jvmmempcArg + "="))
      {
        jvmmempc = arg.substring(jvmmempcArg.length() + 3);
      }
      else if (arg.startsWith("--" + jvmmemmaxArg + "="))
      {
        jvmmemmax = arg.substring(jvmmemmaxArg.length() + 3);
      }
      // retain arg
      else
      {
        arguments.add(arg);
      }
    }
    if (help)
    {
      // --help takes precedence over --gui
      headless = true;
    }
    else if (gui)
    {
      // --gui takes precedence over --headless
      headless = false;
    }
    else
    {
      // --output arguments assume headless mode
      headless = assumeheadless;
    }

    ErrorLog.setQuiet(quiet);

    final String appName = ChannelProperties.getProperty("app_name");

    // if we're using jalview.bin.Launcher we always assume a console is in use
    final String javaBin = LaunchUtils.findJavaBin(true);

    List<String> removeJvmArgs = new ArrayList<>();
    List<String> addJvmArgs = new ArrayList<>();
    // command.add(javaBin);

    String memSetting = null;

    List<String> jvmArgs = ManagementFactory.getRuntimeMXBean()
            .getInputArguments();

    // use saved preferences if no cmdline args
    boolean useCustomisedSettings = LaunchUtils
            .getBooleanUserPreference(MemorySetting.CUSTOMISED_SETTINGS);
    if (useCustomisedSettings)
    {
      if (jvmmempc == null)
      {
        jvmmempc = LaunchUtils
                .getUserPreference(MemorySetting.MEMORY_JVMMEMPC);
      }
      if (jvmmemmax == null)
      {
        jvmmemmax = LaunchUtils
                .getUserPreference(MemorySetting.MEMORY_JVMMEMMAX);
      }
    }

    // add these settings if not already specified
    boolean memSet = false;
    boolean dockIcon = false;
    boolean dockName = false;
    boolean headlessProp = false;
    boolean macosHeadlessProp = false;
    for (int i = 0; i < jvmArgs.size(); i++)
    {
      String arg = jvmArgs.get(i);
      if (arg.startsWith("-Xmx"))
      {
        // only use -Xmx if jvmmemmax and jvmmempc have not been set
        if (jvmmempc == null && jvmmemmax == null)
        {
          memSetting = arg;
          memSet = true;
        }
      }
      else if (arg.startsWith("-Xdock:icon"))
      {
        dockIcon = true;
      }
      else if (arg.startsWith("-Xdock:name"))
      {
        dockName = true;
      }
      else if (arg.startsWith("-D" + headlessProperty + "="))
      {
        headlessProp = true;
      }
      else if (arg.startsWith("-D" + macosHeadlessProperty + "="))
      {
        macosHeadlessProp = true;
      }
    }

    if (!memSet)
    {
      long maxMemLong = MemorySetting.getMemorySetting(jvmmemmax, jvmmempc);

      if (maxMemLong > 0)
      {
        memSetting = "-Xmx" + Long.toString(maxMemLong);
        memSet = true;
        addJvmArgs.add(memSetting);
        removeJvmArgs.add("-Xmx");
      }
    }

    if (LaunchUtils.isMac)
    {
      if (!dockIcon)
      {
        String dockIconPath = System.getProperty("launcher.appdir", ".")
                + File.separator + "resource" + File.separator
                + "jalview_logo.png";
        addJvmArgs.add("-Xdock:icon=" + dockIconPath);
      }
      if (!dockName)
      {
        // -Xdock:name=... doesn't actually work :(
        // Leaving it in in case it gets fixed
        addJvmArgs.add("-Xdock:name=" + appName);
        // This also does not work for the dock
        addJvmArgs.add("-Dcom.apple.mrj.application.apple.menu.about.name="
                + appName);
      }
    }
    if (headless && !headlessProp)
    {
      System.setProperty(headlessProperty, "true");
      /* not setting this in java invocation of running jalview due to problem with Jmol */
      if (help)
      {
        addJvmArgs.add("-D" + headlessProperty + "=true");
      }
    }
    if (headless && LaunchUtils.isMac && !macosHeadlessProp)
    {
      System.setProperty(macosHeadlessProperty, "true");
      addJvmArgs.add("-D" + macosHeadlessProperty + "=true");
    }

    String scalePropertyArg = HiDPISetting.getScalePropertyArg();
    if (scalePropertyArg != null)
    {
      ErrorLog.errPrintln("Running " + startClass + " with scale setting "
              + scalePropertyArg);
      addJvmArgs.add(scalePropertyArg);
    }

    int exitValue = LaunchUtils.startNewJvm(javaBin, removeJvmArgs,
            addJvmArgs, null, null, null, startClass, null, null, arguments,
            launcherprint, launcherwait, launcherstop, debug, quiet);

    ErrorLog.errPrintln("JVM exited with value " + exitValue);
  }

}
