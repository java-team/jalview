/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.bin.argparser;

import jalview.bin.argparser.Arg.Opt;
import jalview.bin.argparser.Arg.Type;

/**
 * A helper class to keep an index of argument position with argument values
 */
public class ArgValue implements Comparable<ArgValue>
{
  private Arg arg;

  private int argIndex;

  private String value;

  private String givenLinkedId = null;

  private String assignedLinkedId = null;

  private boolean setByWildcardLinkedId = false;

  /*
   * Type type is only really used by --help-type
   */
  private Type type = null;

  /*
   * This id is set by a subVal id= to identify the product of this ArgValue
   * later. Set but not currently used.
   */
  private String id;

  private SubVals subVals;

  protected ArgValue(Arg a, SubVals sv, Type type, String content,
          int argIndex, boolean setByWildcardLinkedId, String givenLinkedId,
          String assignedLinkedId)
  {
    this.arg = a;
    this.value = content;
    this.argIndex = argIndex;
    this.subVals = sv == null ? new SubVals("") : sv;
    this.setType(type);
    this.setByWildcardLinkedId = setByWildcardLinkedId;
    this.givenLinkedId = givenLinkedId;
    this.assignedLinkedId = assignedLinkedId;
  }

  protected ArgValue(Arg a, Type type, String value, int argIndex,
          boolean setByWildcardLinkedId, String givenLinkedId,
          String assignedLinkedId)
  {
    this.arg = a;
    this.argIndex = argIndex;
    this.subVals = new SubVals(value);
    this.value = getSubVals().getContent();
    this.setType(type);
    this.setByWildcardLinkedId = setByWildcardLinkedId;
    this.givenLinkedId = givenLinkedId;
    this.assignedLinkedId = assignedLinkedId;
  }

  protected void setType(Type t)
  {
    if (this.getArg().hasOption(Opt.HASTYPE))
      this.type = t;
  }

  public Type getType()
  {
    return type;
  }

  public Arg getArg()
  {
    return arg;
  }

  public String getValue()
  {
    return value;
  }

  public int getArgIndex()
  {
    return argIndex;
  }

  protected void setId(String i)
  {
    id = i;
  }

  public String getId()
  {
    return id;
  }

  public SubVals getSubVals()
  {
    return subVals;
  }

  public String getSubVal(String key)
  {
    if (subVals == null || !subVals.has(key))
      return null;
    return subVals.get(key);
  }

  protected void putSubVal(String key, String val)
  {
    this.subVals.put(key, val);
  }

  @Override
  public final int compareTo(ArgValue o)
  {
    return this.getArgIndex() - o.getArgIndex();
  }

  @Override
  public String toString()
  {
    StringBuilder sb = new StringBuilder();
    sb.append(this.getArg().argString());
    sb.append("=");
    if (!this.getSubVals().getSubValMap().isEmpty())
    {
      sb.append(this.getSubVals().toString());
    }
    sb.append("'");
    sb.append(this.getValue());
    sb.append("'");

    return sb.toString();
  }

  public String getGivenLinkedId()
  {
    return this.givenLinkedId;
  }

  public String getAssignedLinkedId()
  {
    return this.assignedLinkedId;
  }

  public boolean setByWildcardLinkedId()
  {
    // looking for deliberately user set wildcard
    return this.setByWildcardLinkedId && this.getGivenLinkedId() != null;
  }
}