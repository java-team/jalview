/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.bin.groovy;

import jalview.bin.Jalview.ExitCode;
import jalview.gui.AlignFrame;

public class JalviewObject implements JalviewObjectI
{
  private JalviewObjectI object = null;

  public JalviewObject(JalviewObjectI j)
  {
    this.object = j;
  }

  @Override
  public AlignFrame[] getAlignFrames()
  {
    return object == null ? null : object.getAlignFrames();
  }

  @Override
  public AlignFrame getCurrentAlignFrame()
  {
    return object == null ? null : object.getCurrentAlignFrame();
  }

  @Override
  public void quit()
  {
    if (object != null)
    {
      object.quit();
    }
    else
    {
      jalview.bin.Jalview.exit(
              "Groovy console quit without Jalview object.",
              ExitCode.GROOVY_ERROR);
    }
  }

}
