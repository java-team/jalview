/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.bin;

import java.io.File;

import com.threerings.getdown.data.EnvConfig;
import com.threerings.getdown.util.LaunchUtil;

public class GetdownLauncherUpdate
{
  public static void main(String[] args)
  {
    EnvConfig.setVarsFromProperties();

    String appdir = args.length > 0 ? args[0] : null;
    if (appdir == null || appdir.length() == 0)
    {
      appdir = System.getProperty("launcher.appdir", null);
    }
    if (appdir == null)
    {
      appdir = EnvConfig.getUserAppdir();
    }
    if (appdir == null)
    {
      System.err.println("Not running upgradeGetdown");
      return;
    }
    boolean debug = false;
    for (int i = 0; i < args.length; i++)
    {
      if ("--debug".equals(args[i]))
      {
        debug = true;
        break;
      }
    }
    if (debug)
    {
      System.err.println("Running upgradeGetdown");
    }
    File appdirFile = new File(appdir);
    if (!appdirFile.exists())
    {
      System.err.println("Directory '" + appdirFile.getAbsolutePath()
              + "' doesn't exist, cannot update getdown-launcher.jar.");
      if (Boolean.valueOf(System.getProperty("launcher.update")))
      {
        System.exit(1);
      }
    }
    LaunchUtil.upgradeGetdown(new File(appdir, "getdown-launcher-old.jar"),
            new File(appdir, "getdown-launcher.jar"),
            new File(appdir, "getdown-launcher-new.jar"));
  }
}
