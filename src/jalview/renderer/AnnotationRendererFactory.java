/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.renderer;

import jalview.datamodel.AlignmentAnnotation;
import jalview.renderer.api.AnnotationRendererFactoryI;
import jalview.renderer.api.AnnotationRowRendererI;
import jalview.ws.datamodel.alphafold.PAEContactMatrix;

import java.util.IdentityHashMap;

public class AnnotationRendererFactory implements AnnotationRendererFactoryI
{

  private static AnnotationRendererFactoryI factory = null;

  public static AnnotationRendererFactoryI getRendererFactory()
  {
    if (factory == null)
    {
      factory = new AnnotationRendererFactory();
    }
    return factory;
  }

  IdentityHashMap<Object, AnnotationRowRendererI> renderers = new IdentityHashMap<Object, AnnotationRowRendererI>();

  public AnnotationRendererFactory()
  {
    // renderers.put)
  }

  @Override
  public AnnotationRowRendererI getRendererFor(AlignmentAnnotation row)
  {
    if (row.graph == AlignmentAnnotation.CONTACT_MAP)
    {
      // TODO consider configuring renderer/etc according to the type of matrix
      // bound to the annotation row - needs to be looked up in that case
      if (PAEContactMatrix.PAEMATRIX.equals(row.getCalcId()))
      {
        return ContactMapRenderer.newPAERenderer();
      }
      // TODO add potential for configuring renderer directly from the
      // annotation row and/or viewmodel

    }
    return null;
  }

}
