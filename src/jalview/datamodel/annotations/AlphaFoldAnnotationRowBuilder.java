/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.datamodel.annotations;

import jalview.datamodel.Annotation;
import jalview.structure.StructureImportSettings;
import jalview.structure.StructureImportSettings.TFType;

public class AlphaFoldAnnotationRowBuilder extends AnnotationRowBuilder
{
  public static final String LABEL = "Alphafold Reliability";

  public AlphaFoldAnnotationRowBuilder()
  {
    super(LABEL);
    min = 0;
    max = 100;
    hasMinMax = true;
    this.setTFType(StructureImportSettings.TFType.PLDDT);
  }

  @Override
  public void processAnnotation(Annotation annotation)
  {
    if (annotation.value > 90)
    {
      // Very High
      annotation.colour = new java.awt.Color(0, 83, 214);
    }
    if (annotation.value <= 90)
    {
      // High
      annotation.colour = new java.awt.Color(101, 203, 243);
    }
    if (annotation.value <= 70)
    {
      // Confident
      annotation.colour = new java.awt.Color(255, 219, 19);
    }
    if (annotation.value < 50)
    {
      annotation.colour = new java.awt.Color(255, 125, 69);
    }
  }
}