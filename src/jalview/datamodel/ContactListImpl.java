/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.datamodel;

import java.awt.Color;

import jalview.renderer.ContactGeometry.contactInterval;

/**
 * helper class to compute min/max/mean for a range on a contact list
 * 
 * @author jprocter
 *
 */
public class ContactListImpl implements ContactListI
{
  ContactListProviderI clist;

  public static ContactListI newContactList(ContactListProviderI list)
  {
    return new ContactListImpl(list);
  }

  public ContactListImpl(ContactListProviderI list)
  {
    clist = list;
  }

  @Override
  public int getPosition()
  {
    return clist.getPosition();
  }

  @Override
  public double getContactAt(int column)
  {
    return clist.getContactAt(column);
  }

  @Override
  public int getContactHeight()
  {
    return clist.getContactHeight();
  }

  @Override
  public ContactRange getRangeFor(int from_column, int to_column)
  {
    // TODO: consider caching ContactRange for a particular call ?
    if (clist instanceof ContactListI)
    {
      // clist may implement getRangeFor in a more efficient way, so use theirs
      return ((ContactListI) clist).getRangeFor(from_column, to_column);
    }
    if (from_column < 0)
    {
      from_column = 0;
    }
    if (to_column >= getContactHeight())
    {
      to_column = getContactHeight() - 1;
    }
    ContactRange cr = new ContactRange();
    cr.setFrom_column(from_column);
    cr.setTo_column(to_column);
    double tot = 0;
    for (int i = from_column; i <= to_column; i++)
    {
      double contact = getContactAt(i);
      tot += contact;
      if (i == from_column)
      {
        cr.setMin(contact);
        cr.setMax(contact);
        cr.setMinPos(i);
        cr.setMaxPos(i);
      }
      else
      {
        if (cr.getMax() < contact)
        {
          cr.setMax(contact);
          cr.setMaxPos(i);
        }
        if (cr.getMin() < contact)
        {
          cr.setMin(contact);
          cr.setMinPos(i);
        }
      }
    }
    if (tot > 0 && to_column > from_column)
    {
      cr.setMean(tot / (1 + to_column - from_column));
    }
    else
    {
      cr.setMean(tot);
    }
    return cr;
  }

  @Override
  public int[] getMappedPositionsFor(int cStart, int cEnd)
  {
    return clist.getMappedPositionsFor(cStart, cEnd);
  }

  @Override
  public Color getColourForGroup()
  {
    return clist.getColourForGroup();
  }
}
