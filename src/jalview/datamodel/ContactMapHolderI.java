/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.datamodel;

import java.util.Collection;

public interface ContactMapHolderI
{
  /**
   * resolve a contact list instance (if any) associated with the annotation row
   * and column position Columns of ContactMap are indexed relative to context
   * object (columns of alignment, positions on sequence relative to
   * sequence.getStart())
   * 
   * @param _aa
   * @param column
   *          - base 0 column index
   * @return
   */
  ContactListI getContactListFor(AlignmentAnnotation _aa, int column);

  AlignmentAnnotation addContactList(ContactMatrixI cm);

  Collection<ContactMatrixI> getContactMaps();

  public ContactMatrixI getContactMatrixFor(AlignmentAnnotation ann);

  void addContactListFor(AlignmentAnnotation annotation, ContactMatrixI cm);

}
