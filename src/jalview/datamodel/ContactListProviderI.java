/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.datamodel;

import java.awt.Color;

public interface ContactListProviderI
{

  /**
   * 
   * @return position index for this contact List (usually sequence position or
   *         alignment column)
   */
  int getPosition();

  /**
   * dimension of list where getContactAt(column<getContactHeight()) may return
   * a value
   * 
   * @return
   */
  int getContactHeight();

  /**
   * get a value representing contact at column for this site
   * 
   * @param column
   * @return Double.NaN or a contact strength for this site
   */
  double getContactAt(int column);

  /**
   * Return positions in local reference corresponding to cStart and cEnd in
   * matrix data. Positions are base 1 column indices for sequence associated
   * matrices.
   * 
   * @param cStart
   * @param cEnd
   * @return int[] { start, end (inclusive) for each contiguous segment}
   */
  default int[] getMappedPositionsFor(int cStart, int cEnd)
  {
    return new int[] { cStart, cEnd };
  }

  default Color getColourForGroup()
  {
    return null;
  }

}
