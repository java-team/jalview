/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.datamodel;

public class FloatContactMatrix extends GroupSetHolder
        implements ContactMatrixI
{

  int maxrow = 0, maxcol = 0;

  float[][] elements;

  float maxscore;

  public FloatContactMatrix(float[][] matrix)
  {
    maxcol = 0;
    for (float[] row : matrix)
    {
      if (row.length > maxcol)
      {
        maxcol = row.length;
      }
      maxscore = row[0];
      for (float f : row)
      {
        if (maxscore < f)
        {
          maxscore = f;
        }
      }
    }
    maxrow = matrix.length;
    elements = matrix;
  }

  public FloatContactMatrix(float[][] elements2, GroupSet grps2)
  {
    this(elements2);
    setGroupSet(grps2);
  }

  /**
   * getContactList(column) @returns the vector of predicted alignment errors
   * for reference position given by column
   */
  @Override
  public ContactListI getContactList(final int column)
  {
    if (column < 0 || column >= elements.length)
    {
      return null;
    }

    return new ContactListImpl(new ContactListProviderI()
    {
      @Override
      public int getPosition()
      {
        return column;
      }

      @Override
      public int getContactHeight()
      {
        return maxcol - 1;
      }

      @Override
      public double getContactAt(int mcolumn)
      {
        if (mcolumn < 0 || mcolumn >= elements[column].length)
        {
          return -1;
        }
        return elements[column][mcolumn];
      }
    });
  }

  /**
   * getElementAt(column, i) @returns the predicted superposition error for the
   * ith position when column is used as reference
   */
  @Override
  public double getElementAt(int _column, int i)
  {
    return elements[_column][i];
  }

  @Override
  public float getMin()
  {
    return 0;
  }

  @Override
  public float getMax()
  {
    return maxscore;
  }

  @Override
  public String getAnnotDescr()
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public String getAnnotLabel()
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public String getType()
  {
    return null;
  }

  @Override
  public int getWidth()
  {
    return maxcol;
  }

  @Override
  public int getHeight()
  {
    return maxrow;
  }
}
