/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.datamodel;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import jalview.ws.datamodel.MappableContactMatrixI;

public class ContactMapHolder implements ContactMapHolderI
{

  Map<Object, ContactMatrixI> contactmaps = new HashMap<>();

  @Override
  public Collection<ContactMatrixI> getContactMaps()
  {
    if (contactmaps != null && contactmaps.size() > 0)
    {
      // defensive copy, and return non redundant set of ContactMatrixI
      // instances
      return new HashSet<ContactMatrixI>(contactmaps.values());
    }
    return Collections.EMPTY_LIST;
  }

  @Override
  public ContactListI getContactListFor(AlignmentAnnotation _aa, int column)
  {
    ContactMatrixI cm = contactmaps.get(_aa.annotationId);
    if (cm == null)
    {
      return null;
    }
    if (cm instanceof MappableContactMatrixI)
    {
      if (_aa.sequenceRef != null)
      {
        return ((MappableContactMatrixI) cm)
                .getMappableContactList(_aa.sequenceRef, column);
      }
    }
    // TODO: could resolve sequence position to column position here
    // TODO: what about for complexes - where contactMatrix may involve two or
    // more sequences
    return cm.getContactList(column);
  }

  @Override
  public AlignmentAnnotation addContactList(ContactMatrixI cm)
  {

    AlignmentAnnotation aa = new AlignmentAnnotation(cm.getAnnotLabel(),
            cm.getAnnotDescr(), new Annotation[0]);
    aa.graph = AlignmentAnnotation.CONTACT_MAP;
    aa.graphMin = cm.getMin();
    aa.graphMax = cm.getMax();
    aa.editable = false;
    aa.calcId = cm.getType();

    contactmaps.put(aa.annotationId, cm);
    // TODO: contact matrices could be intra or inter - more than one refseq
    // possible!
    if (cm instanceof MappableContactMatrixI)
    {
      aa.setSequenceRef(((MappableContactMatrixI) cm).getReferenceSeq());
    }
    return aa;
  }

  @Override
  public ContactMatrixI getContactMatrixFor(AlignmentAnnotation ann)
  {
    return contactmaps == null ? null : contactmaps.get(ann.annotationId);
  }

  @Override
  public void addContactListFor(AlignmentAnnotation annotation,
          ContactMatrixI cm)
  {
    // update annotation with data from contact map
    annotation.graphMin = cm.getMin();
    annotation.graphMax = cm.getMax();
    annotation.editable = false;
    annotation.graph = AlignmentAnnotation.CONTACT_MAP;
    annotation.calcId = cm.getType();
    if (annotation.label == null || "".equals(annotation.label))
    {
      annotation.label = cm.getAnnotLabel();

    }
    if (annotation.description == null || "".equals(annotation.description))
    {
      annotation.description = cm.getAnnotDescr();
    }
    contactmaps.put(annotation.annotationId, cm);
  }
}
