/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.datamodel;

/**
 * A profile for one column of an alignment
 * 
 * @author gmcarstairs
 *
 */
public class Profile implements ProfileI
{
  /*
   * an object holding counts of symbols in the profile
   */
  private ResidueCount counts;

  private SecondaryStructureCount ssCounts;

  /*
   * the number of sequences (gapped or not) in the profile
   */
  private int height;

  /*
   * the number of non-gapped sequences in the profile
   */
  private int gapped;

  /*
   * the highest count for any residue in the profile
   */
  private int maxCount;

  private int maxSSCount;

  /*
   * the residue (e.g. K) or residues (e.g. KQW) with the
   * highest count in the profile
   */
  private String modalResidue;

  private String modalSS;

  /**
   * Constructor which allows derived data to be stored without having to store
   * the full profile
   * 
   * @param seqCount
   *          the number of sequences in the profile
   * @param gaps
   *          the number of gapped sequences
   * @param max
   *          the highest count for any residue
   * @param modalres
   *          the residue (or concatenated residues) with the highest count
   */
  public Profile(int seqCount, int gaps, int max, String modalRes)
  {
    this.height = seqCount;
    this.gapped = gaps;
    this.maxCount = max;
    this.modalResidue = modalRes;
  }

  public Profile(String modalSS, int ssCount, int gaps, int maxSSCount)
  {
    this.height = ssCount;
    this.gapped = gaps;
    this.maxSSCount = maxSSCount;
    this.modalSS = modalSS;
  }

  /* (non-Javadoc)
   * @see jalview.datamodel.ProfileI#setCounts(jalview.datamodel.ResidueCount)
   */
  @Override
  public void setCounts(ResidueCount residueCounts)
  {
    this.counts = residueCounts;
  }

  @Override
  public void setSSCounts(SecondaryStructureCount secondaryStructureCount)
  {
    this.ssCounts = secondaryStructureCount;
  }

  /* (non-Javadoc)
   * @see jalview.datamodel.ProfileI#getPercentageIdentity(boolean)
   */
  @Override
  public float getPercentageIdentity(boolean ignoreGaps)
  {
    if (height == 0)
    {
      return 0f;
    }
    float pid = 0f;
    if (ignoreGaps && gapped < height)
    {
      pid = (maxCount * 100f) / (height - gapped);
    }
    else
    {
      pid = (maxCount * 100f) / height;
    }
    return pid;
  }

  @Override
  public float getSSPercentageIdentity(boolean ignoreGaps)
  {
    if (height == 0)
    {
      return 0f;
    }
    float ssPid = 0f;
    if (ignoreGaps && gapped < height)
    {
      ssPid = (maxSSCount * 100f) / (height - gapped);
    }
    else
    {
      ssPid = (maxSSCount * 100f) / height;
    }
    return ssPid;
  }

  /* (non-Javadoc)
   * @see jalview.datamodel.ProfileI#getCounts()
   */
  @Override
  public ResidueCount getCounts()
  {
    return counts;
  }

  @Override
  public SecondaryStructureCount getSSCounts()
  {
    return ssCounts;
  }

  /* (non-Javadoc)
   * @see jalview.datamodel.ProfileI#getHeight()
   */
  @Override
  public int getHeight()
  {
    return height;
  }

  /* (non-Javadoc)
   * @see jalview.datamodel.ProfileI#getGapped()
   */
  @Override
  public int getGapped()
  {
    return gapped;
  }

  /* (non-Javadoc)
   * @see jalview.datamodel.ProfileI#getMaxCount()
   */
  @Override
  public int getMaxCount()
  {
    return maxCount;
  }

  @Override
  public int getMaxSSCount()
  {
    return maxSSCount;
  }

  /* (non-Javadoc)
   * @see jalview.datamodel.ProfileI#getModalResidue()
   */
  @Override
  public String getModalResidue()
  {
    return modalResidue;
  }

  @Override
  public String getModalSS()
  {
    return modalSS;
  }

  /* (non-Javadoc)
   * @see jalview.datamodel.ProfileI#getNonGapped()
   */
  @Override
  public int getNonGapped()
  {
    return height - gapped;
  }
}
