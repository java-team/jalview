/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.datamodel;

import java.awt.Color;
import java.util.BitSet;
import java.util.List;

public interface GroupSetI
{
  boolean hasGroups();

  String getNewick();

  boolean hasTree();

  void updateGroups(List<BitSet> colGroups);

  BitSet getGroupsFor(int column);

  Color getColourForGroup(BitSet bs);

  void setColorForGroup(BitSet bs, Color color);

  void restoreGroups(List<BitSet> newgroups, String treeMethod, String tree,
          double thresh2);

  boolean hasCutHeight();

  double getCutHeight();

  String getTreeMethod();

  List<BitSet> getGroups();

}
