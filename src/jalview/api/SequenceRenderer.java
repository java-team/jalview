/*
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 */
package jalview.api;

import jalview.datamodel.SequenceGroup;
import jalview.datamodel.SequenceI;
import jalview.renderer.seqfeatures.FeatureColourFinder;

import java.awt.Color;
import java.awt.Graphics;

public interface SequenceRenderer
{

  Color getResidueColour(SequenceI seq, int position,
          FeatureColourFinder finder);

  /**
   * Configure the Graphics canvas and render options for the renderer
   * 
   * @param g
   *          - the canvas to render to
   * @param renderGaps
   *          - when true, gap characters will be included when rendered
   * 
   */
  void prepare(Graphics g, boolean renderGaps);

  void drawSequence(SequenceI nextSeq, SequenceGroup[] findAllGroups,
          int startRes, int endRes, int i);

  void drawHighlightedText(SequenceI nextSeq, int i, int j, int k, int l);

  void drawCursor(Graphics g, char s, int i, int j);

}
