---
version: 2.11.4.0
date: 2024-09-18
channel: "release"
---

## New Features

- <!-- JAL-4159,JAL-4390, JAL-4446 --> Calculate PASiMap projection for sequences - ported by Thomas Morell ( U. Konstanz)
- <!-- JAL-4392 --> Consensus secondary structure visualization for alignments
- <!-- JAL-4411 --> Show data source for 'reference annotation' from 3D structure (e.g. Secondary Structure)
- <!-- JAL-4386 --> Calculate tree or PCA using secondary structure annotation
<!-- not complete in this version - JAL-789,JAL-4257 -- allow adjustment of gap opening, extension, and score model for built in pairwise alignment -->
- <!-- JAL-4447 --> Pairwise alignment can be performed with different substitution matrices
- <!-- JAL-4447 --> PCA, Pairwise alignment, trees and PaSiMap window titles include the matrix and additional parameters used
- <!-- JAL-4461 --> "Select format by extension" option in Jalview's "Save As" dialog's file type dropdown menu

### Experimental Features (enable via Desktop's tools menu)

- <!-- JAL-1124 --> PCA and PaSiMap panels allow Right-click and drag to select several sequences at once  
- <!-- JAL-4420 --> Jalview sensibly handles opening or a drag'n'drop of several .features, .annotations and .newick files onto an alignment
<!-- not yet! - -- JAL-4366  	Linked visualisation of Foldseek 3Di MSAs with corresponding 3D structures -->

### development and deployment

- <!-- JAL-3830,JAL-4444 --> Add a command-line wrapper script to macOS bundle, linux and Windows installations (bash, powershell and .bat wrappers)
- <!-- JAL-3384 --> 	Redirect stdout and stderr to file when launched from getdown
- <!-- JAL-4428,JAL-4421,JAL-3393 --> OSX DMGs have custom icons and mount point includes architecture and version details
- <!-- JAL-3845 --> 	Add error message when launched directly from Jalview Installer DMG volume
- <!-- JAL-3631,JAL-4104 --> 	Allow jalview auto-updates to download to and work from separate user-space directory
- <!-- JAL-3978, JAL-3196 --> 	Gradle build with install4j launchers
- <!-- JAL-4409, JAL-4160 --> 	Implement in Jalview (and getdown for JVL) the use of specific scheme names for different channels (e.g. jalviewd://)
- <!-- JAL-4428 --> 	integrate OSX codesigning scripts in jalview's repository
- <!-- JAL-3063 -->	new documentation describing how jalview project XML schema is maintained (in doc/jalview-projects.md)
- <!-- JAL-3018,JAL-4449, JAL-4328 --> updated Jalview's Ensembl's client for v15.8 of the REST API - change in BRAF query returned by ensembl Genes for chicken for GRCg7b (now ENSGALG00010013466, was ENSGALG00000012865)
- <!-- JAL-4454 --> "Getdown DEVELOP Channel" linkCheck fails if there's a link to whatsNew#experimental or releaseHistory from other help pages

## Issues Resolved
- <!-- JAL-1054 --> jalview does not fetch from a URL containing a redirect
- <!-- JAL-4414 --> HTTP errors are often interpreted to the user as a file format error - and FileNotFound errors are not propagated from Web or Local file import
- <!-- JAL-3409 --> launcher.log from jalview runtime missing newlines and also lacks stdout/stderr reported in Jalview Console
- <!-- JAL-4072 --> Possible "Zip Slip Vulnerability" in getdown
- <!-- JAL-4417 --> jalview.bin.CommandsTest.headlessOrGuiImageOutputTest produces different images for --gui and --headless when using certain .jalview_properties
- <!-- JAL-4421 --> Release installer DMG does not display the background image
- <!-- JAL-4358 --> Getdown splash screen should disappear as soon as the Jalview Desktop is created/visible
- <!-- JAL-4442 --> Race condition in Jalview quit handler that can cause a hang on very slow interactive systems
- <!-- JAL-4460 --> Adjusting the saved as file format via the Alignment window Save As... dialog box doesn't actually change the format used to save the alignment
- <!-- JAL-4427,JAL-4451 --> Annotation labels from protein disorder services include html tags from description

### development and deployment

- <!-- JAL-4111 --> Removed gradle build's dependency on gradle-imagemagick plugin to allow building on windows (still need imagemagick installed on path for splashscreen commit watermark)
- <!-- JAL-4397 --> Fixed inconsistencies in test suite on different platforms
- <!-- JAL-4398 --> The rendering of the text "Window" for the Window menu item is different to the other menus when testing in Linux 


### New Known Issues

- <!-- JAL-4443 --> PaSiMap not available in JalviewJS
- <!-- JAL-4100 -->	Jalview ignores ncrna genes when importing from genbank format files
- <!-- JAL-4450 --> Sequence letter aspect ratio not set correctly when aspect ratio is very narrow after middle-mouse drag
- <!-- JAL-4464 --> cannot store/restore group associated secondary structure consensus annotations
- <!-- JAL-4349 --> spurious output image .extensions cause jalview CLI execution to fail with Null Pointer Exception (since 2.11.3.0)
