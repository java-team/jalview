---
version: 2.11.3.0
date: 2023-11-15
channel: "release"
---

## New Features
- <!-- JAL-4064 --> Native M1 build for macOS using Adoptium JRE 11 macos-aarch64
- <!-- JAL-3416 --> Jalview now uses a standard 'look and feel' (FlatLaf) on Linux, OSX and everywhere else
- <!-- JAL-4019 --> Ambiguous Base Colourscheme
- <!-- JAL-4061 --> Find can search sequence features' type and description
- <!-- JAL-4062 --> Hold down Shift + CMD/CTRL C to copy highlighted regions as new sequences
- <!-- JAL-1556 --> Quickly enable select and/or colour by for displayed annotation row via annotation panel popup menu
- <!-- JAL-4094 --> Shift+Click+Drag to adjust height of all annotation tracks of same type
- <!-- JAL-4190 --> Pressing escape in tree panel clears any current selection
- <!-- JAL-4089 --> Use selected columns for superposition
- <!-- JAL-4086 --> Highlight aligned positions on all associated structures when mousing over a column
- <!-- JAL-4221 --> sequence descriptions are updated from database reference sources if not already defined
- <!-- JAL-4273,JAL-3497 --> Visible adjuster marks to grab and adjust annotation panel height and id width
- <!-- JAL-4260 --> Adjustable ID margin when alignment is wrapped
- <!-- JAL-4274 --> Command line options and configurable bitmap export (via preferences file) for height, width and scale factor
- <!-- JAL-4307 --> Show or hide ligands in a Jmol structure view via View Ligands submenu
- <!-- JAL-4252 --> Jmol's display is antialiased by default (smoother, less pixellated)

### Improved support for working with structures and computationally determined models
- <!-- JAL-3895 --> Alphafold red/orange/yellow/green colourscheme for structures
- <!-- JAL-4095 --> Interactive picking of low pAE score regions
- <!-- JAL-4027, JAL-3858, JAL-2292 --> Predicted Alignment Error annotation tracks for structures from AlphaFold DB
- <!-- JAL-4033 --> Selections with visual feedback via contact matrix annotation
- <!-- JAL-3855 --> Discover and import alphafold2 models and metadata from https://alphafold.ebi.ac.uk/
- <!-- JAL-4091 --> Visual indication of relationship with associated sequence to distinguish different sequence associated annotation rows
- <!-- JAL-4123 --> GUI and command line allows configuration of how temperature factor in imported 3D structure data should be interpreted
- <!-- JAL-3914 --> Import model reliability scores encoded as temperature factor annotation with their correct name and semantics
- <!-- JAL-3858 --> Import and display alphafold alignment uncertainty matrices from JSON
- <!-- JAL-4134,JAL-4158 --> Column-wise alignment groups and selections and interactive tree viewer for PAE matrices
- <!-- JAL-4124 --> Store/Restore PAE data and visualisation settings from Jalview Project
- <!-- JAL-4281 --> Store and restore adjustable ID margin and annotation panel height in Jalview projects
- <!-- JAL-4083 --> Multiple residue sidechain highlighting in structure viewers from PAE mouseovers
- <!-- JAL-4147 --> Per-structure and chain shown when annotation shown sorted by label enabling secondary structure and temperature factor scores from different chains and structures for the same sequence to be visually compared.
- <!-- JAL-4216 --> PDB or mmCIF files with chains with negative RESNUMs for their whole sequence extent cannot be linked to alignments


### Jalview on the command line
- <!-- JAL-4160,JAL-629,JAL-4262,JAL-4265, --> New command line argument framework allowing flexible batch processing, import of structures, pae matrices and other sequence associated data, and alignment and structure figure generation.
- <!-- JAL-3830 --> Command-line wrapper script for macOS bundle, linux and Windows installations (bash, powershell and .bat wrappers)
- <!-- JAL-4121 --> Assume --headless when jalview is run with a command line argument that generates output
- <!-- JAL-244 --> Automatically adjust Left margin on import to avoid cropping of annotation labels & sequence IDs
- <!-- JAL-901 --> Specify alignment title on import via --title argument
- <!-- JAL-4195 --> Sensible responses from the CLI when things go wrong during image export
- <!-- JAL-4194 --> Add a command line option to set Jalview properties for this session only
- <!-- JAL-4193 --> Add a command line option to suppress opening the startup file for this session

### Other improvements
- <!-- JAL-4250 --> Secondary structure annotation glyphs are rendered anti-aliasing when enabled
- <!-- JAL-325,JAL-4250 --> Helix and Sheet glyphs vertically centred with respect to grey coil secondary structure annotation track
- <!-- JAL-4317 --> feature should be displayed when its rendering and filtering settings are adjusted
- <!-- JAL-4250 --> Updated JFreeSVG (https://www.jfree.org/jfreesvg) from 2.1 to 3.4.3
- <!-- JAL-3119 --> Name of alignment and view included in overview window's title
- <!-- JAL-4213 --> "add reference annotation" add all positions in reference annotation tracks, not just positions in the currently highlighted columns/selection range
- <!-- JAL-4119 --> EMBL-EBI SIFTS file downloads now use split directories
- <!-- JAL-3820 --> In Linux desktops' task-managers, the grouped Jalview windows get a generic name
- <!-- JAL-4206 --> Improved file chooser's 'recent files' view and added filter for 'All known alignment files'
- <!-- JAL-4206 --> Relative files added to recent files list via import from command line are selected when Jalview opened from same location
- <!-- JAL-4308 --> Reduce number of database crossreferences shown in tooltip
- <!-- JAL-4315 --> Drag and drop feature colours file on an alignment to quickly apply feature settings
- <!-- JAL-3676 --> Improved startup info in console output: which properties file was being used and slight revisions to the formatting of platform information.
- <!-- JAL-4304,JAL-4305 --> Upgrade bundled groovy to v4.0.15 and regularised interface for groovy scripts run from CLI, interactively and headlessly.
- <!-- JAL-1713,JAL-2528 --> Overview display and hidden region visibility stored in Jalview projects
- <!-- JAL-4169 --> Updated faq and documentation urls in splashscreen and help documentation

### Development and Deployment
- <!-- JAL-4054 --> Installers built with install4j10
- <!-- JAL-4167 --> Create separate gradle test task for some tests
- <!-- JAL-4212 --> Prevent gradle test on macOS continuously grabbing focus
- <!-- JAL-4111 --> Allow gradle build to create suffixed DEVELOP-... builds with channel appbase
- <!-- JAL-4288 --> Update .jvl generation in build.gradle for jalview branch builds
- <!-- JAL-4243 --> Jalview bio.tools description maintained under jalview's git repo and bundled with source release
- <!-- JAL-4110 --> Output stderr and stdout when running tests via gradle
- <!-- JAL-3599 --> New gradle task providing runtime acceptance test for JalviewJS  based on Chromium for Tests (still work in progress)


## Issues Resolved
- <!-- JAL-2961 --> Jmol view not always centred on structures when multiple structures are viewed
- <!-- JAL-3772 --> Unsaved Alignment windows close without prompting to save, individually or at application quit.
- <!-- JAL-1988 --> Can quit Jalview while 'save project' is in progress
- <!-- JAL-4126 --> 'Use original colours' option of colour by annotation not honoured when restoring view from project
- <!-- JAL-4116 --> PDB structures slow to view when Jalview Java Console is open (2.11.2.7 patch)
- <!-- JAL-3784 --> Multiple overview windows can be opened for a particular alignment view when multiple views are present
- <!-- JAL-3785 --> Overview windows opened automatically (due to preferences settings) lack title
- <!-- JAL-2353 --> Show Crossrefs fails to retrieve CDS from ENA or Ensembl for sequences retrieved from Uniprot due to version numbers in cross-reference accession
- <!-- JAL-4334 --> Uniprot parser fails to import sequences with features that have 'unknown' start or end
- <!-- JAL-4184 --> Stockholm export does not include sequence descriptions
- <!-- JAL-4075 --> Don't add string label version of DSSP secondary structure codes in secondary structure annotation rows
- <!-- JAL-4182 --> reference annotation not correctly transferred to alignment containing a sub-sequence when a selection is active
- <!-- JAL-4177 --> Can press 'Add' or 'New View' multiple times when manually adding and viewing a 3D structure via structure chooser
- <!-- JAL-4311 --> 3D beacons sources providing models scored with PLDDT and pTM not sorted against alphaFoldDB models
- <!-- JAL-4133 --> Jalview project does not preserve font aspect ratio when Viewport is zoomed with mouse
- <!-- JAL-4253,JAL-4293 --> Lower line of the sequence group border does not align with vertical and background residue box
- <!-- JAL-4128 --> Resizing overview quickly with solid-drags enabled causes exception
- <!-- JAL-4150 --> Sequences copied to clipboard from within Jalview cannot be pasted via the desktop's popup menu to a new alignment window
- <!-- JAL-2528, JAL-1713 --> Overview window is saved in project file, and state of 'show hidden regions' is preserved.
- <!-- JAL-4153 --> JvCacheableInputBoxTest flaky on build server
- <!-- JAL-4255 --> SLF4J produces an error to STDERR at Jalview startup due to missing class
- <!-- JAL-4189 --> macOS Dock and KDE taskbar names Jalview icon "java" when running jalview via the getdown app launcher
- <!-- JAL-2910 --> HeadlessException in console in headless mode (actually fixed in 2.11.{0,1,2))
- <!-- JAL-3783 --> Groovy console does not open when Jalview launched from jalview's Java 11 executable jar (available via conda) for recent versions of groovy
- <!-- JAL-4310 --> Don't offer View model page for sources that do not have model pages
- <!-- JAL-4298 --> Java Console opening at startup with the exampleFile_v2_7.jvp opening (nearly always) causes Jalview to hang
- <!-- JAL-4282 --> automatic positioning of ID width for imported project can fail for certain font size configured in User preferences (affects most versions of Jalview prior to 2.11.3.0)
- <!-- JAL-4223 --> RNAML import missing terminal residue
- <!-- JAL-3088 --> RNA secondary structure annotation final-ending-solo-brace arrow points the wrong way
- <!-- JAL-4218 --> Jalview source distribution unnecessarily includes dist directory with built jalview jar and dependencies
- <!-- JAL-3921 --> Jmol sessions involving large molecules may not be fully saved in Jalview project files (known defect in 2.11.2.x)


### New Known Issues
- <!-- JAL-4303 --> EBI-AlphaFold PLDDT colours cannot be overlaid on alignment via 'Colour by annotation' unless the alignment's colourscheme has been set to 'None' via the Colours menu.
- <!-- JAL-4302 --> Tree renderer doesn't show bottom-most leaves of tree when Fit-To-Window is enabled.
- <!-- JAL-4338 --> PAE partition not shown in the same place when tree is closed and reopened
- <!-- JAL-4178 --> Cannot cancel structure view open action once it has been started via the structure chooser dialog
- <!-- JAL-4142 --> Example project's multiple views do not open in distinct locations when eXpand views is used to show them all separately
- <!-- JAL-4127 --> 'Reload' for a jalview project results in all windows being duplicated (since 2.11.2.6, more severe in 2.11.3.0)
- <!-- JAL-4325 --> Overview shown and then closed when importing legacy jalview projects without overview store/restore information
- <!-- JAL-4165 --> Missing last letter when copying consensus sequence from alignment if first column is hidden
- <!-- JAL-4261 --> Last sequence ID in alignment not shown and annotation labels are misaligned in HTML export
- <!-- JAL-3024 --> Files opened via command line with a relative path are added as relative paths to Recent files list (since 2.0.x)
- <!-- JAL-4291 --> Test coverage for ID width adjustment disabled pending fix for new annotation label geometry and width calculation
- <!-- JAL-4323 --> scripts adding new fileformats or colourschemes do not work when run via command line
- <!-- JAL-4290 --> Headless alignment export with structure annotations doesn't include secondary structure and temperature factor
- <!-- JAL-4151 --> Copy sequences from one alignment and Pasting as new window in another alignment doesn't propagate title from original alignment's window
- <!-- JAL-4157 --> Annotation colouring dialog box doesn't remember 'use original colours' settings when opened via the Colour menu
- <!-- JAL-4327 --> When the Groovy console is open Jalview does not prompt to save before quitting  
- <!-- JAL-4328 --> Jalview works with but is not fully compatible with latest Ensembl REST API (15.6)

