---
version: 2.11.4.1
date: 2024-10-29
channel: "release"
---

## New Features

- <!-- JAL-4380 --> Line graphs, secondary structure and other tracks have informative tooltips even when not explicitly set via a jalview annotations file

## Issues Resolved

- <!-- JAL-4470 --> Annotation tracks displayed on top of each other after 'Add reference annotations'
- <!-- JAL-4471 --> Secondary structure tree calculation in 2.11.4.0 breaks for example jalview project
- <!-- JAL-4380 --> Blank tooltips shown for some annotation tracks
- <!-- JAL-4467 --> When building Jalview releases, getdownArchive task fails looking for the archive background image
