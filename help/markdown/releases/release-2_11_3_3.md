---
version: 2.11.3.3
date: 2024-05-22
channel: "release"
---

## New Features

- <!-- JAL-518 --> Left/Right justify the alignment or current selection (with Undo)
- <!-- JAL-4192 --> Minimum versions of Jalview's build toolchain are explicitly specified in doc/building.md

### development and deployment

- <!-- JAL-4413 --> timeout for test tasks to prevent hanging builds

## Issues Resolved

- <!-- JAL-4407 --> Secondary structure not available after dragging a 3D structure onto a sequence to associate it when 'Process secondary structure' flag is true in preferences
- <!-- JAL-840 --> Clustal colourscheme documentation not consistent with implementation
- <!-- JAL-4367 --> Handles to adjust ID panel width or annotation panel height can be dragged out of the Alignment window
- <!-- JAL-2934 --> Trackpad scrolling not proportional to gesture velocity
- <!-- JAL-4353 --> Cannot output individual images of more than one structure view attached to one open alignment
- <!-- JAL-4373 --> --jvmempc and --jvmemmax don't work for jalview installed via conda or jar
- <!-- JAL-4368 --> User's default colour scheme is erroneously applied to alignments in Jalview projects opened via command line when no --colour argument supplied
- <!-- JAL-4290 --> Headless alignment export with structure annotations doesn't include secondary structure and temperature factor (Known defect since 2.11.3.0)
- <!-- JAL-4369 --> Uniprot record retrieval not working in JalviewJS (affects all versions)
- <!-- JAL-4243 --> bio.tools record version number updated
- <!-- JAL-4217 --> Miscellaneous patches to fix hangs in 2.11.3 test suite
- <!-- JAL-3978 --> Jalview archive build getdown doesn't include wrapper scripts 

### New Known Issues

- <!-- JAL-3124 --> Protein structure derived annotation tracks not available as reference annotation unless 'add to alignment' preference is enabled
- <!-- JAL-4415 --> Enabling just "Add Secondary Structure" Structure preference when importing structure via CLI results in temperature factor being shown in alignment

