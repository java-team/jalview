---
version: 2.11.3.1
date: 2023-11-22
channel: "release"
---

## New Features

- <!-- JAL-4243 --> bio.tools record for jalview updated and synced with json in source release

## Issues Resolved

- <!-- JAL-4340 --> Getdown does not preserve executable permission of jalview.sh file when it gets updated
- <!-- JAL-4347 --> Single position features from Uniprot are incorrectly parsed as non-positional features
- <!-- JAL-4345 --> Double clicking in PAE annotation to select column groups doesn't always select group under the mouse
- <!-- JAL-4348 --> ChimeraX 1.3 and Chimera 1.16.2 are not automatically found

### New Known Issues

