---
version: 2.11.3.2
date: 2023-12-01
channel: "release"
---

## New Features

## Issues Resolved

- <!-- JAL-4344 --> --structureimage exports an image without the fully rendered structure when run via CLI (known defect since 2.11.3.0)
- <!-- JAL-4165 --> Missing last letter when copying consensus sequence from alignment after applying filter (known defect affecting 2.10.4 onwards)
- <!-- JAL-4342 --> export all alignment annotation as CSV produces an empty file or cut'n'paste text box


