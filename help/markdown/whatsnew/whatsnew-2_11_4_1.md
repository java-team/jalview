The 2.11.4 series is the fourth in the minor-release trilogy planned for Jalview's 2.11 series. 

This release adds new capabilities for visualisation and clustering of protein secondary structure annotations, 
along with enhanced control of the display and analysis of secondary structure from different 
sources of 3D structure and prediction. 

The <a href="calculations/calculations.html">Calculations dialog</a> has also been revised to accommodate a new implementation of 
the spatial clustering method <a href="calculations/pasimap.html">PASiMap</a> 
([Su et al. 2022 <i>Comp. Struc. Biotech.</i>](https://doi.org/10.1016/j.csbj.2022.09.034)) 
which was added to Jalview by Thomas Morell and Jennifer Fleming.

Jalview 2.11.4 also has new installers and command line scripts that bring several 
minor improvements, and also makes it easier for IT admins to manage Jalview installations
on computers used by many different people. New installations of Jalview will by default 
store updates in user's home directories, Automatic updates can also be disabled, and 
updates instead carried out using a new jalview_update command line tool.

<a name="experimental"/>

<strong>Experimental features</strong>

This release includes experimental support (enabled in the Tools menu) for
handling drag'n'drop of alignment, tree, annotations and feature file(s) 
onto the Jalview Desktop. They can also be dragged onto the Jalview icon.

 