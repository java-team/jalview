The 2.11.3.2 release addresses a handful of minor defects and known issues, and
fixes a critical bug affecting structure viewer image export when jalview is
run as a command line tool on native and conda-based installations.
