The 2.11.3 Jalview release features a new and more powerful command line interface, support for in-depth exploration of predicted alignment error (PAE) matrices from AlphaFold in the context of multiple alignments, AlphaFold's standard Blue-Orange-Red confidence colourscheme, and a substantial number of [minor improvements and bug fixes](releases.html#Jalview.2.11.3.0)

**Interactive exploration of AlphaFold Predicted Alignment Error Matrices**

Predicted alignment error (PAE) matrices are JSON files produced by AlphaFold and other 3D structure prediction tools, which reflect how well any two positions in a predicted structure are positioned correctly relative to each other. Jalview automatically imports PAE matrices when retrieving protein structures from the EBI-AlphaFold database. A PAE matrix file can also be provided when manually importing 3D structure data.

Once imported, PAE matrices are shown in annotation tracks as a heat map shaded pale to dark green, where darker shades indicate higher confidence in relative positioning. Right-clicking a PAE Matrix's annotation label provides the option to cluster the columns of the matrix, producing a tree where regions of well arranged structure are grouped together, allowing columns containing those regions to be selected and manipulated further.


**Jalview's Next Generation Command Line Interface**

Jalview 2's original command line interface (CLI) allowed alignments to be imported, annotated and overlaid with sequence features, coloured, associated with trees, and exported either as figures (EPS, PNG, SVG, HTML SVG or an interactive BioJS page) or with one of Jalview's supported alignment export file formats. The new command line interface provides all this, as well as a range of additional functionality previously only available via the GUI:

  * 3D structures can be associated with sequences in alignments and shown in Jmol or external viewers
  * Secondary structure, Model Reliability and Temperature factor can be shown and used to colour alignments
  * PAE Matrices can be imported and shown as annotation
  * 3D structure data shown in Jmol can be exported as PNG
  * Scale factors and dimensions can be specified, allowing high resolution image exports

In addition to these new functionality, next generation CLI operations can be applied to a range of files and directories through the use of wild-cards. This allows faster and more efficient processing of large numbers of alignments. It also facilitates modularisation of operations through the use of ['command-line argument files'](features/clarguments-argfiles.html). 

All new CLI parameters are preceded by '--', as opposed to an optional single '-' which was used in Jalview's old CLI. Old style arguments can still be used, so existing scripts will still work as before, but old-style operations cannot be mixed with new operations in the same CLI call. Use of old command line operations will also raise a warning, and we plan to completely remove support in Jalview's next major release (2.12).

As usual, please let us know via [Jalview's Discussion forum](https://discourse.jalview.org) of any issues you encounter using the new CLI, and of course any requests for improvement or new functionality !
