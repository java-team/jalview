<html>
<!--
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 -->
<title>Command Line: basic usage</title>
<body>

  <h1>Command Line: basic usage</h1>

  <p>
  <a href="clarguments.html">Command Line: introduction</a>
  <br/>
  Command Line: basic usage
  <br/>
  <a href="clarguments-advanced.html">Command Line: advanced usage</a>
  <br/>
  <a href="clarguments-argfiles.html">Command Line: argument files</a>
  <br/>
  <a href="clarguments-reference.html">Command Line: reference</a>
  </p>

  <hr/>

  <ul>
  <li><a href="#openingalignments">Opening alignments</a></li>
  <li><a href="#alignmentoptions">Alignment options</a></li>
  <li><a href="#adding3dstructures">Adding 3D structures</a></li>
  <li><a href="#outputtingalignmentfiles">Outputting/converting alignment files and images</a></li>
  <li><a href="#filenamesubstitutionsandbatchprocessing">Filename substitutions and batch processing</a></li>
  <li><a href="#alloutputwildcard">The all output wildcard</a></li>
  </ul>

  <h2><a name="openingalignments"></a>Opening alignments (<code>&#8209;&#8209;open</code>, <code>&#8209;&#8209;append</code>, <code>&#8209;&#8209;new</code>)</h2>

  <p>
  To simply open one or more alignment files in different alignment windows just put the filenames as the first arguments:
  <pre>
  jalview filename1 filename2 ...
  </pre>
  </p>

  <p>
  You can use shell-expanded wildcards:
  <pre>
  jalview this/filename* that/filename* other/filename*
  </pre>
  and URLs:
  <pre>
  jalview https://rest.uniprot.org/uniprotkb/P00221.fasta
  </pre>
  </p>

  <p>
  (Using initial filenames is the same as using the <code>&#8209;&#8209;open</code> argument, and further arguments can be used
  after the initial filenames.)
  </p>

  <h3><a name="open"></a><code>&#8209;&#8209;open</code></h3>

  <p>
  Use the <code>&#8209;&#8209;open</code> argument to open alignment files each in their own window.
  </p>

  <p>
  The following are equivalent:
  <pre>
  jalview --open filename1 filename2 ...

  jalview --open filename*

  jalview --open filename1 --open filename2 --open ...

  jalview filename1 filename2 ...
  </pre>
  </p>

  <p>
  Similarly you can open URLs:
  <pre>
  jalview --open https://rest.uniprot.org/uniprotkb/P00221.fasta
  </pre>
  </p>

  <h3><a name="append"></a><code>&#8209;&#8209;append</code></h3>

  <p>
  To append several alignment files together use:
  <pre>
  jalview --open filename1.fa --append filename2.fa filename3.fa
  </pre>
  or, if you haven't previously used <code>&#8209;&#8209;open</code> then you can use --append to open one new window and keep appending each set of alignments:
  <pre>
  jalview --append these/filename*.fa --append more/filename*.fa

  jalview --append https://rest.uniprot.org/uniprotkb/P00221.fasta https://www.uniprot.org/uniprotkb/A0A0K9QVB3/entry
  </pre>
  </p>

  <p>
  <strong>Note</strong> that whilst you can include a Jalview Project File (<code>.jvp</code>) as an <code>&#8209;&#8209;append</code> value, the items in the file will always open in their original windows and not append to another.
  </p>

  <h3><a name="new"></a><code>&#8209;&#8209;new</code></h3>

  <p>
  To append different sets of alignment files in different windows, use <code>&#8209;&#8209;new</code> to move on to a new alignment window:
  <pre>
  jalview --append these/filename*.fa --new --append other/filename*.fa
  </pre>
  </p>

  <p>
  <code>&#8209;&#8209;open</code> is like using <code>&#8209;&#8209;new --append</code> applied to every filename/URL given to <code>&#8209;&#8209;open</code>
  </p>


  <h2><a name="alignmentoptions"></a>Alignment options (<code>&#8209;&#8209;colour</code>, <code>&#8209;&#8209;wrap</code>, <code>&#8209;&#8209;showannotations</code>, <code>&#8209;&#8209;title</code>)</h2>

  <p>
  An opened alignment window (or set of opened alignment windows) can be modified in its appearance using the following arguments before the next <code>&#8209;&#8209;open</code> argument.  These modifying arguments apply to the one or more files that were opened with the preceding <code>&#8209;&#8209;open</code> argument.  E.g. <code>&#8209;&#8209;open file.fa --colour gecos-flower</code> will colour the one alignment window with <code>file.fa</code>.  However, <code>&#8209;&#8209;open *.fa --colour gecos-flower</code> will colour every alignment window matching <code>file*.fa</code>, and <code> --open file1.fa file2.fa --colour gecos-flower</code>
  will colour both opened alignment windows.
  </p>

  <h3><a name="colour"></a><code>&#8209;&#8209;colour</code></h3>

  <p>
  You can specify a residue/base colouring for the alignment using the <code>&#8209;&#8209;colour</code> option (note spelling -- Jalview is made in Scotland!):
  <pre>
  jalview --open examples/uniref50.fa --colour gecos-flower
  </pre>
  There are several colour schemes that you can use.  See the <a href="../colourSchemes/index.html">page on Colour Schemes</a> for details.
  The names to use on the command line for colour schemes are:
  </p>
  <p>
  <code>clustal</code>,
  <br/>
  <code>blosum62</code>,
  <br/>
  <code>pc-identity</code>,
  <br/>
  <code>zappo</code>,
  <br/>
  <code>taylor</code>,
  <br/>
  <code>gecos-flower</code>,
  <br/>
  <code>gecos-blossom</code>,
  <br/>
  <code>gecos-sunset</code>,
  <br/>
  <code>gecos-ocean</code>,
  <br/>
  <code>hydrophobic</code>,
  <br/>
  <code>helix-propensity</code>,
  <br/>
  <code>strand-propensity</code>,
  <br/>
  <code>turn-propensity</code>,
  <br/>
  <code>buried-index</code>,
  <br/>
  <code>nucleotide</code>,
  <br/>
  <code>nucleotide-ambiguity</code>,
  <br/>
  <code>purine-pyrimidine</code>,
  <br/>
  <code>rna-helices</code>,
  <br/>
  <code>t-coffee-scores</code>,
  <br/>
  <code>sequence-id</code>
  </p>

  <h3><a name="wrap"></a><code>&#8209;&#8209;wrap</code></h3>
  <p>
  An alignment should open with your usual preferences stored in the <code>.jalview_properties</code> file.  To open an alignment with the sequences (definitely) wrapped, following your <code>&#8209;&#8209;open</code> (or first <code>&#8209;&#8209;append</code>) argument use the argument <code>&#8209;&#8209;wrap</code>:
  <pre>
  jalview --open examples/uniref50.fa --wrap
  </pre>
  To ensure an alignment is not wrapped use <code>&#8209;&#8209;nowrap</code>:
  <pre>
  jalview --open examples/uniref50.fa --nowrap
  </pre>
  </p>

  <h3><a name="showannotations"></a><code>&#8209;&#8209;showannotations</code> / <code>&#8209;&#8209;noshowannotations</code></h3>

  <p>
  You can specify whether the currently opened alignment window should show alignment annotations (e.g. Conservation, Quality, Consensus...) or not with either <code>&#8209;&#8209;showannotations</code> or <code>&#8209;&#8209;noshowannotations</code>.  If you don't specify then your saved preference will be used.
  <pre>
  jalview --open examples/uniref50.fa --noshowannotations
  </pre>
  </p>

  <h3><a name="title"></a><code>&#8209;&#8209;title</code></h3>

  <p>
  If you would like to give the alignment window a specific title you can do so with the <code>&#8209;&#8209;title</code> option:
  <pre>
  jalview --open examples/uniref50.fa --title "My example alignment"
  </pre>
  </p>




  <h2><a name="adding3dstructures"></a>Adding 3D structures (<code>&#8209;&#8209;structure</code>, <code>&#8209;&#8209;seqid</code>, <code>&#8209;&#8209;structureviewer</code>, <code>&#8209;&#8209;paematrix</code>, <code>&#8209;&#8209;tempfac</code>, <code>&#8209;&#8209;showssannotations</code>)</h2>

  <p>
  </p>

  <h3><a name="structure"></a><code>&#8209;&#8209;structure</code></h3>

  <p>
  You can add a 3D structure file to a sequence in the current alignment window with the <code>&#8209;&#8209;structure</code> option:
  <pre>
  jalview --open examples/uniref50.fa --structure examples/AlphaFold/AF-P00221-F1-model_v4.pdb
  </pre>
  By default this attaches to the first sequence in the alignment but most likely you will want to attach it to a specific sequence.
  </p>

  <h3><a name="seqid"></a><code>&#8209;&#8209;seqid</code></h3>

  <p>
  The easiest way to specify a sequence ID for your structure is to follow the <code>&#8209;&#8209;structure</code> argument with a <code>&#8209;&#8209;seqid</code> argument with a value of a sequence ID in the alignment.  This does of course require some knowledge of the sequences in the alignment files
  that have been opened.
  <br/>
  Alternatively you can specify a <em>sub-value</em> with the <code>&#8209;&#8209;structure</code> argument value.  You do this by preceding the value with square brackets and <code>seqid=SequenceId</code>,
  like this:
  <pre>
  jalview --open examples/uniref50.fa --structure [seqid=FER1_SPIOL]examples/AlphaFold/AF-P00221-F1-model_v4.pdb
  </pre>
  which is equivalent to
  <pre>
  jalview --open examples/uniref50.fa --structure examples/AlphaFold/AF-P00221-F1-model_v4.pdb --seqid FER1_SPIOL
  </pre>
  </p>

  <p>
  The sub-value <code>seqid=FER1_SPIOL</code> takes precedence over the following argument <code>&#8209;&#8209;seqid FER1_SPIOL</code> if you accidentally specify both (in which case the argument will probably be completely unused).
  </p>

  <p>
  If you don't know the sequence IDs but do know the position of the sequence in the alignment, you can also specify an <em>INDEX</em>
  in the sub-values to indicate which sequence in the alignment to attach the sequence to (although this is less precise).  This is a zero-indexed value, so to specify the eighth sequence in the alignment use:
  <pre>
  jalview --open examples/uniref50.fa --structure [7]examples/AlphaFold/AF-P00221-F1-model_v4.pdb
  </pre>

  <p>
  Remember that you might need to escape any spaces in the sequence ID or enclose the ID in quotation marks.
  </p>

  <h3><a name="structureviewer"></a><code>&#8209;&#8209;structureviewer</code></h3>

  <p>
  You can specify which structure viewer (or none) to use to open the structure using either the <code>&#8209;&#8209;structureviewer</code> argument or the <code>structureviewer</code> sub-value.  Multiple sub-values can be specified together, separated by a comma ','.  Possible values for the <code>structureviewer</code> are:
  <br/>
  <code>none</code>,
  <br/>
  <code>jmol</code>,
  <br/>
  <code>chimera</code>,
  <br/>
  <code>chimerax</code>,
  <br/>
  <code>pymol</code>.
  </p>
  <p>
  <code>none</code> and <code>jmol</code> will always be available, but to use the others you must have the appropriate software already set up on your computer and in Jalview.  See the page <a href="../features/viewingpdbs.html">Discovering and Viewing PDB and 3D-Beacons structures</a> for more details.
  <pre>
  jalview --open examples/uniref50.fa --structure [seqid=FER1_SPIOL,structureviewer=none]examples/AlphaFold/AF-P00221-F1-model_v4.pdb
  </pre>
  or, if you prefer
  <pre>
  jalview --open examples/uniref50.fa --structure examples/AlphaFold/AF-P00221-F1-model_v4.pdb --seqid FER1_SPIOL --structureviewer none
  </pre>
  </p>

  <h3><a name="paematrix"></a><code>&#8209;&#8209;paematrix</code></h3>

  <p>
  If you are opening a structure file that has a PAE matrix (provided as a JSON file), such as from an AlphaFold model or an nf-core pipeline, you can add the PAE matrix as an annotation by following the <code>&#8209;&#8209;structure</code> argument with a <code>&#8209;&#8209;paematrix</code> argument with the filename.  You can also specify a <code>paematrix=filename</code> sub-value.
  <pre>
  jalview --open examples/uniref50.fa --structure [seqid=FER1+SPIOL,structureviewer=pymol]examples/AlphaFold/AF-P00221-F1-model_v4.pdb --paematrix examples/AlphaFold/AF-P00221-F1-predicted_aligned_error_v4.json
  </pre>
  </p>

  <h3><a name="tempfac"></a><code>&#8209;&#8209;tempfac</code></h3>

  <p>
  Structure files may have a temperature factor associated with the structure component positions.  If the temperature factor is a pLDDT confidence score, such as with an AlphaFold model, you can specify this by using a following argument of <code>&#8209;&#8209;tempfac</code> with a value of <code>plddt</code>.  This will enable standard pLDDT colouring of the temperature factor annotation.  Valid values are:
  <code>default</code>,
  <code>plddt</code>.
  More types of temperature factor may be added in future releases of Jalview.
  <br/>
  The value can also be specified as a sub-value:
  <pre>
  jalview --open examples/uniref50.fa --structure [seqid=FER1_SPIOL,structureviewer=jmol,tempfac=plddt]examples/AlphaFold/AF-P00221-F1-model_v4.pdb
  </pre>
  which is equivalent to
  <pre>
  jalview --open examples/uniref50.fa --structure examples/AlphaFold/AF-P00221-F1-model_v4.pdb --tempfac plddt --seqid FER1_SPIOL
   --structureviewer jmol
  </pre>

  </p>

  <!-- notempfac not yet working. undocumented until then -->

  <h3><a name="showssannotations"></a><code>&#8209;&#8209;showssannotations</code> / <code>&#8209;&#8209;noshowssannotations</code></h3>

  <p>
  You can specify whether the currently opened alignment window should show secondary structure annotations or not with either <code>&#8209;&#8209;showssannotations</code> or <code>&#8209;&#8209;noshowssannotations</code>.  If you don't specify then your saved preference will be used.
  <pre>
  jalview --open examples/uniref50.fa --structure examples/AlphaFold/AF-P00221-F1-model_v4.pdb --noshowssannotations
  </pre>
  or you can use a sub-value modifier:
  <pre>
  jalview --open examples/uniref50.fa --structure [noshowssannotations]examples/AlphaFold/AF-P00221-F1-model_v4.pdb
  </pre>
  </p>

  <h2><a name="outputtingalignmentfiles"></a>Outputting/converting alignment files and images (<code>&#8209;&#8209;output</code>, <code>&#8209;&#8209;format</code>, <code>&#8209;&#8209;image</code>, <code>&#8209;&#8209;structureimage</code>, <code>&#8209;&#8209;type</code>, <code>&#8209;&#8209;scale</code>, <code>&#8209;&#8209;width</code>, <code>&#8209;&#8209;height</code>, <code>&#8209;&#8209;imagecolour</code>, <code>&#8209;&#8209;bgcolour</code>, <code>&#8209;&#8209;textrenderer</code>, <code>&#8209;&#8209;overwrite</code>, <code>&#8209;&#8209;backups</code>, <code>&#8209;&#8209;mkdirs</code>)</h2>

  <p>
  You can save an alignment as an alignment file, or exported as an image, in different formats.  Jalview's alignment output formats are:
  fasta,
  pfam,
  stockholm,
  pir,
  blc,
  amsa,
  json,
  pileup,
  msf,
  clustal,
  phylip,
  jalview.
  </p>
  <p>
  Alignments can be exported as an image in formats EPS, SVG, HTML, BioJSON (vector formats) or PNG (bitmap format).
  </p>
  <p>
  In vector formats you can specify whether text should be rendered as text (which may have font changes, but will produce a smaller and more usable file) or as lineart (which will retain exact appearance of text, but will be less easy to edit or use to copy text).
  </p>
  <p>
  In bitmap formats (currently only PNG, but what else would you want?!) you can specify a scaling factor to improve the resolution of the output image.
  </p>

  <h3><a name="output"></a><code>&#8209;&#8209;output</code></h3>

  <p>
  To save the open alignment in a new alignment file use <code>&#8209;&#8209;output filename</code>.  The format for the file can be found from the extension of <code>filename</code>, or if you are using a non-standard extension you can use a following <code>&#8209;&#8209;format</code> argument, or specify it as a sub-value modifier.
  </p>
  <p>
  Recognised formats and their recognised extensions are:
  <br/>
  <code>fasta</code> (<code>fa, fasta, mfa, fastq</code>),
  <br/>
  <code>pfam</code> (<code>pfam</code>),
  <br/>
  <code>stockholm</code> (<code>sto, stk</code>),
  <br/>
  <code>pir</code> (<code>pir</code>),
  <br/>
  <code>blc</code> (<code>blc</code>),
  <br/>
  <code>amsa</code> (<code>amsa</code>),
  <br/>
  <code>json</code> (<code>json</code>),
  <br/>
  <code>pileup</code> (<code>pileup</code>),
  <br/>
  <code>msf</code> (<code>msf</code>),
  <br/>
  <code>clustal</code> (<code>aln</code>),
  <br/>
  <code>phylip</code> (<code>phy</code>),
  <br/>
  <code>jalview</code> (<code>jvp, jar</code>).
  </p>
  <p>
  For example, to open a FASTA file, append another FASTA file and then save the concatenation as a Stockholm file, do
  <pre>
  jalview --open alignment1.fa --append alignment2.fa --output bothalignments.stk
  </pre>
  or
  <pre>
  jalview --append alignment*.fa --output bigballofstring.txt --format stockholm
  </pre>
  or
  <pre>
  jalview --append alignment*.fa --output [format=stockholm]bigballofstring.txt
  </pre>
  </p>

  <p>
  <em>Important!</em> If you use <code>&#8209;&#8209;output</code> or any other argument that outputs a file, then it will be assumed you want to run Jalview in headless mode (as if you had specified <code>&#8209;&#8209;headless</code>).  To use Jalview with <code>&#8209;&#8209;output</code> and not assume headless mode, use the <code>&#8209;&#8209;gui</code> argument (the order doesn't matter).
  </p>

  <p>
  If you would like to output an alignment file directly to standard output (often referred to as STDOUT), then use the filename <code>-</code> (a single hyphen).  In this case any messages that would normally appear on STDOUT will be diverted to STDERR to avoid invalidating the output file.
  </p>
  <p>
  For example, to open a Stockholm file and pipe it to another command as a Block file, do
  <pre>
  jalview --open alignment1.stk --output - --format blc | another_command
  </pre>
  or equivalently
  <pre>
  jalview alignment1.stk --output=[format=blc]- | another_command
  </pre>
  </p>

  <h3><a name="format"></a><code>&#8209;&#8209;format</code></h3>

  <p>
  To specify the format of the output file (if using an unrecognised file extension) use the <code>&#8209;&#8209;format</code> argument to specify a value (see above).  A sub-value modifier on the <code>&#8209;&#8209;output</code> value can also be used.
  </p>

  <h3><a name="image"></a><code>&#8209;&#8209;image</code></h3>
  <p>
  To export the open alignment window as an image, use the <code>&#8209;&#8209;image</code> argument, which will give an image of the alignment and annotations as it appears (or would appear if not in <code>&#8209;&#8209;headless</code> mode) in the alignment window if it was large enough for the whole alignment, including colour scheme and features.
  </p>
  <p>
  <pre>
  jalview --open examples/plantfdx.fa --colour gecos-blossom --features examples/plantfdx.features --annotations examples/plantfdx.annotations --image plantfdx.png --headless
  </pre>
  </p>

  <h3><a name="structureimage"></a><code>&#8209;&#8209;structureimage</code></h3>
  <p>
  To export an open structure as an image, use the <code>&#8209;&#8209;structureimage</code> argument, which will give an image of the structure as it appears (or would appear if not in <code>&#8209;&#8209;headless</code> mode) in a Jmol window including colour scheme. <code>&#8209;&#8209;structureimage</code> can currently only be used with structures opened with the <code>jmol</code> structureviewer (the default viewer).
  </p>
  <p>
  <pre>
  jalview --open examples/plantfdx.fa --colour gecos-blossom --features examples/plantfdx.features --annotations examples/plantfdx.annotations --image plantfdx.png --headless
  </pre>
  </p>

  <p>
  These by default produce a PNG image of screen or webpage resolution, which you will probably want to improve upon.  There are two ways of doing this with Jalview: increasing the scale of the PNG image, or using a vector based image format (EPS, SVG, HTML).
  <p>

  <h3><a name="type"></a><code>&#8209;&#8209;type</code></h3>

  <p>
  To specify the type of image file to write (if using an unrecognised file extension) use the <code>&#8209;&#8209;type</code> argument to specify a value (see above).  A sub-value modifier on the <code>&#8209;&#8209;image</code> and <code>&#8209;&#8209;structureimage</code> value can also be used.  Valid values are:
  <br/>
  <code>png</code> - A Portable Network Graphics image (bitmap, default),
  <br/>
  <code>svg</code> - A Scalable Vector Graphics image (vector),
  <br/>
  <code>eps</code> - An Encapsulated PostScript file (vector),
  <br/>
  <code>html</code> - An HTML rendition of the alignment with embedded source data (vector/web browser),
  <br/>
  <code>biojs</code> - An HTML rendition of the alignment with interactive MSA viewer <a href="biojsmsa.html">BioJS-MSA</a> (vector).
  </p>

  <h3><a name="bitmap"></a>Bitmap image types (<code>png</code>)</h3>

  <p>
  Bitmap images are composed of an array of pixels.  Bitmap images with a low resolution that are blown up to a larger size appear "blocky", so it is important to get the resolution for your purpose correct.  Older screens only require a modest resolution, whilst newer HiDPI screens look better with a higher resolution.  For print you will want an even higher resolution although in this case you would probably want to use a vector image format.  In general creating a bitmap image that has a large resolution means you can scale the image down if needed, although if you are running a batch process this will take more time and resources.
  </p>
  <p>
  Jalview only produces a PNG bitmap image type.  This is a high-colour lossless format which can also use lossless compression so is suitable for alignment figures.
  </p>
  <p>
  Let's increase the resolution of the PNG image:
  </p>

  <h3><a name="scale"></a><code>&#8209;&#8209;scale</code></h3>

  <p>
  We can increase the size of the PNG image by a factor of <em>S</em> by following the <code>&#8209;&#8209;image</code> or <code>&#8209;&#8209;structureimage</code> argument with a <code>&#8209;&#8209;scale <em>S</em></code> argument and value.  The value doesn't have to be an integer and should be given as an integer or floating point formatted number, e.g.
  <pre>
  jalview --open examples/uniref50.fa --colour gecos-ocean --image mypic.png --scale 5.5 --headless
  </pre>
  which will produce a PNG image 5.5 times larger (and more detailed) than without the <code>&#8209;&#8209;scale</code> argument.
  </p>
  <p>
  However, since you won't necessarily already know the size of an alignment's exported image you can specify either an exact width or height (in pixels) with either one of the
  <code>&#8209;&#8209;width</code> and <code>&#8209;&#8209;height</code> arguments:

  <h3><a name="width"></a><code>&#8209;&#8209;width</code></h3>

  <p>
  Specify an exact width of an exported PNG image with <code>&#8209;&#8209;width</code>:
  <pre>
  jalview --headless --open https://www.ebi.ac.uk/interpro/api/entry/pfam/PF03760/?annotation=alignment%3Aseed --noshowannotations --colour gecos-sunset --image wallpaper.png --width 3840
  </pre>
  </p>

  <h3><a name="height"></a><code>&#8209;&#8209;height</code></h3>

  <p>
  Alternatively specify an exact height with the <code>&#8209;&#8209;height</code> argument:
  <pre>
  jalview --headless --open https://www.ebi.ac.uk/interpro/api/entry/pfam/PF03760/?annotation=alignment%3Aseed --noshowannotations --colour gecos-ocean --image wallpaper.png --height 2160
  </pre>
  </p>

  <p>
  You can specify two or all of <code>&#8209;&#8209;scale</code>, <code>&#8209;&#8209;width</code> and <code>&#8209;&#8209;height</code> as limits to the size of the image (think of one or two bounding boxes) and the one which produces the smallest scale of image is used.  You can also specify each of these as sub-value modifiers to the <code>&#8209;&#8209;image</code> or <code>&#8209;&#8209;structureimage</code> value:
  <pre>
  jalview --headless --open https://www.ebi.ac.uk/interpro/api/entry/pfam/PF03760/?annotation=alignment%3Aseed --noshowannotations --colour gecos-flower --image [scale=0.25,width=320,height=240]thumbnail.png
  </pre>
  </p>

  <h3><a name="imagecolour"></a><code>&#8209;&#8209;imagecolour</code></h3>

  <p>
  Specify a colour scheme to use just for this image using the <code>&#8209;&#8209;imagecolour</code> argument:
  <pre>
  jalview --open examples/uniref50.fa --colour gecos-flower --image uniref50-residues.png --height 2160 --image uniref50-helix.png --imagecolour helix-propensity --width 800 --image uniref50-turn.png --imagecolour turn-propensity --width 800
  </pre>
  </p>

  <h3><a name="bgcolour"></a><code>&#8209;&#8209;bgcolour</code></h3>

  <p>
  <strong>Only applies to <code>&#8209;&#8209;structureimage</code>.</strong> Specify a background colour for a structure image.  The colour can be specified as a named colour recognised by Java (e.g. <code>"white"</code>, <code>"cyan"</code>) or as a RRGGBB 6 digit hex string (e.g. <code>ffffff</code>, <code>00ffff</code>).
  </p>
  <p>
  E.g.
  <pre>
  jalview --open examples/uniref50.fa --colour gecos-sunset --structure examples/AF-P00221-F1-model_v4.pdb --seqid FER1_SPIOL --structureimage temp.png  --bgcolour magenta
  </pre>
  </p>

  <p>
  Next we look at vector image formats, which maintain detail at all resolutions.
  </p>

  <h3><a name="vector"></a>Vector image export (<code>svg</code>, <code>eps</code>, <code>html</code>, <code>biojs</code>)</h3>

  <p>
  Jalview can export an alignment in Encapsulated PostScript (<code>eps</code>), Scalable Vector Graphics (<code>svg</code>), HTML (<code>html</code>) or <a href="biojsmsa.html">BioJSON</a> -- another HTML format with an interactive MSA viewer (<code>biojs</code>), by using, e.g.
  <pre>
  jalview --open examples/uniref50.fa --colour gecos-flower --image printable.eps
  </pre>
  The image format can be specified with the <code>&#8209;&#8209;type</code> argument or as a sub-value modifier on the <code>&#8209;&#8209;image</code> value.  If neither is used the <code>type</code> will be guessed from the image file extension.  The following three examples should produce the same contents:
  <pre>
  jalview --open examples/uniref50.fa --colour gecos-flower --image printable.eps
  jalview --open examples/uniref50.fa --colour gecos-flower --image printable.postscript --type eps
  jalview --open examples/uniref50.fa --colour gecos-flower --image [type=eps]printable.postscript
  jalview --open examples/uniref50.fa --colour gecos-flower --image [type=biojs]printable.html
  </pre>
  </p>

  <h3><a name="textrenderer"></a><code>&#8209;&#8209;textrenderer</code></h3>

  <p>
  In a vector format any text that appears on the page (including residue/base labels) can be saved in the image file either as <code>text</code> or as <code>lineart</code> using the <code>&#8209;&#8209;textrenderer</code> argument.  This is only available for <code>eps</code>, <code>svg</code> and <code>html</code> formats.
  </p>

  <p>
  The difference is potentially in the appearance of the file, and definitely in the filesize!  Saving with <code>text</code> requires the font used to display the text characters in the alignment to be present on the viewing platform to look exactly the same.  If it isn't then another suitable font will probably be used.  The filesize using <code>text</code> is relatively small.
  </p>
  <p>
  When using <code>lineart</code> each individual character that appears in the alignment (including names/titles and resisdues/bases) is stored in the image with its own vector lines.  This means that the appearance of the text is retained exactly independent of installed fonts, but the filesize is increased.  You will also be unable to copy what appears to be text as text.
  </p>

  <p>
  The type of <code>&#8209;&#8209;textrenderer</code> can be specified with an argument following <code>&#8209;&#8209;image</code> or as a sub-value modifier:
  <pre>
  jalview --open examples/uniref50.fa --colour gecos-flower --image printable.html --type biojs
  jalview --open examples/uniref50.fa --colour gecos-flower --image [type=eps,textrenderer=lineart]printable.ps
  </pre>
  </p>

  <h3><a name="outputbehaviour"></a>Output behaviour</h3>

  <h3><a name="overwrite"></a><code>&#8209;&#8209;overwrite</code></h3>

  <p>
  By default, Jalview will refuse to overwrite an output file (alignment or image) unless backups are in operation (alignment files only).  To force overwriting files, use the <code>&#8209;&#8209;overwrite</code> argument.
  </p>

  <h3><a name="backups"></a><code>&#8209;&#8209;backups / --nobackups</code></h3>

  <p>
  Jalview should honour your preferences for backup files of output alignment files.  Using <code>&#8209;&#8209;backups</code> or <code>&#8209;&#8209;nobackups</code> forces the behaviour.  With no backups set, you will need to use <code>&#8209;&#8209;overwrite</code> to overwrite an existing file.  Note that Jalview does not make backup files of exported images.
  </p>

  <h3><a name="mkdirs"></a><code>&#8209;&#8209;mkdirs</code></h3>

  <p>
  If you want to output a file into a folder that doesn't yet exist (this might happen particularly when using <code>{dirname}</code> substitutions -- see below), then Jalview will fail to write the file since the parent directory doesn't exist.  You can use <code>&#8209;&#8209;mkdirs</code> to tell Jalview to make the new directory (or directories, it will create several nested directories if necessary) before writing the file.  <code>&#8209;&#8209;mkdirs</code> is cautious and will generally refuse to make a new directory using a relative path with <code>..</code> in.
  </p>

  <h2><a name="filenamesubstitutionsandbatchprocessing"></a>Filename substitutions and batch processing (<code>&#8209;&#8209;substitutions</code>, <code>&#8209;&#8209;close</code>, <code>&#8209;&#8209;all</code>)</h2>

  <p>
  One of the more powerful aspects of Jalview's command line operations is that stores all of the different opened alignment arguments (before opening them) and can apply some arguments to <em>all</em> of the alignments as they are opened.  This includes display and output arguments.
  </p>

  <p>
  When outputting a file you will obviously want to use a different filename for each of the alignments that are opened.  There are several ways you can specify how that filename differs, but the easiest way is to refer to the input filename and change the extension.  In the case of an alignment where multiple files are appended, the filename of the first file to be appended or opened is used.
  </p>

  <p>
  To refer to different parts of the opening filename, you can use the strings
  <ul>
  <li><code>{dirname}</code> -- is replaced by the directory path to the opened file.</li>
  <li><code>{basename}</code> -- is replaced by the base of the filename of the opened file. This is without the path or file extension (if there is one).</li>
  <li><code>{extension}</code> -- is replaced by the extension of the filename of the opened file.</li>
  </ul>
  The braces (curly brackets '{', '}') are important!
  </p>

  <p>
  Specifically for <code>&#8209;&#8209;structureimage</code> output, you can also use substitutions using parts of the structure filename:
  <ul>
  <li><code>{structuredirname}</code> -- is replaced by the directory path to the opened structure file.</li>
  <li><code>{structurebasename}</code> -- is replaced by the base of the filename of the opened structure file. This is without the path or file extension (if there is one).</li>
  <li><code>{structureextension}</code> -- is replaced by the extension of the filename of the opened structure file.</li>
  </ul>
  </p>

  <p>
  These filename substitutions are on by default, but if for whatever reason you wish to disable the substitutions, they can be turned off (or back on again) through the list of arguments with:
  </p>

  <h3><a name="substitutions"></a><code>&#8209;&#8209;substitutions / --nosubstitutions</code></h3>

  <p>
  Enable (or disable) filename substitutions in the following argument values and sub-value modifier values.
  <pre>
  jalview --open exampes/uniref50.fa --nosubstitutions --output I_Want_A_Weird_Output_Filname_With_{basename}_Curly_Brackets_In_And_A_Sensible_One_Next.stk --substitutions --output tmp/{basename}.stk --headless
  </pre>
  </p>

  <p>
  For opening single files this is less useful, since you could obviously just type the output filename, but for multiple opened alignments you can also use these substituted values and they will be replaced by the relevant part of the filename given for each opened alignment window.  Normally an <code>&#8209;&#8209;output</code> or <code>&#8209;&#8209;image</code> argument will only be applied to the latest opened alignment window, but you can tell Jalview to apply some arguments to all alignments that have been opened (so far) by using the <code>&#8209;&#8209;all</code> argument.
  </p>

  <h3><a name="all"></a><code>&#8209;&#8209;all / -noall</code></h3>

  <p>
  When using the <code>&#8209;&#8209;all</code> argument, following arguments will apply to all of the previously opened alignment windows.  You can turn this behaviour off again for following arguments using the <code>&#8209;&#8209;noall</code> argument.  The arguments that can apply to all previously opened alignments are:
  <br/>
  <code>&#8209;&#8209;colour</code>
  <br/>
  <code>&#8209;&#8209;sortbytree</code>
  <br/>
  <code>&#8209;&#8209;showannotations</code>
  <br/>
  <code>&#8209;&#8209;wrap</code>
  <br/>
  <code>&#8209;&#8209;nostructure</code>
  <br/>
  <code>&#8209;&#8209;notempfac</code>
  <br/>
  <code>&#8209;&#8209;showssannotations</code>
  <br/>
  <code>&#8209;&#8209;image</code>
  <br/>
  <code>&#8209;&#8209;type</code>
  <br/>
  <code>&#8209;&#8209;textrenderer</code>
  <br/>
  <code>&#8209;&#8209;scale</code>
  <br/>
  <code>&#8209;&#8209;width</code>
  <br/>
  <code>&#8209;&#8209;height</code>
  <br/>
  <code>&#8209;&#8209;output</code>
  <br/>
  <code>&#8209;&#8209;format</code>
  <br/>
  <code>&#8209;&#8209;groovy</code>
  <br/>
  <code>&#8209;&#8209;backups</code>
  <br/>
  <code>&#8209;&#8209;overwrite</code>
  <br/>
  <code>&#8209;&#8209;close</code>
  </p>
  <p>
  In particular, for our example above, we can use <code>-all</code> and <code>&#8209;&#8209;output</code> like this (<code>&#8209;&#8209;close</code> will be explained in a moment):
  <pre>
  jalview --open all_my_fasta_files/*.fa --all --output all_my_converted_stockholm_files/{basename}.stk --close --headless
  </pre>
  </p>

  <p>
  Another example would be to create a print quality coloured postscript image for every Fasta file in several directories and place the image next to the alignment:
  <pre>
  jalview --open */*.fa --all --colour gecos-flower --image {dirname}/{basename}.eps --textrenderer text --close --headless
  </pre>
  or thumbnails for every Fasta file with
  <pre>
  jalview --open */*.fa --all --colour gecos-flower --image {dirname}/{basename}.png --width 256 --height 256 --close --headless
  </pre>
  </p>

  <h3><a name="close"></a><code>&#8209;&#8209;close</code></h3>

  <p>
  The <code>&#8209;&#8209;close</code> tag is used to close an alignment window after all output files or image exports are performed.  This reduces memory use, especially if an <code>&#8209;&#8209;open</code> value is set to open many files.  These will be opened, formatted and output sequentially so since they are closed before the next one is opened memory use will not build up over a large number of alignments.
  <pre>
  </pre>
  </p>


  <h2><a name="alloutputwildcard"></a>The all output wildcard: <code>&#8209;&#8209;output "*/*.ext"</code>,  <code>&#8209;&#8209;image "*/*.ext"</code></h2>

  <p>
  Purely as an intuitive syntactic sweetener, you can use the <code>&#8209;&#8209;output</code> wildcard <code>*</code> in two places as part of an output path and filename.
  </p>

  <p>
  Using an asterisk (<code>*</code>) as a filename before an extension, e.g. <code>&#8209;&#8209;image "tmp/*.png"</code> will result in that asterisk being treated as a <code>{basename}</code> substitution.
  </p>

  <p>
  Using an asterisk (<code>*</code>) before a file separator (usually </code>/</code>), e.g. <code>&#8209;&#8209;image "tmp/*/file1.png"</code> will result in that asterisk being treated as a <code>{dirname}</code> substitution.
  </p>

  <p>
  You can combine these, using an asterisk (<code>*</code>) before and after the last file separator, e.g. <code>&#8209;&#8209;image "tmp/*/*.png"</code> will result in being substituted like <code>tmp/{dirname}/{basename}.png</code>.
  </p>

  <p>
  For example, to achieve the same as the thumbnails example above, you could use
  <pre>
  jalview --open */*.fa --image "*/*.png" --colour gecos-flower --width 256 --height 256 --close --headless
  </pre>
  Here we move the <code>&#8209;&#8209;colour</code> argument after the <code>&#8209;&#8209;output</code> argument (it will still be applied before the image export or file output) so that it is included after the implied <code>&#8209;&#8209;all</code> argument.  The thumbnails will be placed in the same directory as the alignment file with the same filename except for a different extension of <code>.png</code>.
  </p>

  <p>
  For a simple conversion of many fasta files into Stockholm format, simply use
  <pre>
  jalview --open all_my_fasta_files/*.fa --output "*.stk" --close --headless
  </pre>
  </p>

  <p>
  <strong>Important!</strong>  Please note that using a bareword <code>*.ext</code> (i.e. without an escape or quotation marks) in a shell command line will potentially be expanded by the shell to a list of all the files in the current directory ending with <code>.ext</code> <em>before</em> this value is passed to Jalview.  This will result in the first of these files being overwritten (multiple times)!  If you shell-escape the <code>*</code> (usually with a backslash '\') or enclose the value in quotation marks (<code>"*.ext"</code> - that's including the quotation marks) then the shell will not expand the wildcard.
  </p>

  <p>
  An alternative is to use an equals sign ('=') with no spaces between the argument and the value, <code>&#8209;&#8209;output=*.ext</code>, which Jalview will interpret the same, but the shell will not automatically expand before it is sent to Jalview, e.g.
  <pre>
  jalview --open all_my_fasta_files/*.fa --output=*.stk --close --headless
  </pre>
  </p>

  <hr/>
  Continue to <a href="clarguments-advanced.html">Command Line: advanced usage</a>.
  <br/>
  <a href="clarguments-reference.html">Command Line: reference</a>

</body>
</html>
