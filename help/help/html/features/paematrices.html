<html>
<!--
 * Jalview - A Sequence Alignment Editor and Viewer (2.11.4.1)
 * Copyright (C) 2024 The Jalview Authors
 * 
 * This file is part of Jalview.
 * 
 * Jalview is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *  
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 -->
<head>
<title>Working with PAE Matrices in Jalview</title>
</head>

<body>
	<p>
		<strong>Working with Predicted Alignment Error Matrices in
			Jalview</strong>
	</p>

  <p>Predicted Alignment Error (PAE) matrices are produced by
    deep-learning based 3D-structure prediction pipelines such as
    AlphaFold. They reflect how reliably two parts of a model have been
    positioned in space. Each column in a PAE matrix corresponds to a 
    residue in the model, and each gives the likely RMS error (in
    &Aring;ngstroms) between that residue and every other modelled
    position the pair of residues' real relative position, if the model
    and real 3D structure were superimposed at that residue.</p>
	<p>
		Jalview visualises PAE matrices as an alignment annotation track,
		shaded from dark green to white, similar to the encoding used on the
		EBI-AlphaFold website (see <a
			href="https://alphafold.ebi.ac.uk/entry/O04090">O04090 3D model</a>
		at EBI-AlphaFoldDB).
	</p>
  <img src="../structures/epas1_annotdetail.png" width="450" />
  <br/><em>
				Alignment of EPAS1 homologs from Human, Rat and Cow shaded by PLDDT, with
				Predicted Alignment Error and secondary structure annotation shown for Human.
				</em>
	<!--</div>  
  <div width="35%">
		<figure>
			<img src="../structures/epas1_pae_ebiaf.png" />
			<figcaption>
				Predicted Alignment Error for Human EPAS1<br />from <a
					href="https://alphafold.ebi.ac.uk/entry/Q99814">https://alphafold.ebi.ac.uk/entry/Q99814</a>
			</figcaption>
		</figure>
		</div>
	</div> -->
	<p>
		<strong>Importing PAE Matrices</strong>
	</p>
	<p>
		Jalview retrieves PAE matrices when importing predicted 3D structures
		from the EBI-AlphaFold database via <a
			href="../features/structurechooser.html">Jalview's structure
			chooser</a> GUI. If you have produced your own models and accompanying
		PAE matrices using a pipeline such as ColabFold, then you can load
		them both together via the <a
			href="../features/structurechooser.html#loadpdbfile">Load PDB
			File</a> dropdown menu in the 3D structure chooser, providing it is in a
		<a href="../io/paematrixformat.html">supported PAE format</a>.
	</p>
	<p>
		The <a href="../features/clarguments-basic.html">Command Line
			Interface</a> also provides the option to import a PAE matrix alongside
			a 3D structure file, enabling the automated production of alignment figures
		  annotated with PAE matrices and PLDDT scores.
	</p>
	<p>
		<strong>Showing PAE Matrix Annotations </strong>
	</p>
	<p>
		When viewing 3D structures from the EBI-AlphaFold database or local 3D
		structures with an associated PAE file, the PAE is imported as <i>Reference
			Annotation</i>, and is not always automatically added to the alignment
		view.
	</p>
	<p>To show the PAE, right click the sequence and locate the 'Add
		Reference Annotation' entry in the Sequence ID submenu, or select all
		sequences and locate the option in the Selection submenu. You can do
		this in any alignment window (or view) where a sequence with
		associated PAE data appears.</p>
	<p>
		<strong>Adjusting the height of PAE matrix annotations</strong>
	</p>
	<p>
		PAE annotations behave in the same way as Jalview's line graph and
		histogram tracks. Click+dragging up and down with the left (select)
		mouse button held down will increase or decrease the height of the
		annotation. You can also hold down <strong><em>SHIFT</em></strong>
		whilst doing this to adjust the height of all PAE rows at once.
	</p>
	<p>PAE matrix annotation rows behave like any other sequence
		associated annotation, with the following additional features:</p>
	<ul>
		<li>The vertical axis of the PAE heatmap is mapped to positions
			on the linked 3D structure.
			<ul>
				<li>Mousing over the matrix shows a tooltip giving information
					on the range of values under the mouse.<br />Positions in the
					associated 3D structure are also highlighted in any linked views.
				</li>
				<li>Clicking on positions in the matrix selects columns of the
					alignment corresponding to the row and column in the matrix.</li>
			</ul>
		</li>
		<li>Rectangular selections (created by Cmd (or Alt)+Click
			dragging on the matrix) can be created to select multiple ranges of
			columns at once.</li>
		<li>Columns corresponding to adjacent regions with similarly low
			levels of predicted alignment error can be selected by Ctrl+Clicking
			on a region in the matrix.</li>
		<li>Columns of an alignment showing a PAE matrix can be grouped
			and selected by clustering the matrix.</li>
	</ul>
	<p>
		<strong><a name="clustering">Clustering PAE Matrices</a></strong>
	</p>
	<p>PAE matrices are useful for identifying regions of 3D structure
		predictions that are likely to be positioned in space in the same or
		similar way as shown in the predicted structure data. Regions of low
		PAE often correlate with high alphafold reliability (PLDDT) scores,
		but also complement them since they highlight well-folded regions such
		as domains, and how well those regions have been predicted to be
		positioned relative to each other, which is important when evaluating
		whether domain-domain interactions or other contacts can be trusted.</p>
	<p>To make it easy to identify regions of low PAE, Jalview can
		cluster the PAE matrix, allowing columns of the matrix to be grouped
		according to their similarity, using an Average Distance (UPGMA) tree
		algorithm and the sum of differences between each column's PAE values.</p>
	<p>
		<strong><em>dist<sub>ij</sub></em> = &#8741; <em><u>p</u><sub>i</sub>-<u>p</u><sub>j</sub></em>
			&#8741;</strong>
	</p>
	<p>
		<em>Creating a PAE matrix tree</em><br/>Right click on a PAE annotation's label
		to open the annotation popup menu, and select <strong><em>Cluster
				Matrix</em></strong>. Once the calculation has finished, a tree viewer will open,
		and columns of the matrix are then partitioned into groups such that
		the third left-most node from the root is placed in its own group.
		Colours are randomly assigned to each group. By default column group colours will
		also be overlaid on the matrix annotation row - this can be turned off 
		via the PAE annotation row menu (by unticking <em>Show groups on matrix</em>).</p>
	<p>
  The PAE matrix tree viewer behaves like other tree views, except:  
	<ul>
		<li>Selecting nodes or groups of nodes in the tree select
			columns in the alignment, and clicking in the tree window adjust
			the matrix's partition.</li>
		<li>Only one tree and clustering can be defined for a PAE matrix,
			regardless of whether it is displayed in different views or
			alignments.</li>
	</ul>
	Once the PAE annotation has clustering defined:
	<ul><li>Double clicking on a position in the PAE annotation where a
			clustering has been defined will select both the row and column
			clusters for the clicked position. <br/><br/>This makes it easy to select
			clusters corresponding to pairs of interacting regions.</li>
		<li>Cluster colours for a PAE matrix can be used to colour
			sequences or columns of the alignment via the <strong><em><a
					href="../colourSchemes/annotationColouring.html">Colour by
						Annotation.. dialog</a></em></strong>
						(opened by right-clicking the annotation label
			and selecting from the popup menu).
		</li>
	</ul>
	<p>
		<strong>PAE matrices and Jalview Projects</strong>
	</p>
	<p>Any PAE matrices imported to Jalview are saved along side any
		trees and clustering defined on them in Jalview Projects.</p>
	<p>
		<em>Support for visualision and analysis of predicted alignment
			error matrices was added in Jalview 2.11.3. </em>
	</p>
</body>
</html>
