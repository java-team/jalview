<html>
<!--
 * Jalview is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
 * PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Jalview.  If not, see <http://www.gnu.org/licenses/>.
 * The Jalview Authors are detailed in the 'AUTHORS' file.
 -->
<title>Command Line: introduction and reference</title>
<body>

  <h1>Command Line: introduction and reference</h1>

  <p>
  Command Line: introduction
  <br/>
  <a href="clarguments-basic.html">Command Line: basic usage</a>
  <br/>
  <a href="clarguments-advanced.html">Command Line: advanced usage</a>
  <br/>
  <a href="clarguments-argfiles.html">Command Line: argument files</a>
  <br/>
  <a href="clarguments-reference.html">Command Line: reference</a>
  </p>


  <hr/>

  <ul>
  <li><a href="#introduction">Introduction</a></li>
  <li><a href="#syntax">Syntax</a></li>
  <li><a href="#headlessmode">Headless mode</a></li>
  </ul>

  <h2><a name="introduction"></a>Introduction</h2>

  <p>
  From version 2.11.3.0 Jalview has a new set of command line arguments
  which allow more powerful and flexible combinations of arguments, though can
  also be used for simple use cases too.
  </p>

  <p>
  These new arguments are all accessed with a <code>&#8209;&#8209;doubledash</code> form of
  command line argument (with the one exception where simply opening one or more
  files can be performed without any arguments other than the filenames).
  </p>

  <p>
  The old command line arguments can still be used (see
  <a href="clarguments-old.html">the old page on command line arguments</a>) so
  existing scripts utilising them should not break.
  <br/>
  <strong>These are now deprecated and will be removed</strong> in a future version of Jalview.
  </p>

  <p>
  However, you cannot mix old and new style arguments, so if you use any
  <code>-singledash</code> arguments (with the exception of <code>-help</code> or <code>-h</code>), they will all be interpreted as
  old style arguments with the new <code>&#8209;&#8209;doubledash</code>
  arguments being ignored.  If you have a script
  that uses the old arguments without any dashes, and uses the bare-word
  <code>open</code> then these will also be interpreted as old style arguments.
  </p>

  <p>
  <strong>Warning!</strong> If you use command line arguments without any dashes and
  <em>don't</em> use the bare-word argument <code>open</code> then all
  your arguments will be interpreted as alignment files to be opened by the
  new command line argument process!
  </p>

  <p>
  To launch Jalview from the command line, see
  <a href="commandline.html">running Jalview from the command line</a>.
  </p>


  <h2><a name="syntax"></a>Syntax</h2>

  <p>
  The new command line argument parser can group certain labelled arguments together, or give them a default label based on their position in the list of arguments (in which case you won't ever need to know what the label is).  All arguments are read before any alignment actions are performed.  For basic usage without additional syntax, please see the <a href="clarguments-basic.html">Command Line: basic usage</a> explanatory page.
  </p>

  <h3>
  Parts of Jalview's command line arguments
  </h3>
  <pre>jalview --argname[linkedId]=[subvalues]value --switch --noswitch --argname[linkedId] [subvalues]filename1 filename2 ...</pre>

  <p>
  Different arguments can take one or more values, others take no value and act like a switch (some can be set on and off and others are only on, depending on the use).
  <br/>
  <ul>
    <li>
        For arguments that require a value, the value can be given after an equals-sign ('=') or a space (' ').
        <br/>
        <code>&#8209;&#8209;arg value</code>
        <br/>
        <code>&#8209;&#8209;arg=value</code>
    </li>
    <li>
        For arguments that can take multiple values (these will be filenames), the multiple filenames should appear after a space. If you use a filename wildcard you can put this after a space (which will be expanded by the shell unto multiple filenames before they reach Jalview), or you can put it after an equals-sign, which will be used by Jalview to find a list of files.  You cannot use an equals-sign and value followed by further values.
        <br/>
        <code>&#8209;&#8209;arg file1.fa otherfile.stk</code>
        <br/>
        <code>&#8209;&#8209;arg filename*.fa</code> <em>(filenames expanded by shell)</em>
        <br/>
        <code>&#8209;&#8209;arg=filename*.fa</code> <em>(filenames expanded by Jalview)</em>
    </li>
    <li>
        For arguments that act as a switch, most can be negated by preceding the argument name with <code>no</code>.
        <br/>
        <code>&#8209;&#8209;switch</code>
        <br/>
        <code>&#8209;&#8209;noswitch</code>
    </li>
    <li>
        Some values can be modified, or may need additional information (for instance an <code>&#8209;&#8209;image</code> output can be modified with a <code>&#8209;&#8209;scale=number</code> factor, or a <code>&#8209;&#8209;structure</code> can refer to a sequence with a <code>&#8209;&#8209;seqid=ID</code>).  This additional information can be added in a number of different ways.
        <ul>
          <li>
              An argument immediately following the main argument.
              <br/>
              <code>&#8209;&#8209;image output.png --scale 2.5</code>
          </li>
          <li>
              A <em>sub-value modifier</em>, which is where one or more (comma-separated) values are added to the start of the main value, placed in square brackets.
              <br/>
              <code>&#8209;&#8209;open=[nowrap,colour=gecos-blossom]uniref50.fa</code>
              <br/>
              Sub-value modifiers with a value must use an equals-sign separator, and those that act as a switch can simply be included without an equals-sign or value, and can be preceded with <code>no</code> to negate the value, as with the argument name.
          </li>
          <li>
              Another argument with the same <em>linked ID</em>.  A linked ID is an optional identifier for a particular open alignment, placed in square brackets immediately following the argument name (before the equals-sign or space).  If linked IDs are specified they do not need to be near to each other.
              <br/>
              <code>&#8209;&#8209;image[MYID]=output.png --other --args --scale[MYID]=2.5</code>
          </li>
          <li>
              An argument that is designated as applying to <em>all linked IDs</em>
              <br/>
              <code>&#8209;&#8209;image=output.png --other --args --all --scale=2.5</code>
              <br/>
              <code>&#8209;&#8209;image=output.png --other --args --scale[*]=2.5</code>
          </li>
        </ul>
    </li>
  </ul>
  </p>

  <p>
  This may sound complicated, but nearly everything can be done just with plain command line arguments (see <a href="clarguments-basic.html">Command Line: basic usage</a>), though in this case the ordering of the arguments is more important.
  </p>


  <h2><a name="headlessmode"></a>Headless mode</h2>

  <p>
  Jalview can be run in headless mode, i.e. without the usual graphical user interface (GUI), by specifying the <code>&#8209;&#8209;headless</code> argument.  With command line arguments you can specify operations for Jalview to perform on one or more files and then stop running.  Most likely you will want to output another file, either an alignment for image file.
  </p>
  <p>
  <strong>If you specify an argument for an output file</strong> (one or more of <code>&#8209;&#8209;output</code>, <code>&#8209;&#8209;image</code> or <code>&#8209;&#8209;structureimage</code>) then it will be assumed that you wish to <strong>run in headless mode</strong>.
  </p>
  <p>
  You can force Jalview to run in graphical mode using the <code>&#8209;&#8209;gui</code> argument.
  </p>

  <p>
  </p>


  <hr/>
  Continue to <a href="clarguments-basic.html">Command Line: basic usage</a>.
  <br/>
  <a href="clarguments-reference.html">Command Line: reference</a>

</body>
</html>
